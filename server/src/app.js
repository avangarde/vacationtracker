const winston = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
const readline = require('readline');
const {combine, timestamp, printf, splat} = winston.format;

if (!fs.existsSync('logs')) fs.mkdirSync('logs');

var transportLogs = new (winston.transports.DailyRotateFile)({
  filename: 'logs-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HHmm',
  dirname: 'logs',
  frequency: '24h',
  zippedArchive: true,
  level: 'info'
});

var transportErrors = new (winston.transports.DailyRotateFile)({
  filename: 'errors-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HHmm',
  dirname: 'logs',
  frequency: '24h',
  zippedArchive: true,
  level: 'error'
});

global.logger = winston.createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
    splat()
  ),
  transports: [transportLogs, transportErrors]
});


var express = require('express'),
  app = express(),
  mailOptions,
  nodemailer = require("nodemailer"),
  transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true, // use SSL
    auth: {
      user: 'vacationtracker@avangarde-software.com',
      pass: 'Avangarde1234'
    }
  }),

  server = require('http').createServer(app),
  io = require('socket.io')(server),
  port = process.env.PORT || 4000,
  router = express.Router(),
  mysql = require('mysql'),
  bodyParser = require("body-parser"),
  bcrypt = require('bcryptjs'),
  sha256 = require('js-sha256'),
  Holidays = require('date-holidays'),


  year = new Date().getFullYear(),
  nextyear = year + 1,
  fileUpload = require('express-fileupload'),
  moment = require('moment'),
  linkLocal = 'http://localhost:8000/vacationtracker/public/html/display.html?calendar&hid=',
  linkServer = 'http://5.2.197.121:70/public/html/display.html?calendar&hid=',
  bannerLocal = 'C:/wamp64/www/vacationtracker/server/src/banner.png',
  bannerServer = 'C:/wamp64/www/vacationtracker/server/src/banner.png';

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

hd = new Holidays('RO');
hdm = new Holidays('MD');

hd.getHolidays(year);
hd.getHolidays(nextyear);

hdm.getHolidays(year);
hdm.getHolidays(nextyear);

var legalhol = hd.getHolidays(year).concat(hd.getHolidays(nextyear)),
  legalholm = hdm.getHolidays(year).concat(hdm.getHolidays(nextyear));

var pool = mysql.createPool({
  connectionLimit: 100, //important
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'holidayPlanner1',
  debug: false
});

function handleDisconnect() {
  connection = mysql.createPool({
    connectionLimit: 100, //important
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'holidayPlanner1',
    debug: false
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.getConnection(function (err) {              // The server is either down
    if (err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();


io.on('connection', function (socket) {
  socket.on('/setApproved', function (data) {
    var token = data.token,
      status = data.status,
      id = data.id,
      approverComment = data.approverComment,
      type = data.type,
      email = data.email,
      approved = data.approved,
      avfreedays = data.avfreedays,
      admin = data.admin,
      userID = data.userID,
      userObject = data.userObject;
    isValidToken(token).then(function (result) {
      io.emit('/resSetApproved', data);
      setApproved(status, id, approverComment, type, email, approved,
        avfreedays, admin, userID, userObject, function (res) {
        });
    }, function (error) {
      error = ({
        "code": 110,
        "status": "Your session has expired and you are loged out. - redirect la index in FE"
      });
      io.emit('/resSetApproved', error);
    });
  });

  socket.on('/deleteHoliday', function (data) {
    var token = data.token,
      id = data.id;
    isValidToken(token).then(function (result) {
      deleteHoliday(token, id, function (res) {
        io.emit('/resdeleteHoliday', token);
      });
    }, function (error) {
      error = ({
        "code": 110,
        "status": "Your session has expired and you are loged out. - redirect la index in FE"
      });
      io.emit('/resdeleteHoliday', error);
    });
  });

  socket.on('/updatedate', function (data) {
    var token = data.token;
    isValidToken(token).then(function (result) {
      handle_dateupdate(data, function (res) {
        io.emit('/resupdatedate', res);
      });
    }, function (error) {
      error = ({
        "code": 110,
        "status": "Your session has expired and you are loged out. - redirect la index in FE"
      });
      io.emit('/resupdatedate', error);
    });
  });

  socket.on('/refreshManagedUserTable', function (data) {
    var token = data.token;
    isValidToken(token).then(function (result) {
      //refreshManagedUserTable(data, function (res) {
      io.emit('/resRefreshManagedUserTable', data);
      // });
    }, function (error) {
      error = ({
        "code": 110,
        "status": "Your session has expired and you are loged out. - redirect la index in FE"
      });
      io.emit('/resRefreshManagedUserTable', error);
    });
  });

  socket.on('/RefreshCalendar', function (data) {
    var token = data.token;
    isValidToken(token).then(function (result) {
      RefreshCalendar(data, function (res) {
        io.emit('/resRefreshCalendar', res);
      });
    }, function (error) {
      error = ({
        "code": 110,
        "status": "Your session has expired and you are loged out. - redirect la index in FE"
      });
      io.emit('/resRefreshCalendar', error);
    });
  });
});

//insert legal holidays into database
function legalHolidaysToDb(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    hd = new Holidays('RO');
    hd.getHolidays(nextyear);
    legalhol = hd.getHolidays(nextyear);
    res.json(legalhol);

    for (let i in legalhol) {
      if (legalhol[i].type === 'public') {
        let date = moment(legalhol[i].date).format('YYYY-MM-DD');
        connection.query("INSERT INTO legalholidays(startDate, name, type) VALUES ('" + date + "', '" +
          legalhol[i].name + "','" + legalhol[i].type + "')",
          function (err, rows) {
          });
      }
    }
  });
}

//insert legal holidays of Moldova into database
function legalHolidaysMDToDb(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    hd = new Holidays('MD');
    hdm.getHolidays(nextyear);
    var legalholm = hdm.getHolidays(nextyear);
    res.json(legalholm);

    for (var i in legalholm) {
      if (legalholm[i].type == 'public') {
        var date = moment(legalholm[i].date).format('YYYY-MM-DD');
        connection.query("INSERT INTO legalholidaysMD(startDate, name, type) VALUES ('" + date + "', '" +
          legalholm[i].name + "','" + legalholm[i].type + "')",
          function (err, rows) {
          });
      }
    }
  });
}

//set token at login
function setToken(token, id) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE user SET token = '" + token + "' WHERE userID = '" + id + "'", function (err, rows) {
      connection.release();
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//login
function login(req, res) {
  var params = req.body,
    hash = sha256.create();
  hash.update(Math.random().toString(36).substr(2, 5));
  var token = hash.hex();


  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT * FROM user WHERE email='" + params.email + "' AND password='" + params.password + "'", function (err, rows) {
      connection.release();
      if (rows != "") {
        if (!err) {
          var user = rows[0];
          if (!user.token) {
            setToken(token, user.userID);
            user.token = token
          }
          res.json(user);
        } else {
          res.json({
            "code": 100,
            "status": "Error in connection database"
          });
        }
      } else {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get updated data for user
function checkUser(req, res) {
  let params = req.query;

  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT * FROM user WHERE email='" + params.email + "'", function (err, rows) {
      connection.release();
      if (rows != "") {
        if (!err) {
          var user = rows[0];
          res.json(user);
        } else {
          res.json({
            "code": 100,
            "status": "Error in connection database"
          });
        }
      } else {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get free days for current user
function getFreeDaysApprover(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    var token = req.query.token,
      params = req.query,
      page = params.infoPage,
      entries = params.infoEntries,
      totalFreeDays = params.totalFreeDays,
      start = params.start,
      end = params.end,
      all = params.all,
      freedays = {
        'draw': req.draw,
        "recordsTotal": JSON.parse(totalFreeDays),
        "recordsFiltered": JSON.parse(totalFreeDays),
        "data": []
      };
    if (end > 0 && page > 1) {
      query = "SELECT user.name, user.position, user.picture, freedays.* FROM freedays JOIN user " +
        "ON user.userID = freedays.userID WHERE " +
        "freedays.userID = (SELECT user.userID FROM user WHERE token = '" + token + "') " +
        "ORDER BY freedays.approved ASC, freedays.startDate DESC " +
        " LIMIT " + start + `,` + entries + "";
    } else if (page == 1) {
      query = "SELECT user.name, user.position, user.picture, freedays.* FROM freedays JOIN user " +
        "ON user.userID = freedays.userID WHERE " +
        "freedays.userID = (SELECT user.userID FROM user WHERE token = '" + token + "') " +
        "ORDER BY freedays.approved ASC, freedays.startDate DESC " +
        "LIMIT " + entries + "";
    } else if (all == 1) {
      query = "SELECT user.name, user.position, user.picture, freedays.* FROM freedays JOIN user " +
        "ON user.userID = freedays.userID WHERE " +
        "freedays.userID = (SELECT user.userID FROM user WHERE token = '" + token + "') " +
        "ORDER BY freedays.approved ASC, freedays.startDate DESC ";
    }
    connection.query(query, function (err, rows) {
      if (!err) {
        if (all != 1) {
          rows = JSON.stringify(rows);
          freedays["data"] = JSON.parse(rows);
          res.json(freedays);
          connection.release();
        } else if (all == 1) {
          res.json(rows);
          connection.release();
        }
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//?????
function deleteHoliday(token, id, callback) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("DELETE FROM freedays WHERE id = " + id, function (err, rows) {
      connection.release();
      if (!err) {
        callback(rows);
      }
    });
    connection.on('error', function (err) {
      var err = ({
        "code": 100,
        "status": "Error in connection database"
      });
      callback(err);
    });
  });
}

//delete legal holidays from Romania or Moldavia
//when there is no more a legal holiday
function deleteLegalHoliday(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let query;
    if (req.body.country == 'ro') {
      query = "DELETE FROM `legalholidays` WHERE `id` = " + req.body.id;
    } else {
      query = "DELETE FROM `legalholidaysmd` WHERE `id` = " + req.body.id;
    }
    connection.query(query, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get approved or pending free days for current user
function getFreeDaysApprovedOrPending(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query("SELECT * FROM freedays WHERE approved!=2  AND userID=" + params.userID, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get legal holidays from Romania
function getLegalFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query("SELECT * FROM legalholidays ", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get legal holidays from Moldavia
function getLegalFreeDaysMD(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query("SELECT * FROM legalholidaysmd ", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//count free days for managers
function getAllManagerFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let params = req.query,
      userID = params.userID;
    connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
      " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
      " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
      console.log(err);
      if (!err) {
        res.json(rows);
        connection.release();
        return;
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    })
  })
}

//count free days for current user
function getAllUserFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let params = req.query,
      userID = params.userID;
    connection.query("SELECT COUNT(*) as count FROM freedays WHERE userID = " + userID + "", function (err, rows) {
      console.log(err);
      if (!err) {
        res.json(rows);
        connection.release();
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    })
  })
}

//get free days for calendar
function getFilteredFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let params = req.query;
    let month, viewType = params.viewType, viewDate = params.viewDate, current,
      weekstart, weekend, monday, sunday, day, query, table, country, ids = params.ids, approved = params.approved;

    if (viewType === 'month') {
      month = (new Date(viewDate)).getMonth() + 1;
      year = (new Date(viewDate)).getFullYear();
      query = `SELECT user.userID, user.name, user.country, user.position, user.positionInfo, user.email,
      user.admin, freedays.startDate, freedays.endDate, freedays.days, freedays.type, freedays.comment,
      user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays
      JOIN user ON user.userID=freedays.userID  WHERE
      (JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.ids')
      OR JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.dep.ids')
      OR freedays.userID= ` + params.userID + `) AND freedays.approved IN (` + approved + `)
      AND freedays.userID IN (` + ids + `) AND (MONTH(freedays.startDate)=` + month + ` OR MONTH(freedays.endDate)= ` + month + `)
      AND YEAR(freedays.startDate)= ` + year + ` ORDER BY freedays.startDate AND freedays.approved = 0 DESC`;
    } else if (viewType === 'basicWeek') {
      current = new Date(viewDate);
      weekstart = current.getDate() - current.getDay() + 1;
      weekend = weekstart + 6;
      monday = moment(new Date(current.setDate(weekstart))).format('YYYY-MM-DD');
      sunday = moment(new Date(current.setDate(weekend))).format('YYYY-MM-DD');

      query = `SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, user.email,
    user.admin, freedays.startDate, freedays.endDate, freedays.days, freedays.type, freedays.comment,
      user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays
      JOIN user ON user.userID=freedays.userID  WHERE
      (JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.ids')
      OR JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.dep.ids')
      OR freedays.userID= ` + params.userID + `) AND freedays.approved IN (` + approved + `)
      AND freedays.userID IN (` + ids + `) AND (freedays.startDate BETWEEN '` + monday + `'
      AND '` + sunday + `' OR freedays.endDate BETWEEN '` + monday + `' AND '` + sunday + `')
      ORDER BY freedays.startDate AND freedays.approved = 0 DESC`;

    } else {
      day = moment(viewDate).format('YYYY-MM-DD');
      query = `SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, user.email,
     user.admin, freedays.startDate, freedays.endDate, freedays.days, freedays.type, freedays.comment,
      user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays
       JOIN user ON user.userID=freedays.userID  WHERE
       (JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.ids')
      OR JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.dep.ids')
      OR freedays.userID= ` + params.userID + `) AND freedays.approved IN (` + approved + `)
       AND freedays.userID IN (` + ids + `) AND freedays.startDate='` + day + `'
       ORDER BY freedays.startDate AND freedays.approved = 0 DESC`;
    }
    connection.query(query, function (err, rows) {
      if (!err) {
        res.json(rows);
        connection.release();
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get users name for calendar
function getCalendarUsersName(req, res) {
  let params = req.query;
  let query = `SELECT DISTINCT user.userID, user.name FROM user JOIN freedays ON user.userID=freedays.userID
    WHERE freedays.approved != 2 AND (JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.ids')
      OR JSON_CONTAINS(freedays.userObject, '[{"id":` + params.userID + `}]', '$.dep.ids'))`;


  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query(query, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}


//populate table for supervisors
function getManagerFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
      if (err) {
        res.json({
          "code": 100,
          "status": "Error in connection database"
        });
      }

      var params = req.query,
        userID = params.userID,
        page = params.infoPage,
        entries = params.infoEntries,
        count,
        start = params.start,
        end = params.end,
        all = params.all,
        order = params.orderObject,
        freedays = {},
        dataSearch = params.search,
        dataOrderMap = {
          1: "user.name", 2: "user.position", 3: "user.positionInfo", 4: "user.email",
          5: "freedays.startDate", 6: "freedays.endDate", 7: "freedays.requestDate", 8: "freedays.days",
          9: "freedays.type", 10: "freedays.comment", 11: "user.avfreedays", 12: "freedays.approved",
          13: "freedays.approved"
        };

      if (!dataSearch) {
        if (order[0].column == 0) {
          connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
            " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
            " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
            if (!err) {
              count = rows[0].count;
              if (end > 0 && page > 1) {
                freedays["data"] = [];
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY freedays.approved ASC, freedays.startDate DESC" +
                  " LIMIT " + start + `,` + entries + "";
              } else if (page == 1) {
                freedays["data"] = [];
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY freedays.approved ASC, freedays.startDate DESC" +
                  " LIMIT " + entries + "";
              } else if (all == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY freedays.approved ASC, freedays.startDate DESC";
              }

              connection.query(query, function (err, rows) {
                if (!err) {
                  if (all != 1) {
                    rows = JSON.stringify(rows);
                    freedays = {
                      'draw': req.draw,
                      "recordsTotal": count,
                      "recordsFiltered": count,
                      "data": []
                    };
                    freedays["data"] = JSON.parse(rows);
                    res.json(freedays);
                    connection.release();
                  } else if (all == 1) {
                    res.json(rows);
                    connection.release();
                  }
                }
              })
            }
          });
        } else if (order[0].column !== 0) {
          var dataOrderMapString = '';
          for (var i = 0; i <= Object.keys(dataOrderMap).length; i++) {
            if (order[0].column == i) {
              dataOrderMapString += dataOrderMap[i];
            }
          }
          connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
            " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
            " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
            if (!err) {
              count = rows[0].count;
              if (end > 0 && page > 1) {
                freedays["data"] = [];
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY " + dataOrderMapString + " " + order[0].dir + "" +
                  " LIMIT " + start + `,` + entries + "";
              } else if (page == 1) {
                freedays["data"] = [];
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY " + dataOrderMapString + " " + order[0].dir + "" +
                  " LIMIT " + entries + "";
              } else if (all == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.country, user.position, user.positionInfo," +
                  " user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment," +
                  "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                  "user.userID=freedays.userID WHERE JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")" +
                  "ORDER BY " + dataOrderMapString + " " + order[0].dir + "";
              }

              connection.query(query, function (err, rows) {
                if (!err) {
                  if (all != 1) {
                    rows = JSON.stringify(rows);
                    freedays = {
                      'draw': req.draw,
                      "recordsTotal": count,
                      "recordsFiltered": count,
                      "data": []
                    };
                    freedays["data"] = JSON.parse(rows);
                    res.json(freedays);
                    connection.release();
                  } else if (all == 1) {
                    res.json(rows);
                    connection.release();
                  }
                }
              })
            }
          });
        }
      } else if (dataSearch) {
        freedays["data"] = [];
        query = "SELECT (SELECT COUNT(*) FROM freedays JOIN user ON user.userID=freedays.userID WHERE user.name " +
          "LIKE '%" + dataSearch + "%' AND JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") ORDER BY " +
          "freedays.approved, freedays.startDate DESC) as total, user.userID, user.picture, user.name, user.country, " +
          "user.position, user.positionInfo, user.admin, user.email, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, " +
          "freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM " +
          "freedays JOIN user ON user.userID=freedays.userID WHERE user.name LIKE '%" + dataSearch + "%'" +
          " AND  JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") ORDER BY " +
          "freedays.approved ASC, freedays.startDate DESC LIMIT " + start + `,` + entries + "";

        connection.query(query, function (err, rows) {
          console.log(err);
          var total = 0;
          if (!err) {
            if (rows.length > 0) {
              total = rows[0].total;
              rows = JSON.stringify(rows);

              freedays = {
                'draw': req.draw,
                "recordsTotal": total,
                "recordsFiltered": total,
                "data": []
              };
              freedays["data"] = JSON.parse(rows);
              res.json(freedays);
              connection.release();
            } else if (!rows.length) {
              total = 0;
              freedays = {
                'draw': req.draw,
                "recordsTotal": total,
                "recordsFiltered": total,
                "data": []
              };
              freedays["data"] = [];
              res.json(freedays);
              connection.release();
            }
          }
        })
      }

      connection.on('error', function (err) {
        res.json({
          "code": 100,
          "status": "Error in connection database"
        });
      })
    }
  )
}

//PopulateLocalHr - get free days for local hr
function getLocalHrFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let params = req.query,
      userID = params.userID,
      page = params.infoPage,
      entries = params.infoEntries,
      admin = params.admin,
      country = params.country,
      start = params.start,
      end = params.end,
      all = params.all,
      freedays = {},
      dataSearch = params.search,
      order = params.orderObject,
      dataOrderMap = {
        1: "user.name", 2: "user.position", 3: "user.positionInfo", 4: "user.email",
        5: "freedays.startDate", 6: "freedays.endDate", 7: "freedays.requestDate", 8: "freedays.days",
        9: "freedays.type", 10: "freedays.comment", 11: "user.avfreedays", 12: "freedays.approved",
        13: "freedays.approved"
      };

    if (!dataSearch) {
      if (order[0].column == 0) {
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (admin == 4) {
              if (end > 0 && page > 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, " +
                  "freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, " +
                  "freedays.id FROM freedays JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "' AND user.admin != 4 ORDER BY freedays.approved ASC, freedays.startDate DESC " +
                  " LIMIT " + start + `,` + entries + "";
              } else if (page == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days," +
                  " freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject," +
                  " freedays.id FROM freedays  JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "'AND user.admin != 4 ORDER BY freedays.approved ASC, freedays.startDate DESC" +
                  " LIMIT " + entries + "";
              } else if (all == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days," +
                  " freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, " +
                  "freedays.id FROM freedays JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "'  AND user.admin != 4 ORDER BY freedays.approved ASC, freedays.startDate DESC ";
              }
            }
            connection.query(query, function (err, rows) {
              if (!err) {
                if (all != 1) {
                  rows = JSON.stringify(rows);
                  freedays = {
                    'draw': req.draw,
                    "recordsTotal": count,
                    "recordsFiltered": count,
                    "data": []
                  };
                  freedays["data"] = JSON.parse(rows);
                  res.json(freedays);
                  connection.release();
                } else if (all == 1) {
                  res.json(rows);
                  connection.release();
                }
              }
            })
          }
        })
      } else if (order[0].column !== 0) {
        let dataOrderMapString = '';
        for (let i = 0; i <= Object.keys(dataOrderMap).length; i++) {
          if (order[0].column == i) {
            dataOrderMapString += dataOrderMap[i];
          }
        }
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (admin == 4) {
              if (end > 0 && page > 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type," +
                  " freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays " +
                  "JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "' AND user.admin != 4 ORDER BY " + dataOrderMapString + " " + order[0].dir + "" +
                  " AND freedays.approved = 0 LIMIT " + start + `,` + entries + "";
              } else if (page == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type," +
                  " freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays " +
                  "JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "'AND user.admin != 4 ORDER BY " + dataOrderMapString + "" +
                  " " + order[0].dir + " AND freedays.approved = 0 LIMIT " + entries + "";
              } else if (all == 1) {
                query = "SELECT user.userID, user.picture, user.name, user.position, user.positionInfo, user.email," +
                  " user.country, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type," +
                  " freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays " +
                  "JOIN user ON user.userID=freedays.userID WHERE " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
                  "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") " +
                  "AND user.country = '" + country + "'  AND user.admin != 4 ORDER BY " + dataOrderMapString + " " +
                  "" + order[0].dir + " AND freedays.approved = 0";
              }
            }
            connection.query(query, function (err, rows) {
              if (!err) {
                if (all != 1) {
                  rows = JSON.stringify(rows);
                  freedays = {
                    'draw': req.draw,
                    "recordsTotal": count,
                    "recordsFiltered": count,
                    "data": []
                  };
                  freedays["data"] = JSON.parse(rows);
                  res.json(freedays);
                  connection.release();
                } else if (all == 1) {
                  res.json(rows);
                  connection.release();
                }
              }
            })
          }
        })
      }
    } else if (dataSearch) {
      freedays["data"] = [];
      if (admin == 4) {
        if (end > 0 && page > 1) {
          query = "SELECT (SELECT COUNT(*) FROM freedays JOIN user ON user.userID=freedays.userID " +
            "WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
            "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
            "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND user.country = '" + country + "' " +
            "AND user.admin != 4) as total, user.userID, user.picture, user.name," +
            " user.position, user.positionInfo, user.email, user.country, user.admin, freedays.startDate, " +
            "freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, user.avfreedays, freedays.approved," +
            "freedays.userObject, freedays.id FROM freedays JOIN user ON user.userID=freedays.userID" +
            " WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
            "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
            "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND" +
            " user.country = '" + country + "' AND user.admin != 4" +
            " ORDER BY freedays.approved = 0 ASC, freedays.startDate DESC LIMIT " + start + `,` + entries + "";
        } else if (page == 1) {
          query = "SELECT (SELECT COUNT(*) FROM freedays JOIN user ON user.userID=freedays.userID " +
            "WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
            "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
            "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND user.country = '" + country + "' " +
            "AND user.admin != 4) as total, user.userID, user.picture, user.name," +
            " user.position, user.positionInfo, user.email, user.country, user.admin, freedays.startDate, " +
            "freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, user.avfreedays, freedays.approved," +
            "freedays.userObject, freedays.id FROM freedays JOIN user ON user.userID=freedays.userID" +
            " WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
            "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
            "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND" +
            " user.country = '" + country + "' AND user.admin != 4" +
            " ORDER BY freedays.approved = 0 ASC, freedays.startDate DESC LIMIT " + entries + "";
        }
      }

      connection.query(query, function (err, rows) {
        var total = 0;
        if (!err) {
          if (rows.length > 0) {
            total = rows[0].total;
            rows = JSON.stringify(rows);

            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = JSON.parse(rows);
            res.json(freedays);
            connection.release();
          } else if (!rows.length) {
            total = 0;
            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = [];
            res.json(freedays);
            connection.release();
          }
        }
      })
    }
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//PopulateHr - get free days for hr admin
function getHRFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let params = req.query,
      userID = params.userID,
      page = params.infoPage,
      entries = params.infoEntries,
      start = params.start,
      end = params.end,
      all = params.all,
      freedays = {},
      dataSearch = params.search,
      order = params.orderObject,
      dataOrderMap = {
        1: "user.name",
        2: "user.position",
        3: "user.positionInfo",
        4: "user.email",
        5: "freedays.startDate",
        6: "freedays.endDate",
        7: "freedays.requestDate",
        8: "freedays.days",
        9: "freedays.type",
        10: "freedays.comment",
        11: "user.avfreedays",
        12: "freedays.approved",
        13: "freedays.approved"
      };

    if (!dataSearch) {
      if (order[0].column == 0) {
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (end > 0 && page > 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY freedays.approved ASC, freedays.startDate DESC" +
                " LIMIT " + start + `,` + entries;
            } else if (page == 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY freedays.approved ASC, freedays.startDate DESC" +
                " LIMIT " + entries;
            } else if (all == 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY freedays.approved ASC, freedays.startDate DESC";
            }
          }
          connection.query(query, function (err, rows) {
            if (!err) {
              if (all != 1) {
                rows = JSON.stringify(rows);
                freedays = {
                  'draw': req.draw,
                  "recordsTotal": count,
                  "recordsFiltered": count,
                  "data": []
                };
                freedays["data"] = JSON.parse(rows);
                res.json(freedays);
                connection.release();
                return;
              } else if (all == 1) {
                res.json(rows);
                connection.release();
                return;
              }
            }
            connection.on('error', function (err) {
              res.json({
                "code": 100,
                "status": "Error in connection database"
              });
            })
          })
        });
      } else if (order[0].column !== 0) {
        let dataOrderMapString = '';
        for (let i = 0; i <= Object.keys(dataOrderMap).length; i++) {
          if (order[0].column == i) {
            dataOrderMapString += dataOrderMap[i];
          }
        }
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (end > 0 && page > 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY " + dataOrderMapString + " " + order[0].dir + "" +
                " LIMIT " + start + `,` + entries + "";
            } else if (page == 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY " + dataOrderMapString + " " + order[0].dir + "" +
                " LIMIT " + entries + "";
            } else if (all == 1) {
              query = "SELECT user.userID, user.name, user.picture, user.country, user.position, user.positionInfo, " +
                "user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user ON " +
                "user.userID=freedays.userID  WHERE " +
                "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ") AND " +
                "user.position != 'HR - admin' ORDER BY " + dataOrderMapString + " " + order[0].dir + "";
            }
          }
          connection.query(query, function (err, rows) {
            if (!err) {
              if (all != 1) {
                rows = JSON.stringify(rows);
                freedays = {
                  'draw': req.draw,
                  "recordsTotal": count,
                  "recordsFiltered": count,
                  "data": []
                };
                freedays["data"] = JSON.parse(rows);
                res.json(freedays);
                connection.release();
                return;
              } else if (all == 1) {
                res.json(rows);
                connection.release();
                return;
              }
            }
            connection.on('error', function (err) {
              res.json({
                "code": 100,
                "status": "Error in connection database"
              });
            })
          })
        });
      }
    } else if (dataSearch) {
      if (end > 0 && page > 1) {
        query = "SELECT (SELECT COUNT(*)FROM freedays JOIN user ON user.userID=freedays.userID  WHERE " +
          "(user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND" +
          " user.position != 'HR - admin') as total, user.userID, user.name, user.picture, user.country, user.position, " +
          "user.positionInfo, user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, " +
          "freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id " +
          "FROM freedays JOIN user ON user.userID=freedays.userID  WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND " +
          "user.position != 'HR - admin' ORDER BY freedays.approved ASC, freedays.startDate DESC " +
          " LIMIT " + start + `,` + entries + "";
      } else if (page == 1) {
        query = "SELECT (SELECT COUNT(*)FROM freedays JOIN user ON user.userID=freedays.userID  WHERE " +
          "(user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND" +
          " user.position != 'HR - admin') as total, user.userID, user.name, user.picture, user.country, user.position, " +
          "user.positionInfo, user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, " +
          "freedays.type, freedays.comment, user.avfreedays, freedays.approved, freedays.userObject, freedays.id " +
          "FROM freedays JOIN user ON user.userID=freedays.userID  WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")) AND " +
          "user.position != 'HR - admin' ORDER BY freedays.approved ASC, freedays.startDate DESC " +
          "LIMIT " + entries + "";
      }
      connection.query(query, function (err, rows) {
        var total = 0;
        if (!err) {
          if (rows.length > 0) {
            total = rows[0].total;
            rows = JSON.stringify(rows);

            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = JSON.parse(rows);
            res.json(freedays);
            connection.release();
          } else if (!rows.length) {
            total = 0;
            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = [];
            res.json(freedays);
            connection.release();
          }
        }
      })
    }
  });
}

//PopulateAdmin - get free days for administrator
function getAdminFreeDays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    let query, count,
      params = req.query,
      userID = params.userID,
      page = params.infoPage,
      entries = params.infoEntries,
      start = params.start,
      end = params.end,
      all = params.all,
      freedays = {},
      dataSearch = params.search,
      order = params.orderObject,
      dataOrderMap = {
        1: "user.name",
        2: "user.position",
        3: "user.positionInfo",
        4: "user.email",
        5: "freedays.startDate",
        6: "freedays.endDate",
        7: "freedays.requestDate",
        8: "freedays.days",
        9: "freedays.type",
        10: "freedays.comment",
        11: "user.avfreedays",
        12: "freedays.approved",
        13: "freedays.approved"
      };

    if (!dataSearch) {
      if (order[0].column == 0) {
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (end > 0 && page > 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY freedays.approved ASC, freedays.startDate DESC LIMIT " + start + `,` + entries + "";
            } else if (page == 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY freedays.approved ASC, freedays.startDate DESC LIMIT " + entries + "";
            } else if (all == 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY freedays.approved ASC, freedays.startDate DESC ";
            }
            connection.query(query, function (err, rows) {
              if (!err) {
                if (all != 1) {
                  rows = JSON.stringify(rows);
                  freedays = {
                    'draw': req.draw,
                    "recordsTotal": count,
                    "recordsFiltered": count,
                    "data": []
                  };
                  freedays["data"] = JSON.parse(rows);
                  res.json(freedays);
                  connection.release();
                  return;
                } else if (all == 1) {
                  res.json(rows);
                  connection.release();
                  return;
                }
              }
              connection.on('error', function (err) {
                res.json({
                  "code": 100,
                  "status": "Error in connection database"
                });
              })
            })
          }
        });
      } else if (order[0].column !== 0) {
        let dataOrderMapString = '';
        for (let i = 0; i <= Object.keys(dataOrderMap).length; i++) {
          if (order[0].column == i) {
            dataOrderMapString += dataOrderMap[i];
          }
        }
        connection.query("SELECT COUNT(*) as count FROM freedays WHERE " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]'," + '"$.ids"' + ")", function (err, rows) {
          if (!err) {
            count = rows[0].count;
            if (end > 0 && page > 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY " + dataOrderMapString + " " + order[0].dir + " " +
                " LIMIT " + start + `,` + entries + "";
            } else if (page == 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY " + dataOrderMapString + " " + order[0].dir + " " +
                "LIMIT " + entries + "";
            } else if (all == 1) {
              query = "SELECT user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo," +
                " user.email, user.admin, freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, " +
                "user.avfreedays, freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user " +
                "ON user.userID=freedays.userID WHERE" +
                " JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.dep.ids"' + ") " +
                "OR JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + params.userID + "}]', " + '"$.ids"' + ") " +
                "ORDER BY " + dataOrderMapString + " " + order[0].dir + " ";
            }
            connection.query(query, function (err, rows) {
              if (!err) {
                if (all != 1) {
                  rows = JSON.stringify(rows);
                  freedays = {
                    'draw': req.draw,
                    "recordsTotal": count,
                    "recordsFiltered": count,
                    "data": []
                  };
                  freedays["data"] = JSON.parse(rows);
                  res.json(freedays);
                  connection.release();
                  return;
                } else if (all == 1) {
                  res.json(rows);
                  connection.release();
                  return;
                }
              }
              connection.on('error', function (err) {
                res.json({
                  "code": 100,
                  "status": "Error in connection database"
                });
              })
            })
          }
        });
      }
    } else if (dataSearch) {
      if (end > 0 && page > 1) {
        query = "SELECT (SELECT COUNT(*) FROM freedays JOIN user ON user.userID=freedays.userID  " +
          "WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + "))) as total," +
          " user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo, user.email, user.admin," +
          " freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, user.avfreedays, " +
          "freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user" +
          " ON user.userID=freedays.userID WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")) " +
          "ORDER BY freedays.startDate AND" +
          " freedays.approved = 0 DESC LIMIT " + start + `,` + entries + "";
      } else if (page == 1) {
        query = "SELECT (SELECT COUNT(*) FROM freedays JOIN user ON user.userID=freedays.userID  " +
          "WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + "))) as total," +
          " user.userID, user.name ,user.picture, user.country, user.position, user.positionInfo, user.email, user.admin," +
          " freedays.startDate, freedays.endDate, freedays.requestDate, freedays.days, freedays.type, freedays.comment, user.avfreedays, " +
          "freedays.approved, freedays.userObject, freedays.id FROM freedays JOIN user" +
          " ON user.userID=freedays.userID WHERE (user.name LIKE '%" + dataSearch + "%') AND " +
          "(JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ") OR " +
          "JSON_CONTAINS(freedays.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ")) " +
          "ORDER BY freedays.startDate AND" +
          " freedays.approved = 0 DESC LIMIT " + entries + "";
      }
      connection.query(query, function (err, rows) {
        console.log(err);
        var total = 0;
        if (!err) {
          if (rows.length > 0) {
            total = rows[0].total;
            rows = JSON.stringify(rows);

            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = JSON.parse(rows);
            res.json(freedays);
            connection.release();
          } else if (!rows.length) {
            total = 0;
            freedays = {
              'draw': req.draw,
              "recordsTotal": total,
              "recordsFiltered": total,
              "data": []
            };
            freedays["data"] = [];
            res.json(freedays);
            connection.release();
          }
        }
      })
    }
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//updates the user information
function ManagerEditUserForm(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    var query, queryStr,
      params = req.query,
      userUpdated = params.userId,
      userActive = params.userActive,
      newManager = params.new_manager;
    if (params.password != "") {
      if (params.position == 'Supervisor') {
        queryStr = "UPDATE user SET password='" + params.password + "', name='" + params.name +
          "', position='" + params.position + "', positionInfo='" + params.positionInfo + "'," +
          " email='" + params.email + "',admin='1', phone='" + params.phone + "'," +
          " isActive='" + params.userActive + "', avfreedays=" + params.avfreedays + " - bonus + '" +
          params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId
      } else if (params.position == 'Local HR') {
        queryStr = "UPDATE user SET password='" + params.password + "', name='" + params.name +
          "', position='" + params.position + "', positionInfo='" + params.positionInfo + "', email='" + params.email + "'," +
          "admin='4', phone='" + params.phone + "', isActive='" + params.userActive + "', avfreedays=" + params.avfreedays + " - bonus + '" +
          params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
        query = "SELECT userID, userObject FROM user WHERE country = '" + params.country + "' AND admin IN (0, 1)";
      } else if (params.position == 'HR - admin') {
        queryStr = "UPDATE user SET password='" + params.password + "', name='" + params.name +
          "', position='" + params.position + "', positionInfo='" + params.positionInfo + "', email='" + params.email + "'," +
          "admin='3', phone='" + params.phone + "', isActive='" + params.userActive + "', avfreedays=" + params.avfreedays + " - bonus + '" +
          params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
        query = "SELECT userID, userObject FROM user WHERE admin IN (0, 1, 4)";
      } else {
        queryStr = "UPDATE user SET password='" + params.password + "', name='" + params.name +
          "', position='" + params.position + "', positionInfo='" + params.positionInfo + "', email='" + params.email + "'," +
          "admin='0', phone='" + params.phone + "', isActive='" + params.userActive + "', avfreedays=" + params.avfreedays + " - bonus + '" +
          params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
      }
    } else {
      if (params.position == 'Supervisor') {
        queryStr = "UPDATE user SET name='" + params.name + "', position='" + params.position + "'," +
          " positionInfo='" + params.positionInfo + "', email='" + params.email + "',admin='1'," +
          " phone='" + params.phone + "', isActive='" + params.userActive + "'," +
          " avfreedays=" + params.avfreedays + "  - bonus + '" + params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId
      } else if (params.position == 'Local HR') {
        queryStr = "UPDATE user SET name='" + params.name + "', position='" + params.position + "'," +
          " positionInfo='" + params.positionInfo + "', email='" + params.email + "',admin='4'," +
          " phone='" + params.phone + "', isActive='" + params.userActive + "', " +
          "avfreedays=" + params.avfreedays + " - bonus + '" + params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
        query = "SELECT userID, userObject FROM user WHERE country = '" + params.country + "' AND admin IN (0, 1)";
      } else if (params.position == 'HR - admin') {
        queryStr = "UPDATE user SET name='" + params.name + "', position='" + params.position + "'," +
          " positionInfo='" + params.positionInfo + "', email='" + params.email + "',admin='3', " +
          "phone='" + params.phone + "', isActive='" + params.userActive + "'," +
          " avfreedays=" + params.avfreedays + " - bonus + '" + params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
        query = "SELECT userID, userObject FROM user WHERE admin IN (0, 1, 4)";
      } else {
        queryStr = "UPDATE user SET name='" + params.name + "', position='" + params.position + "'," +
          " positionInfo='" + params.positionInfo + "', email='" + params.email + "',admin='1'," +
          " phone='" + params.phone + "', isActive='" + params.userActive + "'," +
          " avfreedays=" + params.avfreedays + " - bonus + '" + params.bonus + "', bonus='" + params.bonus +
          "' WHERE user.userID=" + params.userId;
      }
    }

    connection.query(queryStr, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
        if (userActive == 0) {
          connection.query("SELECT userID, userObject FROM user  WHERE " +
            "JSON_CONTAINS(userObject, '[{\"id\" : " + params.userId + "}]', '$.ids') OR " +
            "JSON_CONTAINS(userObject, '[{\"id\" : " + params.userId + "}]', '$.dep.ids') " +
            "AND user.admin != 2", function (err, rows) {
            if (!err) {
              for (var i = 0; i < rows.length; i++) {
                var userId = rows[i]['userID'],
                  userObj = JSON.parse(rows[i]['userObject']);
                if (userObj['ids'].length > 0) {
                  for (var j = 0; j < userObj['ids'].length; j++) {
                    if ((userObj['ids'][j]['id'] == userUpdated) || userObj['ids'][j]['id'] == newManager) {
                      userObj['ids'].splice(j, 1);
                    } else if (userObj['ids'][j]['id'] == userUpdated) {
                      userObj['ids'][j]['id'] = parseInt(newManager);
                    }

                  }
                }

                if (userObj['ids'].length < 1) {
                  userObj['ids'] = userObj['dep']['ids'];
                  userObj['rel'] = 'OR';
                  userObj['dep'] = {};
                }

                if (userObj['dep']['ids']) {
                  for (var j = 0; j < userObj['dep']['ids'].length; j++) {
                    if (userObj['dep']['ids'][j]['id'] == userUpdated) {
                      userObj['dep']['ids'].splice(j, 1);
                    }
                  }
                }
                var queryStr = "UPDATE user SET userObject ='" + JSON.stringify(userObj) + "' WHERE userID=  " + userId,
                  getObjects = "UPDATE freedays SET userObject ='" + JSON.stringify(userObj) + "'" +
                    " WHERE userID=  " + userId + " AND approved = '0'";

                connection.query(queryStr, function (err, rows) {
                });

                connection.query(getObjects, function (err, rows) {
                });
              }

            }
          });
        } else {
          if (query) {
            addInObject(query, userUpdated)
          }
        }
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//update all holidays from Romania
function updateHolidays(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE legalholidays SET startDate='" + params.startDate + "', name='" + params.name + "'," +
      " type='" + params.type + "' WHERE id=" + params.id, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//update all holidays from Moldavia
function updateHolidaysMD(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE legalholidaysmd SET startDate='" + params.startDate + "', name='" + params.name + "', " +
      "type='" + params.type + "' WHERE id=" + params.id, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//update user's information
function updateUser(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    var params = req.body, queryStr;

    if (!phoneValidation(params.phone)) {
      res.status(400).send('Invalid phone number');
      return;
    }

    if (params.password == '') {
      queryStr = "UPDATE user SET name ='" + params.name + "', phone = '" + params.phone + "' " +
        "WHERE userID=  " + params.userId;
    } else {
      queryStr = "UPDATE user SET name ='" + params.name + "', password = '" + params.password + "', " +
        "phone = '" + params.phone + "' WHERE userID=  " + params.userId;
    }
    connection.query(queryStr, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      } else console.log(err);
    });

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//update user object
function updateUserObject(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    var params = req.query,
      queryStr = "UPDATE user SET userObject ='" + params.userObject + "' WHERE userID=  " + params.userID,
      getObjects = "UPDATE freedays SET userObject ='" + params.userObject + "' WHERE" +
        " userID=  " + params.userID + " AND approved = '0'";

    connection.query(queryStr, function (err, rows) {
      connection.release();
      if (!err) {
        connection.query(getObjects, function (err, rows) {
        });
        res.json(rows);
      } else console.log(err);
    });

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//upload an image
function upload(req, res) {
  var params = req.body;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    var sampleFile = req.files.profileImage.data.toString("base64");
    connection.query("UPDATE user SET picture ='" + sampleFile + "' WHERE userID = " + params.id, function (err, rows) {
      connection.release();

      if (!err) {
        res.json(sampleFile);
      }
    });
    connection.on('error', function (err) {
      console.log(err);
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

function handle_dateupdate(data, callback) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let requestDate = moment().format('YYYY/MM/DD'), query;
    let post = {
      startDate: data.stdate, endDate: data.enddate, requestDate: requestDate, days: data.days, approved:
        '0', userID: data.userID, type: data.vacationtype, comment: data.comment, userObject: data.userObject
    };
    connection.query('INSERT INTO freedays SET ?', post, function (err, rows) {
      connection.release();
      if (err) {
        console.log(err);
      }
      sendHolidayEmailToUser(data.emailList, data.name, data.stdate, data.enddate, data.days, rows['insertId']);
      let eventObj = {
        'userId': data.userID,
        'insertId': rows.insertId,
        'stdate': data.stdate,
        'enddate': data.enddate,
        'userObj': data.userObj,
        'type': data.vacationtype
      };
      callback(eventObj);
    });
    connection.on('error', function (err) {
      var err = ({
        "code": 100,
        "status": "Error in connection database"
      });
      console.log(err);
    });
  });
}

function refreshManagedUserTable(data, callback) {
  callback('{"status": "Managed User Table refreshed"}');
}

function RefreshCalendar(data, callback) {
  callback('{"status": "Calendar refreshed"}');
}

//logout
function logout(req, res) {
  var params = req.body;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE user SET token = '' WHERE email ='" + params.email + "'", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//add's user
function addUser(req, res) {
  var params = req.body, columns, admin, query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    if (params.position == 'Supervisor') {
      admin = 1;
    } else if (params.position == 'Local HR') {
      admin = 4;
      query = "SELECT userID, userObject FROM user WHERE country = '" + params.country + "' AND admin IN (0, 1)";
    } else if (params.position == 'Administrator') {
      admin = 6;
      query = "SELECT userID, userObject FROM user WHERE admin NOT IN (2, 6)";
    } else if (params.position == 'HR - admin') {
      admin = 3;
      query = "SELECT userID, userObject FROM user WHERE admin IN (0, 1, 4)";
    } else {
      admin = 0;
    }

    columns = "'" + params.email + "', '" + params.password + "', '" + params.age + "', " +
      "'" + params.name + "', '" + params.country + "', '" + params.position + "', '" +
      params.positionInfo + "', '" + params.phone + "', '" + params.stwork + "', '" + admin + "','" +
      "" + params.avfreedays + "','" + params.userObject + "'";

    connection.query("INSERT INTO user (email, password, age, name, country, position, positionInfo, " +
      "phone, startDate, admin, avfreedays, userObject) VALUES (" + columns + ")", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
        addInObject(query, rows.insertId);
      } else {
        res.json(err);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}


function addInObject(query, insertedId) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query(query, function (err, rows) {
      if (!err) {
        for (var i = 0; i < rows.length; i++) {
          var userObj = JSON.parse(rows[i]['userObject']);
          if (userObj['dep']['ids']) {
            userObj['dep']['ids'].push({
              "id": parseInt(insertedId)
            })
          } else {
            userObj['ids'].push({
              "id": parseInt(insertedId)
            })
          }
          var queryStr = "UPDATE user SET userObject ='" + JSON.stringify(userObj) + "' " +
            "WHERE userID=  " + rows[i]['userID'];

          connection.query(queryStr, function (err, rows) {
          });
        }
        connection.release();
      }
    });
  });
}

// get all users for managed user table
function getAllUsers(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    var queryString,
      params = req.query,
      inactiveUsers = params.inactiveUsers === 'true' ? "0, 1" : 1,
      admin = params.admin,
      userID = params.userID,
      page = params.infoPage,
      entries = params.infoEntries,
      count,
      start = params.start,
      end = params.end,
      all = params.all,
      order = params.orderObject,
      freedays = {},
      dataSearch = params.search,
      dataOrderMap = {
        1: "user.name", 2: "user.country", 3: "user.position", 4: "user.positionInfo", 5: "user.email",
        6: "user.startDate", 7: "user.phone", 8: "freedays.days", 9: "user.isActive", 10: "user.age",
        11: "user.avfreedays", 12: "user.bonus"
      };


    if (admin == '1' && !dataSearch) {
      if (order[0].column == 0) {
        connection.query("SELECT COUNT(*) as total FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
          "AND isActive IN (" + inactiveUsers + ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
          "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))", function (err, rows) {
            if (!err) {
              count = rows[0].total;
              if (end > 0 && page > 1) {
                queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                  "AND isActive IN (" + inactiveUsers +
                  ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                  "OR JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                  " AND user.admin != 3 AND user.admin != 6 AND user.admin != 2 ORDER BY user.isActive DESC LIMIT " + start + `,` + entries + "";
              } else if (page == 1) {
                queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                  "AND isActive IN (" + inactiveUsers + ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                  "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) " +
                  "AND user.admin != 3 AND user.admin != 6 AND user.admin != 2 ORDER BY user.isActive DESC LIMIT " + entries + "";
              }
              connection.query(queryString, function (err, rows) {
                if (!err) {
                  if (all != 1) {
                    rows = JSON.stringify(rows);
                    freedays = {
                      'draw': req.draw,
                      "recordsTotal": count,
                      "recordsFiltered": count,
                      "data": []
                    };
                    freedays["data"] = JSON.parse(rows);
                    res.json(freedays);
                    connection.release();
                  } else if (all == 1) {
                    res.json(rows);
                    connection.release();
                  }
                }
              });
            }
          }
        );
      } else if (order[0].column !== 0) {
        var dataOrderMapString = '';
        for (var i = 0; i <= Object.keys(dataOrderMap).length; i++) {
          if (order[0].column == i) {
            dataOrderMapString += dataOrderMap[i];
          }
        }
        connection.query("SELECT COUNT(*) as total FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) AND isActive IN (" + inactiveUsers + ") AND" +
          "(JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
          "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))", function (err, rows) {
          if (!err) {
            count = rows[0].total;
            if (end > 0 && page > 1) {
              queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                "AND isActive IN (" + inactiveUsers + ") AND (" +
                " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR" +
                " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                " ORDER BY " + dataOrderMapString + " " + order[0].dir + "  LIMIT " + start + `,` + entries + "";
            } else if (page == 1) {
              queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                "AND isActive IN (" + inactiveUsers + ") AND (" +
                "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR" +
                " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                " ORDER BY " + dataOrderMapString + " " + order[0].dir + " LIMIT " + entries + "";
            }
            connection.query(queryString, function (err, rows) {
              console.log(err);
              if (!err) {
                if (all != 1) {
                  rows = JSON.stringify(rows);
                  freedays = {
                    'draw': req.draw,
                    "recordsTotal": count,
                    "recordsFiltered": count,
                    "data": []
                  };
                  freedays["data"] = JSON.parse(rows);
                  res.json(freedays);
                  connection.release();
                } else if (all == 1) {
                  res.json(rows);
                  connection.release();
                }
              }
            });
          }
        });
      }
    } else {
      if (!dataSearch) {
        if (order[0].column == 0) {
          connection.query("SELECT COUNT(*) as total FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) AND" +
            " (user.admin NOT IN (" + admin + ")) AND isActive IN (" + inactiveUsers +
            ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
            "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))",
            function (err, rows) {
              if (!err) {
                count = rows[0].total;
                if (end > 0 && page > 1) {
                  queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                    "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                    ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                    "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                    " ORDER BY user.isActive DESC";
                  if (params.country !== '') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL )" +
                      " AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                      "OR JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY user.isActive DESC " +
                      " LIMIT " + start + `,` + entries + "";
                  }
                  if ((admin == '3') || (admin == '6')) {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                      "OR JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY user.isActive DESC LIMIT " + start + `,` + entries + "";
                  }
                  if (admin == '1') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") " +
                      "OR JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY user.isActive DESC LIMIT " + start + `,` + entries + "";
                  }
                } else if (page == 1) {
                  queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                    "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                    ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                    "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) " +
                    "ORDER BY user.isActive DESC LIMIT " + entries + "";
                  if (params.country !== '') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL )" +
                      " AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                      "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY user.isActive DESC";
                  }
                  if ((admin == '3') || (admin == '6')) {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND user.admin NOT IN (2,6) AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                      "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) " +
                      "ORDER BY user.isActive DESC LIMIT " + entries + "";
                  }
                  if (admin == '1') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND isActive IN (" + inactiveUsers + ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                      "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) " +
                      "ORDER BY user.isActive DESC";
                  }
                }
                connection.query(queryString, function (err, rows) {
                  if (!err) {
                    if (all != 1) {
                      rows = JSON.stringify(rows);
                      freedays = {
                        'draw': req.draw,
                        "recordsTotal": count,
                        "recordsFiltered": count,
                        "data": []
                      };
                      freedays["data"] = JSON.parse(rows);
                      res.json(freedays);
                      connection.release();
                    } else if (all == 1) {
                      res.json(rows);
                      connection.release();
                    }
                  }
                });
              }
            }
          );
        } else if (order[0].column !== 0) {
          var dataOrderMapString = '';
          for (var i = 0; i <= Object.keys(dataOrderMap).length; i++) {
            if (order[0].column == i) {
              dataOrderMapString += dataOrderMap[i];
            }
          }
          connection.query("SELECT COUNT(*) as total FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) AND" +
            " (user.admin NOT IN (" + admin + ")) AND isActive IN (" + inactiveUsers +
            ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
            "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))",
            function (err, rows) {
              if (!err) {
                count = rows[0].total;
                if (end > 0 && page > 1) {
                  queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                    "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                    ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                    "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) ORDER BY" +
                    " " + dataOrderMapString + " " + order[0].dir + "";
                  if (params.country !== '') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL )" +
                      " AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                      "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + ")) ORDER BY " +
                      "" + dataOrderMapString + " " + order[0].dir + "" +
                      " LIMIT " + start + `,` + entries + "";
                  }
                  if ((admin == '2') || (admin == '6')) {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR" +
                      " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY " + dataOrderMapString + " " + order[0].dir + "  LIMIT " + start + `,` + entries + "";
                  }
                } else if (page == 1) {
                  queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                    "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                    ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR " +
                    "JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                    " ORDER BY " + dataOrderMapString + " " + order[0].dir + " LIMIT " + entries + "";
                  if (params.country !== '') {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL )" +
                      " AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR" +
                      " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY " + dataOrderMapString + " " + order[0].dir + " LIMIT " + entries + "";
                  }
                  if ((admin == '2') || (admin == '6')) {
                    queryString = "SELECT * FROM user WHERE (token != '" + params.token + "' OR token IS NULL ) " +
                      "AND user.admin NOT IN (" + admin + ") AND isActive IN (" + inactiveUsers +
                      ") AND (JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.ids"' + ") OR" +
                      " JSON_CONTAINS(user.userObject, '[{" + '"id"' + ":" + userID + "}]', " + '"$.dep.ids"' + "))" +
                      " ORDER BY " + dataOrderMapString + " " + order[0].dir + " LIMIT " + entries + "";
                  }
                }
                connection.query(queryString, function (err, rows) {
                  if (!err) {
                    if (all != 1) {
                      rows = JSON.stringify(rows);
                      freedays = {
                        'draw': req.draw,
                        "recordsTotal": count,
                        "recordsFiltered": count,
                        "data": []
                      };
                      freedays["data"] = JSON.parse(rows);
                      res.json(freedays);
                      connection.release();
                    } else if (all == 1) {
                      res.json(rows);
                      connection.release();
                    }
                  }
                });
              }
            });
        }
      } else if (dataSearch) {
        freedays["data"] = [];
        if (admin === '3' || admin === '6' || admin === '1') {
          if (admin === '1') {
            queryString = `SELECT * FROM user WHERE (token != "${params.token}" OR token IS NULL) AND user.admin NOT IN (2,6) AND isActive IN (${inactiveUsers}) AND user.name LIKE "%${dataSearch}%" AND (JSON_CONTAINS(user.userObject, '[{"id":${userID}}]', "$.ids") OR JSON_CONTAINS(user.userObject, '[{"id":${userID}}]', "$.dep.ids")) ORDER BY user.isActive DESC`;
          } else {
            queryString = `SELECT * FROM user WHERE (token != '${params.token}' OR token IS NULL) AND user.admin NOT IN (2,6) AND isActive IN (${inactiveUsers}) AND user.name LIKE '%${dataSearch}%' ORDER BY user.isActive DESC`;
          }
        }

        connection.query(queryString, function (err, rows) {
          if (!err) {
            rows = JSON.stringify(rows);
            freedays = {
              'draw': req.draw,
              "recordsTotal": JSON.parse(rows).length,
              "recordsFiltered": JSON.parse(rows).length,
              "data": []
            };
            freedays["data"] = JSON.parse(rows);
            res.json(freedays);
            connection.release();
          }
        });
      }
    }

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//get all holidays from Romania or Moldavia
function getAllHolidays(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let query;
    if (req.query.country == 'ro') {
      query = "SELECT * FROM legalholidays ORDER BY startDate";
    } else {
      query = "SELECT * FROM legalholidaysmd ORDER BY startDate";
    }
    connection.query(query, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//function that verify if the token from one user is valid
function isValidToken(token) {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        return reject({
          msg: 'connection error'
        });
      }
      connection.query("SELECT * FROM user WHERE token ='" + token + "'", function (err, rows) {
        connection.release();
        if (!err) {
          if (rows.length > 0) {
            return resolve(rows);
          } else {
            return reject({
              msg: 'nu exista token'
            });
          }
        }
      });
      connection.on('error', function (err) {
        return reject({
          msg: 'connection error'
        });
      });
    });
  });
}

//get local hr id's
function userIdLocalHr(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT user.userID, user.name FROM user WHERE user.admin = 4 AND user.isActive = 1 AND " +
      "user.country='" + params.country + "'", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get supervisors name
function getSupervisorsName(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    let query = "SELECT user.userID, user.name, user.position, user.email FROM user WHERE user.admin >= 1";
    connection.query(query, function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      } else console.log(err);
    });

    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  })
}

//get hr admin and administrator ids
function userIdHrAdminAndAdministrator(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT user.userID, user.name, user.admin FROM user " +
      "WHERE user.isActive = 1 AND (user.admin = 3 OR user.admin = 6)", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//update available free days for all users
function updateAllAvFreeDays(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE  user SET avfreedays = avfreedays + " + params.avfreedays + " WHERE isActive = 1", function (err, rows) {
      connection.release();
      if (!err) {
        io.emit('/resNewYear', req.query.token);
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    })
  })
}

//update emailSent in db
function setEmailSentInDatabase(id) {

  pool.getConnection(function (err, connection) {
    if (err) {
      return;
    }

    var freeDayID = id;
    connection.query("UPDATE freedays SET emailSent = 1 WHERE id=" + freeDayID, function (err, rows) {
      connection.release();
    });
    connection.on('error', function (err) {
      console.log(err);
    });
  });
}

//sends email to user
function sendEmailToUser(status, email, type, comment, id) {
  var ok = 0;
  comment = comment.replace(/%1%/g, "`")
    .replace(/%2%/g, '"')
    .replace(/%3%/g, "'")
    .replace(/%4%/g, '\\');
  if (status == 2 && email != "undefined") {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'Holiday Request status changed', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague! <br>
       <br> Your holiday request has been rejected with the following reason: <br>
       <br><i> " ` + comment + ` " </i><br>
       <br> Please click on <a href="` + linkServer + `"> this link </a>
      in order to access the tool and see the details regarding your holiday request.<br><br><br><br>
      <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
    ok++;
  } else if (status == 1 && email != "undefined" && type == "Concediu") {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'Holiday Request status changed', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague! <br>
      <br> Your holiday request has been approved, we wish you a pleasant holiday!
      <br> Please click on <a href="` + linkServer + `"> this link </a>
      in order to access the tool and see the details regarding your holiday request.<br><br><br><br>
      <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
    ok++;
  } else if (status == 1 && email != "undefined" && type != "Concediu") {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'Holiday Request status changed', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague! <br>
      <br> Your holiday request has been approved.
      <br> Please click on <a href="` + linkServer + `"> this link </a>
      in order to access the tool and see the details regarding your holiday request.<br><br><br><br>
      <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
    ok++;
  } else if (!email || status == 0) {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '', // list of receivers (who receives)
      subject: 'Holiday Request status changed', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague! <br>
       <br> Your holiday request has been approved.
       <br> Please click on <a href="` + linkServer + `">
       this link </a> in order to access the tool and see the details regarding your holiday request.<br><br><br><br>
       <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
    ok = 0;
  }

  if (ok == 1) {
    ok++;
    setEmailSentInDatabase(id);
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      } else {
        transporter.close();
      }
    });
  }
}

//send email to managers
function sendHolidayEmailToUser(email, name, start, end, days, requestId) {

  let month = moment(new Date(start)).format('M');

  linkServer += requestId + '&month=' + month;
  if (days > 1) {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'New Holiday Request', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague!<br>
             <br> You have received a holiday request from ` + name + `.
             <br> The holiday request was created for a period of ` + days + ` days, between ` + start + ` - ` + end + `.
             <br> Please click on <a href="` + linkServer + `">
             this link </a> in order to access the tool and see the holiday request.<br><br><br><br>
             <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
  } else if (days == 1) {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'New Holiday Request', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague!<br>
             <br> You have received a holiday request from ` + name + `.
             <br> The holiday request was created for ` + start + `.
             <br> Please click on  <a href="` + linkServer + `">
             this link </a> in order to access the tool and see the holiday request.<br><br><br><br>
             <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
  }
  linkServer = 'http://5.2.197.121:70/public/html/display.html?calendar&hid=';

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return console.log(error);
    } else {
      transporter.close();
    }
  });
}

//send email when supervisors are changed
function sendEmailWhenChangeSupervisors(req, res) {
  var email = req.query.emailUser,
    supervisors = req.query.supervisors;

  if (!supervisors.includes(',')) {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'Supervisor changes', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague!<br>
               <br> You have received this email as your supervisor has been changed.
               <br> Your new direct supervisor is: ` + supervisors + `. <br><br><br><br>
               <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
  } else {
    mailOptions = {
      from: 'vacationtracker@avangarde-software.com', // sender address (who sends)
      to: '' + email + '', // list of receivers (who receives)
      subject: 'Supervisor changes', // Subject line
      text: '', // plaintext body
      html: `Hello Avangarde colleague!<br>
               <br> You have received this email as your supervisors have been changed.
               <br> Your new direct supervisors are: ` + supervisors + `. <br><br><br><br>
               <img src="cid:file:///` + bannerServer + `">`, // html body
      attachments: [{
        filename: 'banner.png',
        path: '' + bannerServer + '',
        cid: 'file:///' + bannerServer + '' //same cid value as in the html img src
      }]
    };
  }

  transporter.sendMail(mailOptions, function (error, info) {
    res.json({
      "0": "Email"
    });
    if (error) {
      return console.log(error);
    } else {
      transporter.close();
    }
  });
}

function setApproved(status, id, approverComment, type, email, approved, avfreedays, admin, userID, userObject, callback) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    function getObjects(userObject, key, val) {
      for (var i in userObject.ids) {
        if (approved == 1) {
          if (userObject.ids[i].id == userID) {
            userObject.ids[i].approved = 1;
            userObject.ids[i].reason = "Approved";
          }
        } else if (approved == 2) {
          if (userObject.ids[i].id == userID) {
            userObject.ids[i].approved = 2;
            userObject.ids[i].reason = approverComment;
          }
        }
      }
      for (var i in userObject.dep.ids) {
        if (approved == 1) {
          if (userObject.dep.ids[i].id == userID) {
            userObject.dep.ids[i].approved = 1;
            userObject.dep.ids[i].reason = "Approved";
          }
        } else if (approved == 2) {
          if (userObject.dep.ids[i].id == userID) {
            userObject.dep.ids[i].approved = 2;
            userObject.dep.ids[i].reason = approverComment;
          }
        }
      }
      return userObject;
    }

    approverComment = approverComment.replace(/`/g, "%1%")
      .replace(/"/g, "%2%")
      .replace(/'/g, "%3%")
      .replace(/\\/g, '%4%');

    connection.query("UPDATE freedays JOIN user ON freedays.userID = user.userID SET" +
      " freedays.userObject = '" + JSON.stringify(getObjects(userObject, 'id', userID)) + "'," +
      " freedays.approved = '" + status + "', user.avfreedays = '" + avfreedays + "'" +
      " WHERE freedays.id = '" + id + "' AND user.email = '" + email + "'", function (err, rows) {
      connection.release();
      if (!err) {
        if (approved !== 0) {
          callback(sendEmailToUser(status, email, type, approverComment, id));
        }
        callback(rows);
      }
    });
    connection.on('error', function (err) {
      err = ({
        "code": 100,
        "status": "Error in connection database"
      });
      callback(err);
    });
  });
}

// insert last year in database
function insertLastYear(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("INSERT INTO yearset (year) VALUES ('" + params.year + "')", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//select last year from database
function selectLastYear(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT * FROM yearset ORDER BY year DESC LIMIT 1 ", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//add new holiday
function insertNewHoliday(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query("INSERT INTO legalholidays (startDate, name) VALUES ('" + params.startDate + "'," +
      " '" + params.name + "')", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//add new holiday for md
function insertNewHolidayMD(req, res) {
  let params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query("INSERT INTO legalholidaysmd (startDate, name, type) VALUES " +
      "('" + params.startDate + "', '" + params.name + "', '" + "public" + "')", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//get all supervisors and administrators
function getAllManagers(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT * FROM user WHERE isActive = 1 AND (position = 'Supervisor' " +
      "OR position = 'Administrator')", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//retrieve holidays for selected countries
function retrieveFilteredHolidays(req, res) {
  let month, viewType = req.query.viewType, viewDate = req.query.viewDate, countries = req.query.countries, current,
    weekstart, weekend, monday, sunday, day, query, table, country;

  if (countries.indexOf('1') !== -1 && countries.indexOf('0') !== -1) {
    if (viewType === 'month') {
      month = (new Date(viewDate)).getMonth() + 1, year = (new Date(viewDate)).getFullYear();
      query = `SELECT 'ro' AS country, id, startDate, name, type FROM legalholidays WHERE
       MONTH(legalholidays.startDate)=` + month + ` AND YEAR(legalholidays.startDate)=` + year +
        ` UNION ALL SELECT 'md' AS country, id, startDate, name, type from legalholidaysmd WHERE
         MONTH(legalholidaysmd.startDate)=` + month + ` AND YEAR(legalholidaysmd.startDate)=` + year;
    } else if (viewType === 'basicWeek') {
      current = new Date(viewDate),
        weekstart = current.getDate() - current.getDay() + 1,
        weekend = weekstart + 6,
        monday = moment(new Date(current.setDate(weekstart))).format('YYYY-MM-DD'),
        sunday = moment(new Date(current.setDate(weekend))).format('YYYY-MM-DD');
      query = `SELECT 'ro' AS country, id, startDate, name, type FROM legalholidays WHERE
       legalholidays.startDate BETWEEN '` + monday + `' AND '` + sunday +
        `' UNION ALL SELECT 'md' AS country, id, startDate, name, type from legalholidaysmd WHERE
        legalholidaysmd.startDate BETWEEN '` + monday + `' AND '` + sunday + `'`;
    } else {
      day = moment(viewDate).format('YYYY-MM-DD');
      query = `SELECT 'ro' AS country, id, startDate, name, type FROM legalholidays WHERE
       legalholidays.startDate='` + day +
        `' UNION ALL SELECT 'md' AS country, id, startDate, name, type from legalholidaysmd WHERE
         legalholidaysmd.startDate='` + day + `'`;
    }
  } else {
    if (countries.indexOf('0') !== -1) {
      table = 'legalholidays';
      country = 'ro';
    } else {
      table = 'legalholidaysmd';
      country = 'md';
    }

    if (viewType === 'month') {
      month = (new Date(viewDate)).getMonth() + 1, year = (new Date(viewDate)).getFullYear();
      query = `SELECT '` + country + `' AS country, id, startDate, name, type FROM ` + table + `
       WHERE MONTH(` + table + `.startDate)=` + month + ` AND YEAR(` + table + `.startDate)=` + year;
    } else if (viewType === 'basicWeek') {
      current = new Date(viewDate),
        weekstart = current.getDate() - current.getDay() + 1,
        weekend = weekstart + 6,
        monday = moment(new Date(current.setDate(weekstart))).format('YYYY-MM-DD'),
        sunday = moment(new Date(current.setDate(weekend))).format('YYYY-MM-DD');
      query = `SELECT '` + country + `' AS country, id, startDate, name, type FROM ` + table + `
       WHERE ` + table + `.startDate BETWEEN '` + monday + `' AND '` + sunday + `'`;
    } else {
      day = moment(viewDate).format('YYYY-MM-DD');
      query = `SELECT '` + country + `' AS country, id, startDate, name, type FROM ` + table + `
       WHERE ` + table + `.startDate='` + day + `'`;
    }
  }
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }

    connection.query(query, function (err, rows) {
      connection.release();
      console.log(err)
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

//update available free days for current user
function updateAvFreeDays(req, res) {
  var params = req.query;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("UPDATE user SET avfreedays = '" + params.avfreedays + "' " +
      "WHERE email = '" + params.email + "';", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

function getStartDate(req, res) {
  pool.getConnection(function (err, connection) {
    if (err) {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    }
    connection.query("SELECT user.age FROM user WHERE admin = 2 OR admin = 6", function (err, rows) {
      connection.release();
      if (!err) {
        res.json(rows);
      }
    });
    connection.on('error', function () {
      res.json({
        "code": 100,
        "status": "Error in connection database"
      });
    });
  });
}

function phoneValidation(phone) {
  let phonePattern = /^([0-9]{10})$/;
  return (phonePattern).test(phone);
}

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(fileUpload());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', "GET, PUT, POST, DELETE");
  res.header('Access-Control-Allow-Headers', "'Origin', 'X-Requested-With', 'Content-Type', 'Accept'");
  logger.info(`req.url: ${JSON.stringify(req.url)} \n req.headers: ${JSON.stringify(req.headers)} \n req.body: ${JSON.stringify(req.body)} \n`)

  next();
});

// Router middleware, mentioned it before defining routes.

router.use(function (req, res, next) {
  console.log("/" + req.method);
  next();
});

router.get("/retrieveFilteredHolidays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    retrieveFilteredHolidays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/legalHolidaysToDb", function (req, res) {
  legalHolidaysToDb(req, res);
});

router.get("/legalHolidaysMDToDb", function (req, res) {
  legalHolidaysMDToDb(req, res);
});

router.get("/sendEmailWhenChangeSupervisors", function (req, res) {
  sendEmailWhenChangeSupervisors(req, res);
});

router.post("/login", function (req, res) {
  login(req, res);
});

router.post("/logout", function (req, res) {
  logout(req, res);
});

router.get("/checkUser", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    checkUser(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getLegalFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getLegalFreeDays(req, res);
  }, function () {
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getLegalFreeDaysMD", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getLegalFreeDaysMD(req, res);
  }, function () {
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getFreeDaysApprover", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getFreeDaysApprover(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getFreeDaysApprovedOrPending", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getFreeDaysApprovedOrPending(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.post('/upload', function (req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.profileImage.data;
  if (sampleFile.length < 1048576) {
    upload(req, res);
  } else {
    console.log("Picture is too big");
    res.json({
      "code": 200,
      "uploadMessage": "Picture is too big."
    });
    return res;
  }
  // Use the mv() method to place the file somewhere on your server
});

router.get("/getManagerFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getManagerFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getAllManagerFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAllManagerFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  })
});

router.get("/getAllUserFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAllUserFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  })
});

router.get("/getHRFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getHRFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getFilteredFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getFilteredFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});


router.get("/getCalendarUsersName", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getCalendarUsersName(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  })
});


router.get("/getLocalHrFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getLocalHrFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getAdminFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAdminFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.post("/updateUser", function (req, res) {
  let token = req.body.token;
  isValidToken(token).then(function () {
    updateUser(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.post("/updateUserObject", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    updateUserObject(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getAllManagers", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAllManagers(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getUserIdLocalHr", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    userIdLocalHr(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getSupervisorsName", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getSupervisorsName(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getUserIdHrAdminAndAdministrator", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    userIdHrAdminAndAdministrator(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getAllUsers", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAllUsers(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});


router.get("/getAllHolidays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getAllHolidays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/updateHolidays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    updateHolidays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/updateHolidaysMD", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    updateHolidaysMD(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/ManagerEditUserForm", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    ManagerEditUserForm(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});


router.post("/addUser", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    addUser(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.post("/deleteLegalHoliday", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    deleteLegalHoliday(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/updateAvFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    updateAvFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/updateAllAvFreeDays", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    updateAllAvFreeDays(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/selectLastYear", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    selectLastYear(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/insertLastYear", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    insertLastYear(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});


router.get("/insertNewHoliday", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    insertNewHoliday(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/insertNewHolidayMD", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    insertNewHolidayMD(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

router.get("/getStartDate", function (req, res) {
  let token = req.query.token;
  isValidToken(token).then(function () {
    getStartDate(req, res);
  }, function (error) {
    console.log(error);
    res.json({
      "code": 110,
      "status": "Your session has expired and you are loged out. - redirect la index in FE"
    })
  });
});

app.use("/api", router);

// Listen to this Port

app.listen(3000, function () {
  console.log("Live at Port 3000");
});
