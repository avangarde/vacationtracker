window.appNameSpace = window.appNameSpace || {};
window.sessionInvalid = false;
var token = localStorage.getItem('token'),
  theUser = JSON.parse(localStorage.getItem('user')),
  supervisorsNameForEmail = '',
  reason,
  index = 0,
  numb = 0,
  actualSups = [],
  arrayResult = [],
  managersForUser = [],
  obj = {},
  managementTable, localHrTable, hrAdminTable, adminTable, getAllUsersTable,
  managementTableRequestSequence = 0,
  localHrTableRequestSequence = 0,
  hrAdminTableRequestSequence = 0,
  adminTableRequestSequence = 0,
  allUsersTableRequestSequence = 0,
  linkLocationRef = appConfig.linkLocationRef,
  socket = io(appConfig.io),
  totalFreeDays,
  idsSuperiorsOLD = [];
getSupervisorsName();

$(document).ready(function () {
  $.fn.dataTable.moment('DD/MM/Y');
  var url = new URL(window.location.href), page = url.searchParams.get("page");
  if (page != 1) {
    setTimeout(function () {
        $('[data-dt-idx="' + page + '"]').trigger('click');
      },
      29)
  }
});

function adjustByHeight() {
  let $body = $('body'),
    $iframe = $body.data('iframe.fv');
  if ($iframe) {
    $iframe.height($body.height()); // Adjust the height of iframe when hiding the picker
  }
}

function adjustByScrollHeight() {
  let $body = $('body'),
    $iframe = $body.data('iframe.fv');
  if ($iframe) {
    $iframe.height($body.get(0).scrollHeight); // Adjust the height of iframe when showing the picker
  }
}

if (theUser.admin >= 0) {
  $(function () {
    socket.on('/resRefreshManagedUserTable', function (data) {
      if (data.token === token) {
        $('#users-list-table').DataTable().ajax.reload(null, false);
        getAllUsers();
      }
    });
    socket.on('/resupdatedate', function (dataSocket) {
      if (dataSocket.userId !== theUser.userID && theUser.admin !== 0) {
        $("#manager-table").DataTable().ajax.reload(null, false);
      }
    });
    socket.on('/resdeleteHoliday', function (data) {
      if (data !== token) {
        $("#manager-table").DataTable().ajax.reload(null, false);
      }
    });
    socket.on('/resApproveFreeDays', function (data) {
      if (data)
        out(data.code);
      populate(theUser.admin);
    });
    socket.on('/resNewYear', function (data) {
      if (data === token) {
        $('#users-list-table').DataTable().ajax.reload(null, false);
        $("#manager-table").DataTable().ajax.reload(null, false);
        getAllUsers();
        populate(theUser.admin);
      }
    });

    socket.on('/resSetApproved', function (data) {
      if (theUser.admin === 1) {
        managementTable.state.save();
        managementTable.ajax.reload(null, false);
      } else if (theUser.admin === 6) {
        adminTable.state.save();
        adminTable.ajax.reload(null, false);
      } else if (theUser.admin === 3) {
        hrAdminTable.state.save();
        hrAdminTable.ajax.reload();
      } else if (theUser.admin === 4) {
        localHrTable.state.save();
        localHrTable.ajax.reload(null, false);
      }
    });

    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'getAllManagerFreeDays?token=' + token + '&userID=' + theUser.userID,
      async: false,
    }).done(function (data) {
      totalFreeDays = data[0].count;
    });

    $("input[name='activeUsers']").click(() => {
      localStorage.setItem('activeUsers', $("input[name='activeUsers']").is(":checked"));
      $('#users-list-table').DataTable().ajax.reload(null, false);
      getAllUsers();
    });

    if (theUser.admin !== 0) {
      $("input[name='activeUsers']")[0].checked = localStorage.getItem('activeUsers') === 'true';
    }

    $("a[name='addUser']").click(function () {
      $("#myModalUser").load("addUserForm.html", function () {
        prepareUserForm();
        var options = $('select#ad_select option');
        if (theUser.admin == 3) {
          $.map(options, function (option) {
            if (option.value == 'Administrator') {
              $(option).hide();
            }
          });
        }

        $('#birth').datepicker({
          dateFormat: 'mm/dd/yy'
        });
        $('#dateval').datepicker({
          dateFormat: 'mm/dd/yy'
        });

      })
    });

    $("a[name='updateUser']").click(function () {
      $("#myModalUpdateUser").load("updateUserForm.html", function (e) {
        setTimeout(function () {
          $("input[name = 'passwordUser']").val('');
        }, 50);
        fillUpdateUserForm();
        updateUser();
      })
    });

    function fillUpdateUserForm() {
      var currentName = JSON.parse(localStorage.getItem('user')).name,
        currentPhone = JSON.parse(localStorage.getItem('user')).phone;
      document.getElementById("username").value = currentName;
      document.getElementById("phoneUser").value = currentPhone;
    }

    populate(theUser.admin);

    getAllUsers();


    $("#approve-modal-btn-no").click(function () {
      $("#approve-freedays-modal").modal('hide');
    });
    $("#approve-modal-btn-yes").click(function () {
      $("#approve-freedays-modal").modal('hide');
    })
  });

// Shows display ApproveModal
  function displayApproveModal(obj, elem, id, type, email, avFD, action, isOk, status) {
    var approveModal = $("#approve-freedays-modal"),
      approverComment = ' ';

    if (action === 2) {
      $("#approve-freedays-modal .modal-body").css('display', 'block');
      $("#approve-freedays-modal #approve-modal-btn-yes").attr('disabled', 'disabled');
    } else {
      $("#approve-freedays-modal .modal-body").css('display', 'none');
      $("#approve-freedays-modal #approve-modal-btn-yes").attr('disabled', false);
    }

    $("#approverComment").on('change keyup paste', function () {
      if (!$("#approverComment").val()) {
        $("#approve-freedays-modal #approve-modal-btn-yes").attr('disabled', 'disabled');
      } else {
        $("#approve-freedays-modal #approve-modal-btn-yes").attr('disabled', false);
        approverComment = $("#approverComment").val();
      }
    });

    approveModal.modal('show');
    $('#approverComment').val('');
    approveModal.attr("approveId", id);
    approveModal.attr("approve-action", action);
    $("#manager-table tr").removeClass("activeModal");
    $(elem).closest("tr").addClass("activeModal");
    $("#approve-modal-btn-yes").off();
    $("#approve-modal-btn-yes").on('click', function (event) {
      $("#approve-freedays-modal").modal('hide');
      $('.modal-backdrop').remove();
      if (status === 2) {
        socket.emit('/setApproved', {
          token: token,
          id: id,
          status: 2,
          approverComment: approverComment,
          type: type,
          email: email,
          approved: 2,
          avfreedays: avFD,
          admin: theUser.admin,
          userID: theUser.userID,
          userObject: obj
        })
      } else if (status === 1) {
        socket.emit('/setApproved', {
          token: token,
          id: id,
          status: 1,
          approverComment: approverComment,
          type: type,
          email: email,
          approved: 1,
          avfreedays: avFD,
          admin: theUser.admin,
          userID: theUser.userID,
          userObject: obj
        })
      } else if (status === 0) {
        socket.emit('/setApproved', {
          token: token,
          id: id,
          status: 0,
          approverComment: approverComment,
          type: type,
          email: email,
          approved: 1,
          avfreedays: avFD,
          admin: theUser.admin,
          userID: theUser.userID,
          userObject: obj
        })
      }
    });
  }

  function colorTableRow(approved) {
    if (approved === 1) {
      return "info";
    } else if (approved === 2) {
      return "danger";
    } else {
      return "white";
    }
  }

  function managerEditUser(elem, userId, supsIds) {
    var arraySupsIds = JSON.parse("[" + supsIds + "]"),
      tr = $(elem).closest("tr"), userInfo = tr.find("td");
    // $("#users-list tr").removeClass("active-user");
    tr.addClass("active-user");

    $("#myModalUser").load("editUserForm.html", function () {
      prepareUserForm();

      var editUserForm = $("#edit-user-form");
      editUserForm.attr("user-id", userId);
      // Add user details into edit form.

      editUserForm.find("input[name='userId']").val(userId);
      var name = userInfo.eq(1).text();
      editUserForm.find("input[name='name']").val(name);

      var country = userInfo.eq(2).text();
      if (country == "ro") {
        editUserForm.find("#mold").attr("checked", false);
        editUserForm.find("#rom").attr("checked", true);
      } else if (country == "md") {
        editUserForm.find("#rom").attr("checked", false);
        editUserForm.find("#mold").attr("checked", true);
      }

      var position = userInfo.eq(3).text(),
        positionInfo = userInfo.eq(4).text(),
        email = userInfo.eq(5).text(),
        phone = userInfo.eq(7).text(),
        manager = userInfo.eq(8).text(),
        freedays = userInfo.eq(11).text(),
        bonus = userInfo.eq(12).text();

      editUserForm.find("select[name='position']").val(position);
      editUserForm.find("input[name='posInfo']").val(positionInfo);
      editUserForm.find("input[name='email']").val(email);
      editUserForm.find("input[name='phone']").val(phone);
      editUserForm.find("option[name='manager-name']").text(manager);
      editUserForm.find("input[name='freedays']").val(freedays);
      editUserForm.find("input[name='bonus']").val(bonus);

      if (userInfo.eq(9).text() == 'true') {
        $('[name=userActive]:first').attr('checked', true);
      } else {
        $('[name=userActive]:not(:first)').attr('checked', true);
      }


      if (position == "Supervisor") {
        $.get(appConfig.url + appConfig.api + 'getAllManagers?token=' + token, function (managers) {
          out(managers.code);
          $('select[name="new_manager"]').append($("<option></option>")
            .attr("value", 0)
            .text('Select new supervisor'));
          for (var i in managers) {
            if (managers[i].userID != userId && managers[i].position !== 'Administrator') {
              $('select[name="new_manager"]').append($("<option></option>")
                .attr("value", managers[i].userID)
                .text(managers[i].name));
            }
          }
        });
      } else {
        editUserForm.find("select[name='new_manager']").remove();
        editUserForm.find("label[for='new_manager']").remove();
      }
      var selectNewManager = editUserForm.find(".new_manager");

      if ($('input[name=userActive]:checked').attr('value') == '1') {
        selectNewManager.hide();
      }

      $('[name=userActive]').click(function () {
        if ($(this).attr('value') == '1') {
          selectNewManager.hide();
          editUserForm.find("input[name=isActive]").val('true');
        } else {
          selectNewManager.css('display', 'block');
          editUserForm.find("input[name=isActive]").val('false');
        }
      });


      var isAdmin = localStorage.getItem('admin');
      if ((isAdmin == 2) || (isAdmin == 6) || (isAdmin == 3)) {
        var selectChangeManager = editUserForm.find("select[name='change_manager']");
        selectChangeManager.empty();
        managers = managersForUser;
        actualSups = [];
        for (var i in managers) {
          checked = false
          if ((managers[i].userID != userId)) {
            for (var p = 0; p < arraySupsIds.length; p++) {
              if (arraySupsIds[p] == managers[i].userID) {
                checked = true;
              }
            }
            if (checked) {
              actualSups.push(managers[i].userID);
              selectChangeManager.append($("<option></option>")
                .attr("value", managers[i].userID)
                .attr("data-position", managers[i].position)
                .attr("selected", 'selected')
                .text(managers[i].name));
            } else {
              selectChangeManager.append($("<option></option>")
                .attr("value", managers[i].userID)
                .attr("data-position", managers[i].position)
                .text(managers[i].name));
            }
          }
        }

        $("#edit-user-form").formValidation({
          framework: 'bootstrap',
          // Exclude only disabled fields
          // The invisible fields set by Bootstrap Multiselect must be validated
          excluded: ':disabled',
          icon: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
          },
          change_manager: {
            validators: {
              callback: {
                message: 'Please choose 1 or 2 managers',
                callback: function (value, validator, $field) {
                  // Get the selected options
                  var options = validator.getFieldElements('change_manager').val();
                  return (options != null && options.length >= 0);
                }
              }
            }
          }
        })
          .find('[name="change_manager"]')
          .multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            // Re-validate the multiselect field when it is changed
            onChange: function (element, checked) {
              $('#edit-user-form').formValidation('revalidateField', 'change_manager');

              adjustByScrollHeight();
            },
            onDropdownShown: function (e) {
              adjustByScrollHeight();
            },
            onDropdownHidden: function (e) {
              adjustByHeight();
            }
          })
          .end();

        function adjustByHeight() {
          let $body = $('body'),
            $iframe = $body.data('iframe.fv');
          if ($iframe) {
            // Adjust the height of iframe when hiding the picker
            $iframe.height($body.height());
          }
        }

        function adjustByScrollHeight() {
          let $body = $('body'),
            $iframe = $body.data('iframe.fv');
          if ($iframe) {
            // Adjust the height of iframe when showing the picker
            $iframe.height($body.get(0).scrollHeight);
          }
        }
      } else if (isAdmin < 6) {
        editUserForm.find("input[name='userpass']").remove();
        editUserForm.find("label[for='userpass']").remove();
      } else {
        editUserForm.find("select[name='change_manager']").remove();
        editUserForm.find("label[for='change_manager']").remove();
      }
    });
  }

  //Add user form
  function prepareUserForm() {
    var date = new Date();
    date.setDate(date.getDate());
    $('#stwork').datepicker({
      endDate: date,
      format: 'yyyy/mm/dd'
    }).on('changeDate', function (e) {
      $('#add-user-form').formValidation('revalidateField', 'stwork');
    });

    // Add user form.
    $("input:checkbox").on('click', function () {
      var $box = $(this);
      if ($box.is(":checked")) {
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
    $("#old").change(function () {
      if ($(this).is(":checked")) {
        $("#avfree").show();
      } else {
        $("#avfree").hide();
        $("input[name='avfreedays']").val("");
      }
    });
    $("#new").change(function () {
      if ($(this).is(":checked")) {
        $("#avfree").hide();
        $("input[name='avfreedays']").val("");
      }
    });

    var selectNewManager = $("#add-user-form").find("select[name='new_manageradd']");
    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'getAllManagers?token=' + token,
      async: false,
    }).done(function (managers) {
      out(managers.code);
      managersForUser = managers;
      for (var i in managers) {
        selectNewManager.append($("<option></option>")
          .attr("value", managers[i].userID)
          .attr("data-position", managers[i].position)
          .text(managers[i].name));
      }

      //Add user form
      $form = $('#add-user-form'); // cache
      var validAge = (moment().subtract(18, 'years')).format('YYYY-MM-DD').toString(),
        minStWork, currentDay = (moment().format('YYYY-MM-DD')).toString();

      $.get(appConfig.url + appConfig.api + 'getStartDate?token=' + token, function (data) {
        out(data.code);
        minStWork = moment(data[0].age).format('YYYY-MM-DD').toString();
      }).done(function () {
        $("#add-user-form").find('[name="pos"]').selectpicker().change(function (e) {
        }).end().bootstrapValidator({
          framework: 'bootstrap',
          icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            username: {
              validators: {
                notEmpty: {
                  message: 'This is required'
                }
              }
            },
            email: {
              validators: {
                notEmpty: {
                  message: 'This is required'
                },
                regexp: {
                  regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                  message: 'The value is not a valid email address'
                }
              }
            },
            phoneUser: {
              validators: {
                notEmpty: {
                  message: 'The type is required'
                },
                regexp: {
                  regexp: '^([0-9]{10})$',
                  message: 'Invalid Phone number, must contain 10 digits'
                }
              }
            },
            pos: {
              validators: {
                notEmpty: {
                  message: 'Please select a position.'
                },
                regexp: {
                  regexp: /^(.?$|[^S].+|S[^e].*)/,
                  message: 'Please select position. '
                },
              }
            },
            posInfo: {
              validators: {
                notEmpty: {
                  message: 'The position info is required'
                }
              }
            },
            ageUser: {
              validators: {
                notEmpty: {
                  message: 'The birthday is required'
                }
              }
            },
            stwork: {
              validators: {
                notEmpty: {
                  message: 'The started working is required'
                }
              }

            },
          },
          new_manageradd: {
            excluded: ':disabled',
            validators: {
              callback: {
                message: 'Please choose 1 or 2 managers',
                callback: function (value, validator, $field) {
                  // Get the selected options
                  var options = validator.getFieldElements('new_manageradd').val();
                  return (options != null && options.length >= 0);
                }
              }
            }
          }
        }).on('status.field.bv', function (e, data) {
          let $this = $(this);
          let formIsValid = true;
          if (!$this.find('input[name="username"]').parent().hasClass("has-success")) {
            formIsValid = false;
          }
          if (!$this.find('input[name="email"]').parent().hasClass("has-success")) {
            formIsValid = false;
          }
          if (!$this.find('input[name="phoneUser"]').parent().hasClass("has-success")) {
            formIsValid = false;
          }

          if (!$this.find('input[name="posInfo"]').parent().hasClass("has-success")) {
            formIsValid = false;
          }

          $('#register', $this).attr('disabled', !formIsValid);
        }).on('change', function (e, data) {
          $("#add-user-form").formValidation('revalidateField', 'ageUser');
          $("#add-user-form").formValidation('revalidateField', 'stwork');
          $("#add-user-form").formValidation('revalidateField', 'email');
          $("#add-user-form").formValidation('revalidateField', 'phoneUser');
          $("#add-user-form").formValidation('revalidateField', 'pos');
          $("#add-user-form").formValidation('revalidateField', 'posInfo');
          $("#add-user-form").formValidation('revalidateField', 'username');
          if ($("#add-user-form").data('formValidation').isValid()) {
            if ($("[name=ageUser]")) {
              if ($("[name=stwork]")) {
                $('#register').attr('disabled', false);
              }
            }
          }
          if ($("[name = 'pos']").val() == 'Administrator') {
            $("#admins").css('display', 'none');
          } else {
            $("#admins").css('display', 'block');
          }
        }).on('submit', function (e, data) {
          $("#avfree").hide();
          var birthday = "'" + moment($("#add-user-form").find("input[name = 'ageUser']").val()).format("YYYY-MM-DD") + "'",
            startWork = "'" + moment($("#add-user-form").find("[name = 'stwork']").val()).format("YYYY-MM-DD") + "'",
            isOkAge = true,
            isOkSt = true;
          if (moment(birthday).isSameOrAfter("'" + validAge + "'")) {
            isOkAge = false;
          } else {
            isOkAge = true;
          }

          if (moment(startWork).isSameOrBefore("'" + minStWork + "'") || moment(startWork).isAfter("'" + currentDay + "'")) {
            isOkSt = false;
          } else {
            isOkSt = true;
          }

          if (e.isDefaultPrevented()) {
            //prevent default
          } else if (isOkAge == true && isOkSt == true && $("#add-user-form").data('formValidation').isValid()) {
            $('#register').attr('disabled', 'disabled');
            var formWrapper = $("#add-user-form"),
              userName = formWrapper.find("input[name = 'username']").val(),
              age = moment(formWrapper.find("input[name = 'ageUser']").val()).format("YYYY-MM-DD"),
              position = formWrapper.find("[name = 'pos']").val(),
              positionInfo = formWrapper.find("[name = 'posInfo']").val(),
              stwork = moment(formWrapper.find("[name = 'stwork']").val()).format("YYYY-MM-DD"),
              email = formWrapper.find("input[name = 'email']").val(),
              phone = formWrapper.find("input[name = 'phoneUser']").val(),
              avfreedays = formWrapper.find("input[name = 'avfreedays']").val(),
              country = formWrapper.find("input[name='country']:checked").val(),
              manager = formWrapper.find("[name = new_manageradd]").val(),
              newposition = formWrapper.find("[name = pos]").val(),
              hashObj = new jsSHA("SHA-512", "TEXT", {
                numRounds: 1
              });
            hashObj.update('avangarde');
            var password = hashObj.getHash("HEX");
            if (!avfreedays.length) {
              var a = moment().endOf('year'), b = moment().today, x;
              x = a.diff(stwork, 'days');
              avfreedays = Math.round((21 * x) / 365);
            }

            if (theUser.admin != 0) {
              $.post(appConfig.url + appConfig.api + 'addUser?token=' + token, {
                email: email,
                name: userName,
                age: age,
                password: password,
                position: position,
                positionInfo: positionInfo,
                country: country,
                phone: phone,
                stwork: stwork,
                avfreedays: avfreedays,
                userObject: " "
              }).done(function (data) {
                out(data.code);

                socket.emit('/refreshManagedUserTable', {
                  token: token
                });
                if (data.errno == 1062) {
                  alert('An user with this email already exists.');
                  $("[name=email]").val('');
                  $('#add-user-form').formValidation('revalidateField', 'email');
                } else {
                  var userid = JSON.parse(localStorage.getItem('user')).userID;
                  if (position != 'Administrator') {
                    options = formWrapper.find("[name = new_manageradd]").find('option:selected');
                    country = formWrapper.find("input[name='country']:checked").val();
                    var newposition = formWrapper.find("[name = pos]").val();
                    setSuperiors(options, newposition, token, country, data.insertId);
                  } else {
                    callDependencies([], [], data.insertId);
                  }
                  var j = userid;
                  $('.modal-body> div:first-child').css('display', 'block');
                  $('#myModalUser').find('form')[0].reset();
                }
              });
              // });
            }
            e.preventDefault();
          } else {
            e.preventDefault();
            if (isOkSt) {
              alert('Age is not valid.');
              $('#register').attr('disabled', true);
              $("#birth").next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
              $("#birth").parent().removeClass('has-success').addClass('has-error');
              $(".datepicker-days > table td").removeClass('active');
              $('#birth').datepicker('setDate', null);
            } else if (isOkAge) {
              alert("Started working is not valid.");
              $('#register').attr('disabled', true);
              $("#dateval").next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
              $("#dateval").parent().removeClass('has-success').addClass('has-error');
              $("#dateval").datepicker('setDate', null);
            } else {
              alert("Age and started working are not valid.");
              $('#register').attr('disabled', true);
              $("#birth").next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
              $("#dateval").next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
              $("#dateval").parent().removeClass('has-success').addClass('has-error');
              $("#birth").parent().removeClass('has-success').addClass('has-error');
              $("#birth").datepicker('setDate', null);
              $("#dateval").datepicker('setDate', null);
            }
          }
        }).find('[name="new_manageradd"]')
          .multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            // Re-validate the multiselect field when it is changed
            onChange: function (element, checked) {
              $('#add-user-form').formValidation('revalidateField', 'new_manageradd');
              adjustByScrollHeight();
            },
            onDropdownShown: function (e) {
              adjustByScrollHeight();
            },
            onDropdownHidden: function (e) {
              adjustByHeight();
            }
          })
      })
    });

    // Edit user form.
    $("#edit-user-form").formValidation({
      framework: 'bootstrap',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'The name is required.'
            }
          }
        },
        country: {
          validators: {
            notEmpty: {
              message: 'The country is required.'
            }
          }
        },
        position: {
          validators: {
            notEmpty: {
              message: 'The position is required.'
            }
          }
        },
        positionInfo: {
          validators: {
            notEmpty: {
              message: 'The position info required.'
            }
          }
        },
        email: {
          validators: {
            regexp: {
              regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
              message: 'The value is not a valid email address'
            }
          }
        },
        stwork: {
          validators: {
            notEmpty: {
              message: 'The start date is required.'
            },
            date: {
              format: 'YYYY-MM-DD',
              message: 'The value is not a valid date'
            }
          }
        },
        phone: {
          validators: {
            notEmpty: {
              message: 'The phone number is required.'
            }
          }
        }
      }
    }).on('submit', function (e, data) {
      if (e.isDefaultPrevented()) {
        // handle the invalid form...
      } else {
        var formWrapper = $("form#edit-user-form").serialize(),
          formWrapperpass = $("form#edit-user-form"),
          passwordUser = formWrapperpass.find("input[name = 'userpass']").val(),
          positionInfo = formWrapperpass.find("input[name = 'posInfo']").val(),
          userId = formWrapperpass.find("input[name = 'userId']").val(),
          avfreedays = formWrapperpass.find("input[name = 'freedays']").val(),
          hashObj = new jsSHA("SHA-512", "TEXT", {
            numRounds: 1
          });

        if (passwordUser !== '' && (theUser.admin === 6 || theUser.admin === 3)) {
          hashObj.update(passwordUser);
          passwordUser = hashObj.getHash("HEX");
        } else {
          passwordUser = "";
        }

        $.get(appConfig.url + appConfig.api + 'ManagerEditUserForm?' + formWrapper + '&password=' + passwordUser +
          '&positionInfo=' + positionInfo + '&avfreedays=' + avfreedays + '&token=' + token, function (data) {
          out(data.code);
          $("#users-list-table").DataTable().ajax.reload(null, false);
          if ((theUser.admin === 2) || (theUser.admin === 6) || (theUser.admin === 3)) {
            $("#users-list-table").DataTable().clear();
            var formWrapper = $("#edit-user-form"),
              options = formWrapper.find("[name = change_manager]").find('option:selected'),
              country = formWrapper.find("input[name='country']:checked").val(),
              newposition = formWrapper.find("[name = position]").val(),
              emailUser = formWrapper.find("[name = email]").val(),
              updatedSups = false;
            if (options.length !== actualSups.length) {
              updatedSups = true;
            }
            supervisorsNameForEmail = '';

            for (let i = 0; i < options.length; i++) {
              if (i == options.length - 1) {
                supervisorsNameForEmail += options[i].text;
              } else {
                supervisorsNameForEmail += options[i].text + ', ';
              }
              if ($.inArray(parseInt(options[i].value), actualSups) === -1) {
                updatedSups = true;
              }
            }

            if (updatedSups) {
              sendEmailChangeSupervisor(emailUser, supervisorsNameForEmail);
            }
            setSuperiors(options, newposition, token, country, userId);
            socket.emit('/refreshManagedUserTable', {
              token: token,
              userId: userId
            });
          } else {
            socket.emit('/refreshManagedUserTable', {
              token: token,
              userId: userId
            });
          }
        });
        e.preventDefault();
        $('.modal-body> div:first-child').css('display', 'block');
        $("#edit-user-form").data('formValidation').resetForm();
        $("#myModalUser").modal("hide");
        $('.modal-backdrop').remove();
      }
    })
  }


  function setSuperiors(options, newposition, token, country, insertedId) {
    var managerz = [];
    $.map(options, function (option) {
      var val = parseInt(option.value),
        pos = option.getAttribute('data-position'),
        name = option.text;
      if (pos == 'Supervisor') {
        managerz.push({
          '1': val
        });
      }
    });

    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'getUserIdLocalHr?token=' + token + "&country=" + country,
      async: false
    }).done(function (idLocal) {
      out(idLocal.code);
      for (var i = 0; i < idLocal.length; i++)
        if (newposition != "Local HR" && newposition != 'HR - admin' && idLocal[i]['userID'] != insertedId) {
          var id = idLocal[i]['userID'];
          managerz.push({
            '4': id
          });
        }
    });

    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'getUserIdHrAdminAndAdministrator?token=' + token,
      async: false
    }).done(function (idAdmin) {
      out(idAdmin.code);
      for (var i = 0; i < idAdmin.length; i++) {
        var id = idAdmin[i]['userID'];
        if (idAdmin[i].admin == 6) {
          managerz.push({
            '6': id
          });
        } else {
          if (newposition != "HR - admin" && idAdmin[i]['userID'] != insertedId) {
            managerz.push({
              '3': id
            });
          }
        }
      }
    });

    callDependencies(managerz, managerz, insertedId);
  }

  function updateUser() {
    $("#update-user-form").formValidation({
      framework: 'bootstrap',
      fields: {
        passwordUser: {
          validators: {
            identical: {
              field: 'confirmPassword',
              message: 'The password and its confirm are not the same'
            }
          }
        },
        confirmPassword: {
          validators: {
            identical: {
              field: 'passwordUser',
              message: 'The password and its confirm are not the same'
            }
          }
        },
        phoneUser: {
          validators: {
            notEmpty: {
              message: 'The phone number is required.'
            },
            regexp: {
              regexp: '^([0-9]{10})$',
              message: 'Invalid Phone number, must contain 10 digits'
            }
          }
        }
      }
    }).on('submit', function (e, data) {
      if (e.isDefaultPrevented()) {
        // handle the invalid form...
      } else {
        var formWrapper = $("#update-user-form"),
          userName = formWrapper.find("input[name = 'username']").val(),
          passwordUser = formWrapper.find("input[name = 'passwordUser']").val(),
          phone = formWrapper.find("input[name = 'phoneUser']").val(),
          userid = JSON.parse(localStorage.getItem('user')).userID,
          hashObj = new jsSHA("SHA-512", "TEXT", {
            numRounds: 1
          });
        hashObj.update(passwordUser);
        if (passwordUser != '') {
          passwordUser = hashObj.getHash("HEX");
        }
        theUser.name = userName;
        theUser.phone = phone;
        localStorage.setItem('user', JSON.stringify(theUser));
        $.post(appConfig.url + appConfig.api + 'updateUser', {
          name: userName,
          phone: phone,
          password: passwordUser,
          userId: userid,
          token: token
        }).done(function (data) {
          out(data.code);
          socket.emit('/refreshManagedUserTable', {
            token: token,
            userId: userid
          });
        });
        $('.update > div:first-child').css('display', 'block');
        e.preventDefault();
        $("#update-user-form").data('formValidation').resetForm();
        //change
      }
      setTimeout(function () {
        $("#myModalUpdateUser").modal("hide");
      }, 1000);
    });
  }


  function out(data) {
    if (data == 110) {
      if (!appConfig.sessionInvalid) {
        window.location.href = '' + linkLocationRef + '';
        appConfig.sessionInvalid = true;
        alert('Session expired');
        $.post(appConfig.url + appConfig.api + 'logout', {
          email: theUser.email
        });
      }
    }
  }

  function managementHolidayApprove(obj, approved) {
    var supervisor = true,
      one_super,
      admin = false,
      contentS = "",
      content = "",
      nr = 0,
      showActiveButtons = false,
      denied = false,
      number = obj.ids.length,
      searchNames = [];

    for (let j = 0; j < obj.ids.length; j++) {
      if (obj.ids[j]['id']) {
        searchNames.push(obj.ids[j]['id']);
      }
    }
    if (obj.dep.ids) {
      for (let j = 0; j < obj.dep.ids.length; j++) {
        if (obj.dep.ids[j]['id']) {
          searchNames.push(obj.dep.ids[j]['id']);
        }
      }
    }
    // result = getSupervisorsNameOLD(searchNames, 2);
    getUserSuperiors(obj);
    result = arrayResult;
    if (obj.rel === "AND") {
      reason = '';
      for (var j in obj.ids) {
        var i = 0;
        while (i < result.length) {
          if (obj.ids[j].id === result[i].id) {
            var name = result[i].name;
          }
          i++;
        }
        if ((obj.ids[j].approved === 0) || !obj.ids[j].approved) {
          supervisor = false;
          content += ' ' + name + ': ' + setStatus(0) + '\n';
          if (obj.ids[j].id === theUser.userID) {
            showActiveButtons = true;
          }
        }
        if (obj.ids[j].approved === 2) {
          content = ' ' + name + ': ' + setStatus(2) + '\n';
          denied = true;
          reason = obj.ids[j].reason;
          break;
        }
        if (obj.ids[j].approved === 1) {
          nr++;
          content += ' ' + name + ': ' + setStatus(1) + '\n';
          if (obj.ids[j].id === theUser.userID) {
            showActiveButtons = false;
          }
          if (nr === number) {
            supervisor = true;
            content = "";
            for (var index = 0; index < nr; index++) {
              content += ' ' + name + ': ' + setStatus(1) + '\n';
            }
          }
        }
      }
    }

    if (obj.rel === "OR") {
      reason = '';
      for (let j in obj.ids) {
        let i = 0;
        while (i < result.length) {
          if (obj.ids[j].id === result[i].id) {
            var name = result[i].name;
          }
          i++;
        }
        if ((obj.ids[j].approved === 0) || !one_super) {
          reason = '';
          supervisor = false;
          content += ' ' + name + ': ' + setStatus(0) + '\n';
          if (obj.ids[j].id === theUser.userID) {
            showActiveButtons = true;
          }
        }
        if (obj.ids[j].approved === 2) {
          reason = '';
          content = ' ' + name + ': ' + setStatus(2) + '\n';
          denied = true;
          reason = obj.ids[j].reason;
          break;
        }
        if (obj.ids[j].approved === 1) {
          reason = '';
          nr++;
          showActiveButtons = false;
          one_super = true;
          supervisor = false;
          content = ' ' + name + ': ' + setStatus(1) + '\n';
        }
        if (nr > 0) {
          showActiveButtons = false;
        }
      }
    }

    if (obj.dep.rel === "OR" && obj.rel === "AND" && denied === false) {
      reason = '';
      contentS = content;
      for (var j in obj.dep.ids) {
        var i = 0;
        while (i < result.length) {
          if (obj.dep.ids[j].id === result[i].id) {
            var name = result[i].name;
          }
          i++;
        }
        if (!obj.dep.ids[j].approved) {
          content += ' ' + name + ': ' + setStatus(0) + '\n';
        }
        if (!obj.dep.ids[j].approved && (nr === number) && (obj.dep.ids[j].id === theUser.userID)) {
          showActiveButtons = true;
        }
        if (obj.dep.ids[j].approved === 1) {
          admin = true;
          content = contentS + name + ': ' + setStatus(1) + '\n';
          if (contentS.indexOf('Pending') !== -1) {
            content = name + ': ' + setStatus(1) + '\n';
          }
        } else if (obj.dep.ids[j].approved === 2) {
          denied = true;
          content = ' ' + name + ': ' + setStatus(2) + '\n';
          reason = obj.dep.ids[j].reason;
          break;
        }
      }
    }

    if (obj.dep.rel === "OR" && obj.rel === "OR" && denied === false) {
      reason = '';
      contentS = content;
      for (var j in obj.dep.ids) {
        var i = 0;
        while (i < result.length) {
          if (obj.dep.ids[j].id === result[i].id) {
            var name = result[i].name;
          }
          i++;
        }
        if (!obj.dep.ids[j].approved && (admin !== true)) {
          content += ' ' + name + ': ' + setStatus(0) + '\n';
        }
        if ((one_super === true) && (obj.dep.ids[j].id === theUser.userID)) {
          showActiveButtons = true;
        }
        if (obj.dep.ids[j].approved === 1) {
          admin = true;
          content = contentS + name + ': ' + setStatus(1) + '\n';
          if (contentS.indexOf('Pending') !== -1) {
            searchNames = contentS.split('\n');
            for (var i in searchNames) {
              if (searchNames[i].indexOf('Pending') !== -1) {
                searchNames.splice(i);
              }
              if (searchNames[i - 1]) {
                content = searchNames[i - 1] + '\n' + name + ': ' + setStatus(1) + '\n';
              } else {
                content = name + ': ' + setStatus(1) + '\n';
              }
            }
          }
        } else if (obj.dep.ids[j].approved === 2) {
          denied = true;
          content = ' ' + name + ': ' + setStatus(2) + '\n';
          reason = obj.dep.ids[j].reason;
          showActiveButtons = false;
          break;
        }
      }
    }

    if (approved === 1) {
      showActiveButtons = false;
    }

    return {
      content: content,
      reason: reason,
      showActiveButtons: showActiveButtons,
      denied: denied,
      supervisor: supervisor,
      admin: admin
    };
  }

  function sendEmailChangeSupervisor(emailUser, supervisorsNameForEmail) {
    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'sendEmailWhenChangeSupervisors?token=' + token + '&emailUser=' + emailUser + '&supervisors=' + supervisorsNameForEmail,
      async: false,
    }).done(function (dataEmail) {
    });
  }

//Refactoring
  //Populate management table for supervisor
  function populateTable() {
    let colorClass, freeDaysStatus, content, obj = [], info, infoEntries, startPage, endPage;
    managementTable = $('#manager-table').DataTable({
      "searching": true,
      "ordering": true,
      "bFilter": true,
      "processing": true,
      "serverSide": true,
      "bServerSide": true,
      "responsive": true,
      "bSorted": true,
      "ajax": {
        "url": appConfig.url + appConfig.api + 'getManagerFreeDays',
        "data": function (data) {
          info = $('#manager-table').DataTable().page.info();
          infoEntries = $('#manager-table').DataTable().page.info().length;
          info.page += 1;
          startPage = info.start;
          endPage = info.end;
          data.token = token;
          data.userID = theUser.userID;
          data.infoPage = info.page;
          data.infoEntries = infoEntries;
          data.totalFreeDays = totalFreeDays;
          data.start = startPage;
          data.end = endPage;
          data.search = data.search.value;
          data.orderObject = data.order;
          data.draw = managementTableRequestSequence;
          managementTableRequestSequence++;
          colorClass = colorTableRow(0);
        },
      },
      columns: [
        {data: "picture"},
        {data: "name"},
        {data: "position"},
        {data: "positionInfo"},
        {data: "email"},
        {data: "startDate"},
        {data: "endDate"},
        {data: "requestDate"},
        {data: "days"},
        {data: "type"},
        {data: "comment"},
        {data: "avfreedays"},
        {data: "approved"},
        {data: "approve"}
      ],
      retrieve: true,
      "columnDefs": [
        {
          "render": function (data, type, row, full) {
            let avatar, avatarStyle, avatarStyleAfter,
              indexNumber = (info.page - 1) * 10 + full.row + 1;
            indexNumber < 10 ? (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"') :
              (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"');
            if (row.picture) {
              avatar = '<div id="avatarimg' + row.userID + '" ' + avatarStyleAfter + '><img id="avatar' + row.userID
                + '" style="border-radius: 50%; object-fit: cover; width: 40px;" src="data:image/png;base64,' + row.picture + '"></div>';
            } else if ((row.picture === "undefined") || !row.picture || row.picture == null) {
              avatar = '<div id="avatarimg' + row.userID + '" class="fa fa-camera fa-2x" ' +
                '' + avatarStyle + '></div>'
            }

            if ((row.picture === "undefined") || (row.picture == null) || (!row.picture)) {
              $("#avatar" + row.userID).removeAttr("src");
              $("#avatarimg" + row.userID).addClass("fa fa-camera fa-2x");
            } else {
              $(".sorting_1 #avatar" + row.userID).attr("src", 'data:image/png;base64,' + row.picture);
              $(".sorting_1 #avatarimg" + row.userID).removeClass("fa fa-camera fa-2x");
            }

            if (info.page === 1) {
              return (full.row + 1) + "" + avatar;
            } else {
              return indexNumber + "" + avatar;
            }
          },
          "targets": 0,
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.positionInfo + "</div>"
          },
          "targets": 3
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.email + "</div>"
          },
          "targets": 4
        },
        {
          "render": function (data, type, row) {
            return moment(row.startDate).format("DD/MM/Y");
          },
          "targets": 5
        },
        {
          "render": function (data, type, row) {
            return moment(row.endDate).format("DD/MM/Y");
          },
          "targets": 6
        },
        {
          "render": function (data, type, row) {
            let requestDate = moment(row.requestDate).format("DD/MM/Y");
            (requestDate === 'Invalid date') ? requestDate = '-' : requestDate;
            return requestDate;
          },
          "targets": 7
        },
        {
          "render": function (data, type, row) {
            let responseObject, commentRow;
            obj = jQuery.parseJSON(row.userObject);
            responseObject = managementHolidayApprove(obj, row.approved);
            reason = responseObject.reason;
            if (reason) {
              reason = reason.replace(/`/g, "%1%")
                .replace(/"/g, "%2%")
                .replace(/'/g, "%3%")
                .replace(/\\/g, '%4%');
            }

            commentRow = row.comment.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;" ' +
              'onclick="displayDialogCommentManagers(\'' + row.name + '\',\'' +
              reason + '\',\'' + commentRow + '\')">' + row.comment + '</div>'
          },
          "targets": 10
        },
        {
          "render": function (data, type, row) {
            freeDaysStatus = "Pending";
            row.approved === 1 ? freeDaysStatus = "Approved" :
              (row.approved === 2 ? freeDaysStatus = "Not Approved" : "Pending");
            return freeDaysStatus;
          },
          "targets": 12
        },
        {
          "render": function (data, type, row) {
            let freeDaysStatus,
              approveButtons = "", responseObject, denied, admin, showActiveButtons, supervisor, avFD, params
            status;
            colorClass = colorTableRow(0);
            obj = jQuery.parseJSON(row.userObject);

            responseObject = managementHolidayApprove(obj, row.approved);
            content = responseObject.content,
              denied = responseObject.denied,
              admin = responseObject.admin,
              showActiveButtons = responseObject.showActiveButtons,
              supervisor = responseObject.supervisor;

            if (denied === true || row.approved === 2) {
              freeDaysStatus = "Not approved";
              colorClass = colorTableRow(2);
              if (row.approved !== 2 && theUser.position === "Supervisor") {
                status = 2;
              }
              showActiveButtons = false;
            } else if (supervisor && admin || row.approved == 1) {
              freeDaysStatus = "Approved";
              colorClass = colorTableRow(1);
              showActiveButtons = false;
            }

            avFD = row.avfreedays;
            params = '"' + row.type + '",' + '"' + row.email + '",' + avFD;
            obj = JSON.stringify(obj);
            if (showActiveButtons === true) {
              approveButtons = "<span class='fa fa-check' style='padding: 10px; font-size: x-large;' " +
                "onclick='displayApproveModal(" + obj + ", this ," + row.id + "," + params + ", 1, true, 0)'></span>";
              approveButtons += "<span class='fa fa-times' style='font-size: x-large'" +
                "onclick='displayApproveModal(" + obj + ", this ," + row.id + "," + params + ", 2, false, 2)'></span>";
            }
            return approveButtons;
          },
          "targets": 13
        },
      ],
      "search": {
        "regex": true,
        "smart": true,
      },
      "createdRow": function (row, data) {
        // Set the data-status attribute, and add a class
        $(row).attr('id', data.id);
        $(row).find('td')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-editable-mode', 'inline')
          .popover();
        $(row).find('td:eq(0)')
          .css('width', '8%');
        $(row).find('td:eq(13)')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-content', content)
          .attr('data-editable-mode', 'inline')
          .popover();
      },
    })
  }

  // Populate hr-ro and hr-md table (local hr table).
  function populateLocalHrTable() {
    let colorClass, freeDaysStatus, content, obj = [], info, infoEntries, startPage, endPage;
    localHrTable = $('#manager-table').DataTable({
      "searching": true,
      "bFilter": true,
      "processing": true,
      "serverSide": true,
      "bServerSide": true,
      "responsive": true,
      "ajax": {
        "url": appConfig.url + appConfig.api + 'getLocalHrFreeDays?',
        "data": function (data) {
          info = $('#manager-table').DataTable().page.info();
          infoEntries = $('#manager-table').DataTable().page.info().length;
          info.page += 1;
          startPage = info.start;
          endPage = info.end;

          data.token = token;
          data.admin = theUser.admin;
          data.country = theUser.country;
          data.userID = theUser.userID;
          data.infoPage = info.page;
          data.infoEntries = infoEntries;
          data.totalFreeDays = totalFreeDays;
          data.start = startPage;
          data.end = endPage;
          data.draw = localHrTableRequestSequence;
          localHrTableRequestSequence++;
          data.search = data.search.value;
          data.orderObject = data.order;
          colorClass = colorTableRow(0);
        },
      },
      columns: [
        {data: "picture"},
        {data: "name"},
        {data: "position"},
        {data: "positionInfo"},
        {data: "email"},
        {data: "startDate"},
        {data: "endDate"},
        {data: "requestDate"},
        {data: "days"},
        {data: "type"},
        {data: "comment"},
        {data: "avfreedays"},
        {data: "approved"},
        {data: "approve"}
      ],
      retrieve: true,
      "columnDefs": [
        {orderable: true},
        {
          "render": function (data, type, row, full) {
            let avatar, avatarStyle, avatarStyleAfter,
              indexNumber = (info.page - 1) * 10 + full.row + 1;
            indexNumber < 10 ? (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"') :
              (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"');
            if (row.picture) {
              avatar = '<div id="avatarimg' + row.userID + '" ' + avatarStyleAfter + '><img id="avatar' + row.userID
                + '" style="border-radius: 50%; object-fit: cover; width: 40px;" src="data:image/png;base64,' + row.picture + '"></div>';
            } else if ((row.picture === "undefined") || !row.picture || row.picture == null) {
              avatar = '<div id="avatarimg' + row.userID + '" class="fa fa-camera fa-2x" ' +
                '' + avatarStyle + '></div>'
            }

            if ((row.picture === "undefined") || (row.picture == null) || (!row.picture)) {
              $("#avatar" + row.userID).removeAttr("src");
              $("#avatarimg" + row.userID).addClass("fa fa-camera fa-2x");
            } else {
              $(".sorting_1 #avatar" + row.userID).attr("src", 'data:image/png;base64,' + row.picture);
              $(".sorting_1 #avatarimg" + row.userID).removeClass("fa fa-camera fa-2x");
            }

            if (info.page === 1) {
              return (full.row + 1) + "" + avatar;
            } else {
              return indexNumber + "" + avatar;
            }
          },
          "targets": 0
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.positionInfo + "</div>"
          },
          "targets": 3
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.email + "</div>"
          },
          "targets": 4
        },
        {
          "render": function (data, type, row) {
            return moment(row.startDate).format("DD/MM/Y");
          },
          "targets": 5
        },
        {
          "render": function (data, type, row) {
            return moment(row.endDate).format("DD/MM/Y");
          },
          "targets": 6
        },
        {
          "render": function (data, type, row) {
            let requestDate = moment(row.requestDate).format("DD/MM/Y");
            (requestDate === 'Invalid date') ? requestDate = '-' : requestDate;
            return requestDate;
          },
          "targets": 7
        },
        {
          "render": function (data, type, row) {
            let responseObject, commentRow;
            obj = jQuery.parseJSON(row.userObject);
            responseObject = managementHolidayApprove(obj, row.approved);
            reason = responseObject.reason;
            if (reason) {
              reason = reason.replace(/`/g, "%1%")
                .replace(/"/g, "%2%")
                .replace(/'/g, "%3%")
                .replace(/\\/g, '%4%');
            }
            commentRow = row.comment.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;"' +
              ' onclick="displayDialogCommentManagers(\'' + row.name + '\',\'' +
              reason + '\',\'' + commentRow + '\')">' + row.comment + '</div>'
          },
          "targets": 10
        },
        {
          "render": function (data, type, row) {
            freeDaysStatus = "Pending";
            row.approved === 1 ? freeDaysStatus = "Approved" :
              (row.approved === 2 ? freeDaysStatus = "Not Approved" : "Pending");
            return freeDaysStatus;
          },
          "targets": 12
        },
        {
          "render": function (data, type, row) {
            let freeDaysStatus = "Pending",
              approveButtons = "", responseObject, denied, admin, showActiveButtons, supervisor, avFD, available,
              params,
              params1, status;

            obj = jQuery.parseJSON(row.userObject);

            responseObject = managementHolidayApprove(obj, row.approved);
            content = responseObject.content;
            admin = responseObject.admin;
            denied = responseObject.denied;
            showActiveButtons = responseObject.showActiveButtons;
            supervisor = responseObject.supervisor;

            if (denied === true || row.approved === 2) {
              freeDaysStatus = "Not approved";
              showActiveButtons = false;
              if (row.approved !== 2) {
                status = '2';
              }
            } else if (supervisor && admin || row.approved === 1) {
              freeDaysStatus = "Approved";
              showActiveButtons = false;
            }

            if (row.type === 'Concediu') {
              avFD = row.avfreedays - row.days;
            } else {
              avFD = row.avfreedays;
            }

            available = row.avfreedays;
            params = '"' + row.type + '",' + '"' + row.email + '",' + available;
            params1 = '"' + row.type + '",' + '"' + row.email + '",' + avFD;

            obj = JSON.stringify(obj);

            if (showActiveButtons === true) {
              approveButtons = "<span class='fa fa-check'  style='padding: 10px; font-size: x-large;' " +
                "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + params1 + ", 1, true, 1)'></span>";
              approveButtons += "<span class='fa fa-times' style='font-size: x-large;'" +
                "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + params + ", 2, false, 2)'></span>";
            }

            (freeDaysStatus === "Approved") ? colorClass = colorTableRow(1) :
              ((freeDaysStatus === "Not approved") ? colorClass = colorTableRow(2) :
                colorClass = colorTableRow(0));

            return approveButtons;
          },
          "targets": 13
        },
      ],
      "createdRow": function (row, data, dataIndex) {
        // Set the data-status attribute, and add a class
        $(row).attr('id', data.id)
        $(row).find('td')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-editable-mode', 'inline')
          .popover();
        $(row).find('td:eq(0)')
          .css('width', '8%');
        $(row).find('td:eq(13)')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-content', content)
          .attr('data-editable-mode', 'inline')
          .popover();
      },
    })
  }

  // HR manager table
  function populateHRTable() {
    let colorClass, freeDaysStatus, obj = [], info, infoEntries, startPage, endPage, content;
    hrAdminTable = $('#manager-table').DataTable({
      "searching": true,
      "bFilter": true,
      "processing": true,
      "serverSide": true,
      "bServerSide": true,
      "responsive": true,
      "ajax": {
        "url": appConfig.url + appConfig.api + 'getHRFreeDays',
        "data": function (data) {
          info = $('#manager-table').DataTable().page.info();
          infoEntries = $('#manager-table').DataTable().page.info().length;
          info.page += 1;
          startPage = info.start;
          endPage = info.end;

          data.token = token;
          data.userID = theUser.userID;
          data.infoPage = info.page;
          data.infoEntries = infoEntries;
          data.totalFreeDays = totalFreeDays;
          data.start = startPage;
          data.end = endPage;
          data.search = data.search.value;
          data.orderObject = data.order;
          data.draw = hrAdminTableRequestSequence;
          hrAdminTableRequestSequence++;
          colorClass = colorTableRow(0);
        },
      },
      columns: [
        {data: "picture"},
        {data: "name"},
        {data: "position"},
        {data: "positionInfo"},
        {data: "email"},
        {data: "startDate"},
        {data: "endDate"},
        {data: "requestDate"},
        {data: "days"},
        {data: "type"},
        {data: "comment"},
        {data: "avfreedays"},
        {data: "approved"},
        {data: "approve"}
      ],
      retrieve: true,
      "columnDefs": [
        {orderable: true},
        {
          "render": function (data, type, row, full) {
            let avatar, avatarStyle, avatarStyleAfter,
              indexNumber = (info.page - 1) * 10 + full.row + 1;
            indexNumber < 10 ? (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"') :
              (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"');
            if (row.picture) {
              avatar = '<div id="avatarimg' + row.userID + '" ' + avatarStyleAfter + '><img id="avatar' + row.userID
                + '" style="border-radius: 50%; object-fit: cover; width: 40px;" src="data:image/png;base64,' + row.picture + '"></div>';
            } else if ((row.picture === "undefined") || !row.picture || row.picture == null) {
              avatar = '<div id="avatarimg' + row.userID + '" class="fa fa-camera fa-2x" ' +
                '' + avatarStyle + '></div>'
            }

            if ((row.picture === "undefined") || (row.picture == null) || (!row.picture)) {
              $("#avatar" + row.userID).removeAttr("src");
              $("#avatarimg" + row.userID).addClass("fa fa-camera fa-2x");
            } else {
              $(".sorting_1 #avatar" + row.userID).attr("src", 'data:image/png;base64,' + row.picture);
              $(".sorting_1 #avatarimg" + row.userID).removeClass("fa fa-camera fa-2x");
            }

            if (info.page === 1) {
              return (full.row + 1) + "" + avatar;
            } else {
              return indexNumber + "" + avatar;
            }
          },
          "targets": 0
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.positionInfo + "</div>"
          },
          "targets": 3
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              row.email + "</div>"
          },
          "targets": 4
        },
        {
          "render": function (data, type, row) {
            return moment(row.startDate).format("DD/MM/Y");
          },
          "targets": 5
        },
        {
          "render": function (data, type, row) {
            return moment(row.endDate).format("DD/MM/Y");
          },
          "targets": 6
        },
        {
          "render": function (data, type, row) {
            let requestDate = moment(row.requestDate).format("DD/MM/Y");
            (requestDate === 'Invalid date') ? requestDate = '-' : requestDate;
            return requestDate;
          },
          "targets": 7
        },
        {
          "render": function (data, type, row) {
            let responseObject, commentRow;
            obj = jQuery.parseJSON(row.userObject);
            responseObject = managementHolidayApprove(obj, row.approved);
            reason = responseObject.reason;
            reason = reason.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
            commentRow = row.comment.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;" ' +
              'onclick="displayDialogCommentManagers(\'' + row.name + '\',\'' +
              reason + '\',\'' + commentRow + '\')">' + row.comment + '</div>'
          },
          "targets": 10
        },
        {
          "render": function (data, type, row) {
            freeDaysStatus = "Pending";
            row.approved === 1 ? freeDaysStatus = "Approved" :
              (row.approved === 2 ? freeDaysStatus = "Not Approved" : "Pending");
            return freeDaysStatus;
          },
          "targets": 12
        },
        {
          "render": function (data, type, row) {
            let freeDaysStatus = "Pending",
              approveButtons = "", responseObject, denied, admin, showActiveButtons, supervisor, avFD, avFd, param,
              params1;

            obj = jQuery.parseJSON(row.userObject);
            responseObject = managementHolidayApprove(obj, row.approved);
            content = responseObject.content;
            admin = responseObject.admin;
            denied = responseObject.denied;
            showActiveButtons = responseObject.showActiveButtons;
            supervisor = responseObject.supervisor;


            if (denied === true || row.approved === 2) {
              freeDaysStatus = "Not approved";
              colorClass = colorTableRow(2);
              showActiveButtons = false;
            } else if (supervisor && admin || row.approved === 1) {
              freeDaysStatus = "Approved";
              colorClass = colorTableRow(1);
              showActiveButtons = false;
            }

            if (row.type === 'Concediu' || row.approved === 1) {
              avFD = row.avfreedays - row.days;
            } else {
              avFD = row.avfreedays;
            }


            avFd = row.avfreedays;
            param = '"' + row.type + '",' + '"' + row.email + '",' + avFD;
            params1 = '"' + row.type + '",' + '"' + row.email + '",' + avFd;

            obj = JSON.stringify(obj);
            if ((showActiveButtons === true) && (freeDaysStatus !== "Not approved")) {
              approveButtons = "<span class='fa fa-check' style='padding: 10px; font-size: x-large;'" +
                "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + param + ", 1,  true, 1)'></span>";
              approveButtons += "<span class='fa fa-times' style='font-size: x-large;'" +
                "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + params1 + ", 2, false, 2)'></span>";
            } else if ((showActiveButtons === false) && (row.approved === 1)) {
              if (row.type === 'Concediu') {
                avFD = row.avfreedays + row.days;
              } else {
                avFD = row.avfreedays;
              }
              param = '"' + row.type + '",' + '"' + row.email + '",' + avFD;
              approveButtons += "<span class='fa fa-times' style='font-size: x-large;'" +
                " onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + param + ", 2, true, 2)' " +
                "approvedAdmin='0'></span>";
            }

            (freeDaysStatus === "Approved") ? colorClass = colorTableRow(1) :
              ((freeDaysStatus === "Not approved") ? colorClass = colorTableRow(2) :
                colorClass = colorTableRow(0));

            return approveButtons;
          },
          "targets": 13
        },
      ],
      "createdRow": function (row, data) {
        // Set the data-status attribute, and add a class
        $(row).attr('id', data.id)
        $(row).find('td')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-editable-mode', 'inline')
          .popover();
        $(row).find('td:eq(0)')
          .css('width', '8%');
        $(row).find('td:eq(13)')
          .addClass(colorClass)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'bottom')
          .attr('data-content', content)
          .attr('data-editable-mode', 'inline')
          .popover();
      },
    })
  }

  function callDependencies(admin_dependencies, managers, insertedUserId) {
    var rel_or = [],
      rel_and = [];
    for (var i = 0; i < admin_dependencies.length; i++) {
      for (var j in admin_dependencies[i]) {
        if (j != '1') {
          rel_or.push({
            id: admin_dependencies[i][j]
          });
        } else {
          rel_and.push({
            id: admin_dependencies[i][j]
          });
        }
      }
    }
    var managementObj = {},
      ids = [],
      dep_ids = [];

    if (rel_and.length > 0) {
      for (var i = 0; i < rel_and.length; i++) {
        ids.push(rel_and[i]);
      }
      managementObj = {
        ids: ids,
        rel: 'OR',
        dep: {}
      };
      if (rel_or.length > 0) {
        for (var i = 0; i < rel_or.length; i++) {
          dep_ids.push(rel_or[i]);
        }
        managementObj.dep = {
          ids: dep_ids,
          rel: 'OR'
        };
      }
    } else {
      if (rel_or.length > 0) {
        for (var i = 0; i < rel_or.length; i++) {
          ids.push(rel_or[i]);
        }
        managementObj = {
          ids: ids,
          rel: 'OR',
          dep: {}
        };
      }
    }

    var params = '&token=' + token + '&userObject=' + JSON.stringify(managementObj) + "&userID=" + insertedUserId;
    $.ajax({
      type: 'POST',
      url: appConfig.url + appConfig.api + 'updateUserObject?' + params,
      async: false
    }).then(function () {
    });
  }

  function getAllUsers() {
    let country, visible = false, colorRow, colorName, columns, info, infoEntries, startPage, endPage;
    (theUser.admin === 2 || theUser.admin === 3 || theUser.admin === 6) ? country = false :
      (theUser.admin === 4 && theUser.country === "ro" ? country = "ro" :
        (theUser.admin === 4 && theUser.country === "ro" ?
          country = "md" : ''));

    columns = [
      {data: "name"},
      {data: "country"},
      {data: "position"},
      {data: "positionInfo"},
      {data: "email"},
      {data: "startDate"},
      {data: "endDate"},
      {data: "phone"},
      {data: "supervisor"},
      {data: "active"},
      {data: "age"},
      {data: "freedays"},
      {data: "bonus"}
    ];

    $('#row').css("display", "none");
    if (theUser.admin === 6 || theUser.admin === 3) {
      $('#row').css("display", "block");
      visible = true;
      columns.push({data: "actions"});
    }

    getAllUsersTable = $('#users-list-table').DataTable({
      "searching": true,
      "ordering": true,
      "bFilter": true,
      "processing": true,
      "serverSide": true,
      "bServerSide": true,
      "responsive": true,
      "bSorted": true,
      "ajax": {
        "url": appConfig.url + appConfig.api + 'getAllUsers',
        "data": function (data) {
          info = $('#users-list-table').DataTable().page.info();
          infoEntries = $('#users-list-table').DataTable().page.info().length;
          info.page += 1;
          startPage = info.start;
          endPage = info.end;
          data.token = token;
          data.inactiveUsers = localStorage.getItem('activeUsers');
          data.userID = theUser.userID;
          data.admin = theUser.admin;
          data.country = country;
          data.infoPage = info.page;
          data.infoEntries = infoEntries;
          data.start = startPage;
          data.end = endPage;
          data.search = data.search.value;
          data.orderObject = data.order;
          data.draw = allUsersTableRequestSequence;
          allUsersTableRequestSequence++;
          colorClass = colorTableRow(0);
        },
      },
      columns: columns,
      retrieve: true,
      "columnDefs": [
        {
          "render": function (data, type, row, full) {
            let avatar, avatarStyle, avatarStyleAfter,
              indexNumber = (info.page - 1) * 10 + full.row + 1;
            indexNumber < 10 ? (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"') :
              (avatarStyle = 'style="display: inline; float: right"',
                avatarStyleAfter = 'style=" display: inline; float: right"');
            if (row.picture) {
              avatar = '<div id="avatarimg' + row.userID + '" ' + avatarStyleAfter + '><img id="avatar' + row.userID
                + '" style="border-radius: 50%; object-fit: cover; width: 40px;"' +
                ' src="data:image/png;base64,' + row.picture + '"></div>';
            } else if ((row.picture === "undefined") || !row.picture || row.picture == null) {
              avatar = '<div id="avatarimg' + row.userID + '" class="fa fa-camera fa-2x" ' +
                '' + avatarStyle + '></div>'
            }

            if ((row.picture === "undefined") || (row.picture == null) || (!row.picture)) {
              $("#avatar" + row.userID).removeAttr("src");
              $("#avatarimg" + row.userID).addClass("fa fa-camera fa-2x");
            } else {
              $(".sorting_1 #avatar" + row.userID).attr("src", 'data:image/png;base64,' + row.picture);
              $(".sorting_1 #avatarimg" + row.userID).removeClass("fa fa-camera fa-2x");
            }

            if (info.page === 1) {
              return (full.row + 1) + "" + avatar;
            } else {
              return indexNumber + "" + avatar;
            }
          },
          "targets": 0
        },
        {
          "render": function (data, type, row) {
            return row.name;
          },
          "targets": 1
        },
        {
          "render": function (data, type, row) {
            return row.country;
          },
          "targets": 2
        },
        {
          "render": function (data, type, row) {
            return row.position;
          },
          "targets": 3
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:100px;">' +
              row.positionInfo + "</div>"
          },
          "targets": 4
        },
        {
          "render": function (data, type, row) {
            return '<div style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:100px;">' +
              row.email + "</div>"
          },
          "targets": 5
        },
        {
          "render": function (data, type, row) {
            return moment(row.startDate).format("DD/MM/Y");
          },
          "targets": 6
        },
        {
          "render": function (data, type, row) {
            return row.phone;
          },
          "targets": 7
        },
        {
          "render": function (data, type, row) {
            let result = " ",
              userObj = {},
              searchSupervisors = [],
              sups, supsIds = [];
            userObj = JSON.parse(row['userObject'])['ids'];
            for (var j = 0; j < userObj.length; j++) {
              if (userObj[j]['id']) {
                searchSupervisors.push(userObj[j]['id']);
              }
            }
            sups = searchForSupervisors(searchSupervisors, arrayResult);
            if (sups.length > 1) {
              for (var p = 0; p < sups.length; p++) {
                result += sups[p].name + ', ';
                supsIds.push(sups[p].id);
              }
            } else {
              result += sups[0].name;
              supsIds.push(sups[0].id);
            }

            if (row.isActive === 0) {
              colorRow = 'warning';
              active = 'false';
            } else {
              colorRow = '';
              active = 'true';
            }

            (!result.indexOf(" Administrator") && row.isActive !== 0 && theUser.admin === 6)
              ? colorName = 'green' : colorName = 'black';


            return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
              result + "</div>";
          },
          "targets": 8
        },
        {
          "render": function (data, type, row) {
            let active;
            if (row.isActive === 1) {
              active = 'true';
            } else {
              active = 'false';
            }
            return active;
          },
          "targets": 9
        },
        {
          "render": function (data, type, row) {
            return moment().diff(row.age, 'years', false);
          },
          "targets": 10
        },
        {
          "render": function (data, type, row) {
            return row.avfreedays;
          },
          "targets": 11
        },
        {
          "render": function (data, type, row) {
            return row.bonus;
          },
          "targets": 12
        },
        {
          "render": function (data, type, row) {
            let result = " ",
              userObj = {}, sups,
              searchSupervisors = [],
              supsIds = [];
            userObj = JSON.parse(row['userObject'])['ids'];

            for (let j = 0; j < userObj.length; j++) {
              if (userObj[j]['id']) {
                searchSupervisors.push(userObj[j]['id']);
              }
            }
            sups = searchForSupervisors(searchSupervisors, arrayResult);
            if (sups.length > 1) {
              for (let p = 0; p < sups.length; p++) {
                result += sups[p].name + ', ';
                supsIds.push(sups[p].id);
              }
            } else {
              result += sups[0].name;
              supsIds.push(sups[0].id);
            }

            return '<a class="btn btn-default fa fa-edit" href="#" data-toggle="modal" ' +
              'data-target="#myModalUser" name="editUser"' +
              ' onclick="managerEditUser(this ,' + row.userID + ', \'' + supsIds + '\')"></a>'
          },
          "targets": 13, visible: visible, orderable: false
        },
      ],
      "search": {
        "regex": true,
        "smart": true,
      },
      "createdRow": function (row) {
        // Set the data-status attribute, and add a class
        $(row).find('td')
          .addClass(colorRow)
          .attr('role', 'button')
          .attr('data-toggle', 'popover')
          .attr('data-trigger', 'hover')
          .attr('data-placement', 'right')
          .attr('data-editable-mode', 'inline')
          .popover();
        $(row).find('td:eq(0)')
          .css('width', '8%')
        $(row).find('td:eq(1)')
          .css('color', colorName)
      },
    })
  }
}

//Admin tabel for management
function populateAdminTable() {
  var colorClass, freeDaysStatus, obj = [], info, infoEntries, startPage, endPage, content, colorName;
  adminTable = $('#manager-table').DataTable({
    "searching": true,
    "bFilter": true,
    "processing": true,
    "serverSide": true,
    "bServerSide": true,
    "responsive": true,
    "ajax": {
      "url": appConfig.url + appConfig.api + 'getAdminFreeDays',
      "data": function (data) {
        info = $('#manager-table').DataTable().page.info();
        infoEntries = $('#manager-table').DataTable().page.info().length;
        info.page += 1;
        startPage = info.start;
        endPage = info.end;
        data.token = token;
        data.userID = theUser.userID;
        data.infoPage = info.page;
        data.infoEntries = infoEntries;
        data.totalFreeDays = totalFreeDays;
        data.start = startPage;
        data.end = endPage;
        data.search = data.search.value;
        data.orderObject = data.order;
        data.draw = adminTableRequestSequence;
        adminTableRequestSequence++;
        colorClass = colorTableRow(0);
      },
    },
    columns: [
      {data: "picture"},
      {data: "name"},
      {data: "position"},
      {data: "positionInfo"},
      {data: "email"},
      {data: "startDate"},
      {data: "endDate"},
      {data: "requestDate"},
      {data: "days"},
      {data: "type"},
      {data: "comment"},
      {data: "avfreedays"},
      {data: "approved"},
      {data: "approve"}
    ],
    retrieve: true,
    "columnDefs": [
      {
        "render": function (data, type, row, full) {
          let avatar, avatarStyle, avatarStyleAfter,
            indexNumber = (info.page - 1) * 10 + full.row + 1;
          indexNumber < 10 ? (avatarStyle = 'style="display: inline; float: right"',
              avatarStyleAfter = 'style=" display: inline; float: right"') :
            (avatarStyle = 'style="display: inline; float: right"',
              avatarStyleAfter = 'style=" display: inline; float: right"');
          if (row.picture) {
            avatar = '<div id="avatarimg' + row.userID + '" ' + avatarStyleAfter + '><img id="avatar' + row.userID
              + '" style="border-radius: 50%; object-fit: cover; width: 40px;" src="data:image/png;base64,' + row.picture + '"></div>';
          } else if ((row.picture === "undefined") || !row.picture || row.picture == null) {
            avatar = '<div id="avatarimg' + row.userID + '" class="fa fa-camera fa-2x" ' +
              '' + avatarStyle + '></div>'
          }

          if ((row.picture === "undefined") || (row.picture == null) || (!row.picture)) {
            $("#avatar" + row.userID).removeAttr("src");
            $("#avatarimg" + row.userID).addClass("fa fa-camera fa-2x");
          } else {
            $(".sorting_1 #avatar" + row.userID).attr("src", 'data:image/png;base64,' + row.picture);
            $(".sorting_1 #avatarimg" + row.userID).removeClass("fa fa-camera fa-2x");
          }

          if (info.page === 1) {
            return (full.row + 1) + "" + avatar;
          } else {
            return indexNumber + "" + avatar;
          }
        },
        "targets": 0
      },
      {
        "render": function (data, type, row) {
          return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
            row.positionInfo + "</div>"
        },
        "targets": 3
      },
      {
        "render": function (data, type, row) {
          return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;">' +
            row.email + "</div>"
        },
        "targets": 4
      },
      {
        "render": function (data, type, row) {
          return moment(row.startDate).format("DD/MM/Y");
        },
        "targets": 5
      },
      {
        "render": function (data, type, row) {
          return moment(row.endDate).format("DD/MM/Y");
        },
        "targets": 6
      },
      {
        "render": function (data, type, row) {
          let requestDate = moment(row.requestDate).format("DD/MM/Y");
          (requestDate === 'Invalid date') ? requestDate = '-' : requestDate;
          return requestDate;
        },
        "targets": 7
      },
      {
        "render": function (data, type, row) {
          let responseObject, commentRow;
          obj = jQuery.parseJSON(row.userObject);
          responseObject = managementHolidayApprove(obj, row.approved);
          reason = responseObject.reason;
          if (reason) {
            reason = reason.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
          }
          commentRow = row.comment.replace(/`/g, "%1%")
            .replace(/"/g, "%2%")
            .replace(/'/g, "%3%")
            .replace(/\\/g, '%4%');
          return '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 100px;"' +
            ' onclick="displayDialogCommentManagers(\'' + row.name + '\',\'' +
            reason + '\',\'' + commentRow + '\')">' + row.comment + '</div>'
        },
        "targets": 10
      },
      {
        "render": function (data, type, row) {
          freeDaysStatus = "Pending";
          row.approved === 1 ? freeDaysStatus = "Approved" :
            (row.approved === 2 ? freeDaysStatus = "Not Approved" : "Pending");
          return freeDaysStatus;
        },
        "targets": 12
      },
      {
        "render": function (data, type, row) {
          let freeDaysStatus = "Pending",
            approveButtons = "", responseObject, denied, admin, showActiveButtons, supervisor, avFD, avFd, param,
            params1;

          obj = jQuery.parseJSON(row.userObject);
          responseObject = managementHolidayApprove(obj, row.approved);
          content = responseObject.content;
          admin = responseObject.admin;
          denied = responseObject.denied;
          showActiveButtons = responseObject.showActiveButtons;
          supervisor = responseObject.supervisor;

          if (denied === true) {
            freeDaysStatus = "Not approved";
            colorClass = colorTableRow(2);
            showActiveButtons = false;
          } else if (supervisor && admin || row.approved === 1) {
            freeDaysStatus = "Approved";
            colorClass = colorTableRow(1);
          }

          if (row.type === 'Concediu') {
            avFD = row.avfreedays - row.days;
          } else {
            avFD = row.avfreedays;
          }

          avFd = row.avfreedays;
          param = '"' + row.type + '",' + '"' + row.email + '",' + avFD;
          params1 = '"' + row.type + '",' + '"' + row.email + '",' + avFd;

          isOk = true;
          if (!obj.dep.ids) {
            colorName = 'green';
          } else {
            colorName = 'black';
          }
          obj = JSON.stringify(obj);


          if (!admin && row.approved === 0) {
            approveButtons = "<span class='fa fa-check' style='padding: 10px; font-size: x-large;'" +
              "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + param + ", 1, true, 1)'></span>";
            approveButtons += "<span class='fa fa-times' style='font-size: x-large;'" +
              "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + params1 + ", 2, false, 2)'></span>";
          } else if (row.approved === 1) {
            let ava, paramsNot;
            if (row.type === 'Concediu') {
              ava = row.avfreedays + row.days;
            } else {
              ava = row.avfreedays;
            }
            paramsNot = '"' + row.type + '",' + '"' + row.email + '",' + ava;
            approveButtons += "<span class='fa fa-times' style='font-size: x-large;'" +
              "onclick='displayApproveModal(" + obj + ",this ," + row.id + "," + paramsNot + ", 2, true, 2)'></span>";
          }

          (freeDaysStatus === "Approved") ? colorClass = colorTableRow(1) :
            ((freeDaysStatus === "Not approved") ? colorClass = colorTableRow(2) :
              colorClass = colorTableRow(0));

          return approveButtons;
        },
        "targets": 13
      },
    ],
    "createdRow": function (row, data) {
      $(row).attr('id', data.id);
      $(row).find('td')
        .addClass(colorClass)
        .attr('role', 'button')
        .attr('data-toggle', 'popover')
        .attr('data-trigger', 'hover')
        .attr('data-placement', 'bottom')
        .attr('data-editable-mode', 'inline')
        .popover();
      $(row).find('td:eq(0)')
        .css('width', '8%')
      $(row).find('td:eq(1)')
        .css('color', colorName)
      $(row).find('td:eq(13)')
        .addClass(colorClass)
        .attr('role', 'button')
        .attr('data-toggle', 'popover')
        .attr('data-trigger', 'hover')
        .attr('data-placement', 'bottom')
        .attr('data-content', content)
        .attr('data-editable-mode', 'inline')
        .popover();
    },
  })
}


function setStatus(value) {
  return value === 0 ? 'Pending' : (value === 1 ? 'Approved' : 'Not approved');
}

function populate(admin) {
  if (admin === 1) {
    populateTable();
  }
  if (admin === 3) {
    populateHRTable();
  }
  if (admin === 4) {
    populateLocalHrTable();
  }
  if (admin === 6) {
    populateAdminTable();
  }
}

function displayDialogCommentManagers(name, approverComment, comment) {
  if (approverComment) {
    approverComment = approverComment.replace(/%1%/g, "`")
      .replace(/%2%/g, '"')
      .replace(/%3%/g, "'")
      .replace(/%4%/g, '\\');
  }
  comment = comment.replace(/%1%/g, "`")
    .replace(/%2%/g, '"')
    .replace(/%3%/g, "'")
    .replace(/%4%/g, '\\');
  $("#dialog-comment .modal-title").text("From: " + name);
  $("#dialog-comment textarea:first-of-type").text(comment);
  if (approverComment == 'undefined' || approverComment == '') {
    $("#dialog-comment textarea:not(:first-of-type)").css('display', 'none');
    $("#dialog-comment .modal-body h4").css('display', 'none');
  } else {
    $("#dialog-comment textarea:not(:first-of-type)").text(approverComment);
    $("#dialog-comment textarea:not(:first-of-type)").css('display', 'block');
    $("#dialog-comment .modal-body h4").css('display', 'block');
  }
  $("#dialog-comment").modal('show');
}

function getSupervisorsName(idsSuperiors) {
  var interval = '';
  numb++, arrayResult = [];
  for (let i in idsSuperiors) {
    if (i == 0) {
      interval = idsSuperiors[i];
    } else {
      interval += ' , ' + idsSuperiors[i];
    }
  }
  $.ajax({
    type: 'GET',
    url: appConfig.url + appConfig.api + 'getSupervisorsName?token=' + token,
    async: false
  }).done(function (searchedSupervisors) {
    for (let j = 0; j < searchedSupervisors.length; j++) {
      arrayResult.push({
        id: searchedSupervisors[j]['userID'],
        name: searchedSupervisors[j]['name'],
        email: searchedSupervisors[j]['email']
      });
    }
  });
  return arrayResult;
}

function getUserSuperiors(arraySuperiors) {
  var idsSuperiors = [];
  for (let i in arraySuperiors.ids) {
    idsSuperiors.push(arraySuperiors.ids[i].id)
  }

  if (arraySuperiors.dep) {
    for (let j in arraySuperiors.dep.ids) {
      idsSuperiors.push(arraySuperiors.dep.ids[j].id)
    }
  }
  idsSuperiorsOLD = idsSuperiors;
  return idsSuperiorsOLD;
}

function searchForSupervisors(searchSupervisors, arrayManaged) {
  var arrayManagedPerUser = [];
  for (let i in arrayManaged) {
    for (let j in searchSupervisors) {
      if (searchSupervisors[j] === arrayManaged[i].id) {
        arrayManagedPerUser.push({
          id: arrayManaged[i]['id'],
          name: arrayManaged[i]['name'],
          email: arrayManaged[i]['email']
        });
      }
    }
  }
  return arrayManagedPerUser;
}
