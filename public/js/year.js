$(document).ready(function () {
  var theUser = JSON.parse(localStorage.getItem('user')),
    token = localStorage.getItem('token');

  if ((theUser.admin === 2) || (theUser.admin == 6)) {
    var ldate = moment(),
      year = moment().year(),
      today = new Date().getFullYear(),
      id,
      holidayid;

    function checkYear() {
      $.get(appConfig.url + appConfig.api + 'selectLastYear?token=' + token + '&year=' + year, function (data) {
        out(data.code);
        if ((data.length === 0) || (today > data[0].year)) {
          displayForm();
          $.get(appConfig.url + appConfig.api + 'insertLastYear?token=' + token + '&year=' + today, function (data) {
            out(data.code);
          });
          $.get(appConfig.url + appConfig.api + 'legalHolidaysToDb', function (data) {
            out(data.code);
          });
          $.get(appConfig.url + appConfig.api + 'legalHolidaysMDToDb', function (data) {
            out(data.code);
          });
        }
      });
    }

    checkYear();
  }

  // Load the New Year Days Off form on button click.
  $("a[name='udpateinfo']").click(function () {
    displayFormClick();
  });

  function newYearForm() {
    //New year update
    var date = new Date();
    date.setDate(date.getDate());
    $('#stholi').datepicker({
      format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
      $('#new-year-form').formValidation('revalidateField', 'stholi');
    });

    $("#new-year-form").formValidation({
      framework: 'bootstrap',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {}
    }).on('submit', function (e, data) {
      if (!e.isDefaultPrevented()) {
        var formWrapper = $("#new-year-form"),
          avfreedays = formWrapper.find("input[name = 'avfreedays']").val(),
          name = formWrapper.find("input[name ='newholiday']").val(),
          startDate = formWrapper.find("input[name = 'stholi']").val();
        $.get(appConfig.url + appConfig.api + 'updateAllAvFreeDays?token=' + token + '&avfreedays=' + avfreedays, function (data) {
          out(data.code);
        });
      }
      e.preventDefault();
      $('.modal-body> div:first-child').css('display', 'block');
      $('#myModalOncePerYear').modal('hide');
    });
  };

  function newHolidayForm() {
    //New holiday update
    var date = new Date();
    date.setDate(date.getDate());
    $('#stholi').datepicker({
      format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
      $('#new-year-form').formValidation('revalidateField', 'stholi');
    });

    $("#new-year-form").formValidation({
      framework: 'bootstrap',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        newholiday: {
          validators: {
            notEmpty: {
              message: 'This is required'
            }
          }
        },
        stholi: {
          validators: {
            notEmpty: {
              message: 'The start date is required'
            }
          }
        }
      }
    }).on('submit', function (e, data) {
      if (!e.isDefaultPrevented()) {
        var formWrapper = $("#new-year-form"),
          name = formWrapper.find("input[name ='newholiday']").val(),
          startDate = formWrapper.find("input[name = 'stholi']").val();
        socket.emit('/RefreshCalendar', {
          token: token,
        });
        if (($("#rom").attr("checked") == 'checked')) {
          $.get(appConfig.url + appConfig.api + 'insertNewHoliday?token=' + token + '&startDate=' + startDate + '&name=' + name, function (data) {
            out(data.code);
          });
        } else if (($("#mold").attr("checked") == 'checked')) {
          $.get(appConfig.url + appConfig.api + 'insertNewHolidayMD?token=' + token + '&startDate=' + startDate + '&name=' + name, function (data) {
            out(data.code);
          });
        }

        $('#myModalOncePerYear').modal('hide');
      }
      e.preventDefault();
    });
    var table = $('#example').DataTable();
    getAllHolidays('ro');
    $("input[name = 'country']").click(function () {
      if ($(this).attr('value') == "ro") {
        table
          .clear()
          .draw();
        $(this).attr('checked', true);
        $("#mold").attr('checked', false);
        getAllHolidays('ro');
      }

      if ($(this).attr("value") == "md") {
        table
          .clear()
          .draw();
        $(this).attr('checked', true);
        $('#rom').attr('checked', false);
        getAllHolidays('md');
      }
    });
  }

  function updateHolidayForm(country, holidayid) {
    //Update holidays
    var date = new Date();
    date.setDate(date.getDate());
    $('#stholi').datepicker({
      format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
      $('#edit-holiday-form').formValidation('revalidateField', 'stholi');
    });

    $("#edit-holiday-form").formValidation({
      framework: 'bootstrap',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        stholi: {
          validators: {
            date: {
              format: 'YYYY-MM-DD',
              message: 'The value is not a valid date'
            }
          }
        },
      }
    }).on('submit', function (e, data) {
      if (!e.isDefaultPrevented()) {
        var formWrapper = $("#edit-holiday-form"),
          name = formWrapper.find("input[name ='name']").val(),
          startDate = formWrapper.find("input[name = 'stholi']").val();

        if (country == "md") {
          $.get(appConfig.url + appConfig.api + 'updateHolidaysMD?token=' + token + '&id=' + holidayid + '&startDate=' + startDate + '&name=' + name + '&type=public', function (datah) {
            socket.emit('/RefreshCalendar', {
              token: token,
            });
          });
        } else {
          $.get(appConfig.url + appConfig.api + 'updateHolidays?token=' + token + '&id=' + holidayid + '&startDate=' + startDate + '&name=' + name + '&type=public', function (datah) {
            socket.emit('/RefreshCalendar', {
              token: token,
            });
          });
        }

      }

      e.preventDefault();
      $('.modal-body> div:first-child').css('display', 'block');
      setTimeout(function () {
        $('#myModalOncePerYear').modal('hide');
      }, 1000);

    })
  }

  function getAllHolidays(country) {
    $.ajax({
      type: 'GET',
      url: appConfig.url + appConfig.api + 'getAllHolidays?token=' + token + '&country=' + country,
      success: function (data) {
        var holidaytable = $('#example').DataTable(),
          j = 1;
        for (var i in data) {
          holidaytable.row.add([
            j,
            moment(data[i].startDate).format("YYYY-MM-DD"),
            data[i].name
          ]).draw(false)
            .nodes()
            .to$()
            .attr("holiday-id", +data[i].id);
          j++;
        }

        $('#example tbody').on('click', 'tr', function () {
          id = $(this).find("td:nth-child(1)").html();
          var data = $(this).find("td:nth-child(2)").html(),
            name = $(this).find("td:nth-child(3)").html();
          holidayid = $(this).attr("holiday-id");
          displayFormOnUpdateClick(id, holidayid, name, data, holidayid);
        });
      },
      async: false
    });
  }

  function displayForm() {
    $('#myModalOncePerYear').modal({
      backdrop: 'static',
      keyboard: false
    });

    $("#myModalOncePerYear").load("newyearform.html", function () {
      newYearForm();
      $('#myModalOncePerYear').modal('show');

    });
  }

  function displayFormClick() {
    $("#myModalOncePerYear").load("updateinfo.html", function () {
      newHolidayForm();
      $('#myModalOncePerYear').modal('show');
    });
  }

  function displayFormOnUpdateClick(id, holidayid, name, data, holidayid) {
    var country = $('input[name="country"]:checked').val();
    $("#myModalOncePerYear").load("editholiday.html", function () {
      $("#nume").val(name);
      $("div > #dateval").val(data);

      $("#delete-holiday").click(function () {
        $('#myModalOncePerYear').modal('hide');
        $("#delete-modal-holiday").modal('show');
        $("#delete-modal-holiday-btn-yes").one('click', function () {
          deleteLegalHolidays(holidayid, country);
          $("#delete-modal-holiday").modal('hide');
        });

        $("#delete-modal-holiday-btn-no").click(function () {
          $('#myModalOncePerYear').modal('show');
          $("#delete-modal-holiday").modal('hide');
        });
      });
      updateHolidayForm(country, holidayid);
      $('#myModalOncePerYear').modal('show');
    });
  }

  function out(data) {
    if (data === 110) {
      if (!appConfig.sessionInvalid) {
        appConfig.sessionInvalid = true;
        alert('Session expired');
        $.post(appConfig.url + appConfig.api + 'logout', {
          email: theUser.email
        });
        window.location.href = 'index.html';
      }
    }
  }

  function deleteLegalHolidays(holidayid, country) {
    $.post(appConfig.url + appConfig.api + 'deleteLegalHoliday?token=' + token, {
      id: holidayid,
      country: country
    }).done(function (data) {
      socket.emit('/RefreshCalendar', {
        token: token,
      });
      $("#example").DataTable().clear();
      getHolidays();
    });
  }
});
