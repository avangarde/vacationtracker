"use strict";
window.appNameSpace = window.appNameSpace || {};
window.sessionInvalid = false;

var socket = io(appConfig.io),
  acc = "Pending",
  reason,
  colorClass = colorTableRow(0),
  content = "",
  userTable,
  userTableRequestSequence = 0,
  countFreeDays = 0,
  comment = '',
  indexTable = 0,
  linkLocationRef = appConfig.linkLocationRef;

if (theUser.admin === 0) {
  $('#managementTab').css("display", "none");
  allUsers();
}

$(document).ready(function () {
  $("#tabClickMyHolidays").on('click', function () {
    allUsers();
    getHolidays(1);
  })
});

socket.on('/resdeleteHoliday', function (data) {
  if (data != token) {
    allUsers();
    getHolidays(1);
    userTable.ajax.reload(null, false);
  }
});

socket.on('/resupdatedate', function (data) {
  if (theUser.admin !== 6 && theUser.userID === data.userId) {
    allUsers();
    getHolidays(1);
    userTable.ajax.reload(null, false);
  }
});

socket.on('/resApproveFreeDays', function (data) {
  if (theUser.admin !== 6) {
    allUsers();
    getHolidays(1);
    userTable.ajax.reload(null, false);
  }
});

socket.on('/resSetApproved', function (data) {
  if (theUser.email === data.email) {
    allUsers();
    getHolidays(1);
    userTable.ajax.reload(null, false);
  }
});

function allUsers() {
  indexTable = 0;
  $.ajax({
    type: 'GET',
    url: appConfig.url + appConfig.api + 'getAllUserFreeDays?token=' + token + '&userID=' + theUser.userID,
    async: false,
  }).done(function (data) {
    countFreeDays = data[0].count;
  });
}

function holidayApprove(obj, id) {
  let number = obj.ids.length,
    j = 0,
    content = "",
    result,
    supervisor,
    admin;
  result = arrayResult;
  //Supervisori - 2 dependecies
  if (obj.rel === "AND" && id) {
    for (let i in obj.ids) {
      let k = 0;
      while (k < result.length) {
        if (obj.ids[i].id === result[k].id) {
          var name = result[k].name;
        }
        k++;
      }
      if (!obj.ids[i].approved) {
        colorClass = colorTableRow(0);
        reason = '';
        content += ' ' + name + ': ' + setStatus(0) + '\n';
      }
      if (obj.ids[i].approved === 2) {
        colorClass = colorTableRow(2);
        reason = obj.ids[i].reason;
        content = ' ' + name + ': ' + setStatus(2) + '\n';
        break;
      }
      if (obj.ids[i].approved === 1) {
        j++;
        reason = '';
        content += ' ' + name + ': ' + setStatus(1) + '\n';
      }

      if (j === number) {
        colorClass = colorTableRow(0);
      }
    }
  }
  var contentS = content;
  if (obj.dep.rel === "OR" && id) {
    for (let j in obj.dep.ids) {
      let i = 0;
      while (i < result.length) {
        if (obj.dep.ids[j].id === result[i].id) {
          var name = result[i].name;
        }
        i++;
      }
      if (!obj.dep.ids[j].approved && supervisor !== true) {
        colorClass = colorTableRow(0);
        reason = '';
        content += ' ' + name + ': ' + setStatus(0) + '\n';
      }
      if (obj.dep.ids[j].approved === 2) {
        colorClass = colorTableRow(2);
        reason = obj.dep.ids[j].reason;
        content = ' ' + name + ': ' + setStatus(2) + '\n';
        break;
      }

      if (obj.dep.ids[j].approved === 1) {
        reason = '';
        if (content.indexOf('Approved') !== -1) {
          content = contentS + name + ': ' + setStatus(1) + '\n';
          break;
        } else {
          supervisor = true;
          colorClass = colorTableRow(1);
          content = name + ': ' + setStatus(1) + '\n';
        }
      }
    }
  }

  // Manageri - 1 dependence
  if (obj.rel === "OR" && !obj.dep.rel && id) {
    content = "";
    for (let i in obj.ids) {
      let j = 0;
      while (j < result.length) {
        if (obj.ids[i].id === result[j].id) {
          var name = result[j].name;
        }
        j++;
      }
      if (!obj.ids[i].approved && !admin) {
        reason = '';
        colorClass = colorTableRow(0);
        content += ' ' + name + ': ' + setStatus(0) + '\n';
      }
      if (obj.ids[i].approved === 2) {
        colorClass = colorTableRow(2);
        reason = obj.ids[i].reason;
        content = ' ' + name + ': ' + setStatus(2) + '\n';
        break;
      }
      if (obj.ids[i].approved === 1) {
        admin = true;
        reason = '';
        colorClass = colorTableRow(1);
        content = ' ' + name + ': ' + setStatus(1) + '\n';
      }
    }
  }

  if (obj.rel === "OR" && obj.dep.rel && id) {
    for (let i in obj.ids) {
      let j = 0;
      while (j < result.length) {
        if (obj.ids[i].id === result[j].id) {
          var name = result[j].name;
        }
        j++;
      }

      if (!obj.ids[i].approved && !supervisor) {
        reason = '';
        colorClass = colorTableRow(0);
        content += ' ' + name + ': ' + setStatus(0) + '\n';
      }
      if (obj.ids[i].approved === 2) {
        colorClass = colorTableRow(2);
        reason = obj.ids[i].reason;
        content += ' ' + name + ': ' + setStatus(2) + '\n';
        break;
      }
      if (obj.ids[i].approved === 1) {
        j++;
        reason = '';
        supervisor === true ? colorClass = colorTableRow(1) : colorClass = colorTableRow(0);
        content += ' ' + name + ': ' + setStatus(1) + '\n';
        break;
      }
    }
  }
  return {
    content: content,
    reason: reason,
  };
}

function getHolidays() {
  var colorClass, content, obj = [], info, infoEntries, startPage, endPage;
  userTable = $('#userTable').DataTable({
    "searching": true,
    "bFilter": true,
    "processing": true,
    "serverSide": true,
    "bServerSide": true,
    "responsive": true,
    "ajax": {
      "url": appConfig.url + appConfig.api + 'getFreeDaysApprover?',
      "data": function (data) {
        info = $('#userTable').DataTable().page.info();
        infoEntries = $('#userTable').DataTable().page.info().length;
        info.page += 1;
        startPage = info.start;
        endPage = info.end;
        data.token = token;
        data.userID = theUser.userID;
        data.infoPage = info.page;
        data.infoEntries = infoEntries;
        data.totalFreeDays = countFreeDays;
        data.start = startPage;
        data.end = endPage;
        data.draw = userTableRequestSequence;
        userTableRequestSequence++;
        if (info.page == 1) {
          indexTable = 0;
        }
        colorClass = colorTableRow(0);
      },
    },
    columns: [
      {data: "#"},
      {data: "days"},
      {data: "startDate"},
      {data: "endDate"},
      {data: "requestDate"},
      {data: "type"},
      {data: "comment"},
      {data: "approved"},
      {data: "delete"}
    ],
    retrieve: true,
    "columnDefs": [
      {orderable: true},
      {
        "render": function (data, type, row, full) {
          if (info.page === 1) {
            return full.row + 1;
          } else {
            return (info.page - 1) * 10 + full.row + 1;
          }

        },
        "targets": 0,
      },
      {
        "render": function (data, type, row) {
          return moment(row.startDate).format("DD/MM/Y");
        },
        "targets": 2
      },
      {
        "render": function (data, type, row) {
          return moment(row.endDate).format("DD/MM/Y");
        },
        "targets": 3
      },
      {
        "render": function (data, type, row) {
          let requestDate = moment(row.requestDate).format("DD/MM/Y");
          (requestDate === 'Invalid date') ? requestDate = '-' : requestDate;
          return requestDate;
        },
        "targets": 4
      },
      {
        "render": function (data, type, row) {
          let responseObject, commentRow;
          obj = jQuery.parseJSON(row.userObject);
          responseObject = holidayApprove(obj, row.id);
          content = responseObject.content;
          reason = responseObject.reason;
          comment = '';
          if (reason) {
            reason = reason.replace(/`/g, "%1%")
              .replace(/"/g, "%2%")
              .replace(/'/g, "%3%")
              .replace(/\\/g, '%4%');
          }
          commentRow = row.comment.replace(/`/g, "%1%")
            .replace(/"/g, "%2%")
            .replace(/'/g, "%3%")
            .replace(/\\/g, '%4%');
          comment = '<div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 200px;" ' +
            'onclick="displayDialogComment(\'' + row.name + '\',\'' +
            reason + '\',\'' + commentRow + '\')">' + row.comment + '</div>';
          return comment;
        },
        "targets": 6
      },
      {
        "render": function (data, type, row) {
          acc = "Pending";
          row.approved === 1 ? acc = "Approved" : (row.approved === 2 ? acc = "Not Approved" : "Pending");
          return acc;
        },
        "targets": 7
      },
      {
        "render": function (data, type, row) {
          let responseObject;
          obj = jQuery.parseJSON(row.userObject);
          responseObject = holidayApprove(obj, row.id);
          content = responseObject.content;
          reason = responseObject.reason;
          colorClass = colorTableRow(row.approved);
          return '<div' +
            ' onclick="displayDeleteModal(event, this, ' + row.id + ',' + row.approved + ')"><i class="fa fa-times"</i></div>'
        },
        "targets": 8
      },
    ],
    "createdRow": function (row) {
      // Set the data-status attribute, and add a class
      $(row).find('td')
        .addClass(colorClass);
      $(row).find('td:eq(0)')
        .css('width', '10%');
      $(row).find('td:eq(1)')
        .css('width', '10%');
      $(row).find('td:eq(2)')
        .css('width', '10%');
      $(row).find('td:eq(3)')
        .css('width', '10%');
      $(row).find('td:eq(4)')
        .css('width', '10%');
      $(row).find('td:eq(5)')
        .css('width', '10%');
      $(row).find('td:eq(6)')
        .css('width', '20%');
      $(row).find('td:eq(7)')
        .css('width', '10%');
      $(row).find('td:eq(8)')
        .css('width', '10%');
      $(row).find('td:eq(8)')
        .addClass(colorClass)
        .attr('role', 'button')
        .attr('data-content', 'popover')
        .attr('data-trigger', 'hover')
        .attr('data-placement', 'bottom')
        .attr('data-content', content)
        .popover('show');

      $('td[data-toggle="popover"]').hover(function (e) {
        e.preventDefault();
      });

    },
  })
}

function colorTableRow(approved) {
  return (approved === 1) ? "info" : (approved === 2 ? "danger" : "white");
}

function setStatus(value) {
  return value === 0 ? 'Pending' : (value === 1 ? 'Approved' : 'Not approved');
}

function displayDialogComment(name, approverComment, comment) {
  if (approverComment) {
    approverComment = approverComment.replace(/%1%/g, "`")
      .replace(/%2%/g, '"')
      .replace(/%3%/g, "'")
      .replace(/%4%/g, '\\');
  }
  comment = comment.replace(/%1%/g, "`")
    .replace(/%2%/g, '"')
    .replace(/%3%/g, "'")
    .replace(/%4%/g, '\\');

  $("#dialog-comment textarea:first-of-type").text(comment);
  if (approverComment == 'undefined' || approverComment == '') {
    $("#dialog-comment textarea:not(:first-of-type)").css('display', 'none');
    $("#dialog-comment .modal-body h4").css('display', 'none');
  } else {
    $("#dialog-comment textarea:not(:first-of-type)").text(approverComment);
    $("#dialog-comment textarea:not(:first-of-type)").css('display', 'block');
    $("#dialog-comment .modal-body h4").css('display', 'block');
  }
  $("#dialog-comment").modal('show');
}

function displayDeleteModal(event, elem, id, approved) {
  var deleteModal = $("#delete-modal");
  deleteModal.modal('show');


  $("#delete-modal-btn-yes").off().one('click', function () {
    event.stopPropagation();
    deleteHolidayModal(elem, id, approved);
    allUsers();
    indexTable = 0;
    userTable.ajax.reload(null, false);
    $("#delete-modal").modal('hide');
    $('.modal-backdrop').remove();
  });
  $("#delete-modal-btn-no").click(function () {
    $("#delete-modal").modal('hide');
    $('.modal-backdrop').remove();
  });
}

function deleteHolidayModal(elem, id, approved) {
  if (approved == 0) {
    socket.emit('/deleteHoliday', {
      token: token,
      id: id
    });
  } else {
    alert("You can not delete this. Please contact your supervisor.");
  }
}

function out(data) {
  if (data == 110) {
    if (!appConfig.sessionInvalid) {
      window.location.href = '' + linkLocationRef + '';
      appConfig.sessionInvalid = true;
      alert('Session expired');
      $.post(appConfig.url + appConfig.api + 'logout', {
        email: theUser.email
      })
    }
  }
}
