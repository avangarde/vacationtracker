"use strict";
window.sessionInvalid = false;
var socket = io(appConfig.io),
  linkLocationRef = appConfig.linkLocationRef;

if (!theUser) {
  window.location.href = appConfig.linkLocationRef;
  appConfig.sessionInvalid = true;
} else {
  $('#first-container').css('display', 'block');
  $('#second-container').css('display', 'block');
}

function weekend(d1, d2) {
  var we = 0,
    days = d2.diff(d1, "days") + 1;
  while (d1 <= d2) {
    if (d1.days() == 0 || d1.days() == 6) {
      we++;
    }
    d1.add(1, "days");
  }
  return days - we;
}

var holidaysNoCount = [{
  type: 'Donare sange',
  days: 1
},
  {
    type: 'Deces (gradul 1)',
    days: 3
  },
  {
    type: 'Deces (gradul 2)',
    days: 1
  },
  {
    type: 'Casatorie',
    days: 5
  },
  {
    type: 'Nou nascut',
    days: 10
  },
  {
    type: 'Maternitate/Paternitate',
    days: 730
  }
], datesDisabledRo = [], datesDisabledMd = [], refresh = true;

$(document).ready(function () {
  var theUser = JSON.parse(localStorage.getItem('user')),
    token = localStorage.getItem('token');

  updatelocalStorageUser(theUser.email, token);

  socket.on('/resSetApproved', function (data) {
    if (theUser.admin != 6 && theUser.admin != 2 && data['email'] == theUser.email) {
      updatelocalStorageUser(theUser.email, token);
    }
  });

  $('#tabClickCalendar').click(function () {
    if (window.location.href.indexOf("calendar") === -1) {
      window.history.pushState("", "Title", `${appConfig.linkLocal}calendar`);
      if (refresh) {
        window.location.reload();
      }
    }
  });

  $('#tabClickMyHolidays').click(function () {
    $(".popover").popover('hide');
    window.history.pushState("", "Title", `${appConfig.linkLocal}userTable`);
  });

  $('#managementTab').click(function () {
    $(".popover").popover('hide');
    window.history.pushState("", "Title", `${appConfig.linkLocal}managementTable`);
  });
  $('#managedUsers').click(function () {
    $(".popover").popover('hide');
    window.history.pushState("", "Title", `${appConfig.linkLocal}managedUsers`);
  });

  if (window.location.href.indexOf("userTable") !== -1) {
    $("#tabClickCalendar").removeClass('active');
    $("#tabClickMyHolidays").addClass('active in');
    $("#managementTab").removeClass('active');
    $("#managedUsers").removeClass('active');

    $("#calendartab").removeClass("active in");
    $("#table").addClass("active in");
    $("#management").removeClass("active in");
    $("#users-list").removeClass('active in');
    allUsers();
    getHolidays(1);
  } else if (window.location.href.indexOf("managementTable") !== -1) {
    $("#tabClickCalendar").removeClass('active');
    $("#tabClickMyHolidays").removeClass('active');
    $("#managementTab").addClass('active');
    $("#managedUsers").removeClass('active');

    $("#calendartab").removeClass("active in");
    $("#table").removeClass("active in");
    $("#management").addClass("active in");
    $("#users-list").removeClass('active in')
  } else if (window.location.href.indexOf("managedUsers") !== -1) {
    $("#tabClickCalendar").removeClass('active');
    $("#tabClickMyHolidays").removeClass('active');
    $("#managementTab").removeClass('active');
    $("#managedUsers").addClass('active');

    $("#calendartab").removeClass("active in");
    $("#table").removeClass("active in");
    $("#management").removeClass("active in");
    $("#users-list").addClass('active in')
  } else if (window.location.href.indexOf("calendar") !== -1) {
    refresh = false;
    $("#tabClickCalendar").addClass('active');
    $("#tabClickMyHolidays").removeClass('active');
    $("#managementTab").removeClass('active');
    $("#managedUsers").removeClass('active');

    $("#calendartab").addClass("active in");
    $("#table").removeClass("active in");
    $("#management").removeClass("active in");
    $("#users-list").removeClass('active in');
    fillDate()
  }

  socket.on('/resRefreshManagedUserTable', function (data) {
    if (data['userId'] == theUser.userID) {
      updatelocalStorageUser(theUser.email, token);
    }
  });

  displayForm();

  function freedays() {
    $.get(appConfig.url + appConfig.api + 'getLegalFreeDays?token=' + token, function (data) {
      out(data.code);
      for (var i in data) {
        datesDisabledRo.push(data[i].startDate.toString().substring(0, 10));
      }

      $('#datepicker').datepicker({
        multidate: 2,
        multidateSeparator: ";",
        toggleActive: true,
        clearBtn: true,
        minViewMode: 0,
        format: 'yyyy-mm-dd',
        datesDisabled: datesDisabledRo
      }).on('changeDate', function (e) {
        $('#eventForm').formValidation('revalidateField', 'dates');
      });
    });
  }


  function freedaysMD() {
    $.get(appConfig.url + appConfig.api + 'getLegalFreeDaysMD?token=' + token, function (data) {
      out(data.code);
      for (var i in data) {
        datesDisabledMd.push(data[i].startDate.toString().substring(0, 10));
      }
      $('#datepicker').datepicker({
        multidate: 2,
        multidateSeparator: ";",
        toggleActive: true,
        clearBtn: true,
        minViewMode: 0,
        format: 'yyyy-mm-dd',
        datesDisabled: datesDisabledMd
      }).on('changeDate', function (e) {
        // Revalidate the date field
        $('#eventForm').formValidation('revalidateField', 'dates');
      });
    });
  }

//  $('#tabClick').addClass('active');
  $('#tabClickCalendar :first-child').css("padding-bottom", "8px");
  $('#tabClickMyHolidays :first-child').css("padding-bottom", "8px");
  $('[name = "userst"]').css("padding-bottom", "12px").css("padding-top", "12px");
  var theUser = JSON.parse(localStorage.getItem('user')),
    token = localStorage.getItem('token'),
    manager, manId,
    isOk = true;

  $('#fileupload').fileupload({
    url: appConfig.url + appConfig.api + 'upload',
    formData: {
      id: theUser.userID,
      token: token
    },
    dataType: 'json',
    maxChunkSize: 1000000, // 10 MB
    disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 800,
    imageMaxHeight: 800,
    imageCrop: true, // Force cropped images
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $('<p/>').text(file.name).appendTo('#files');
      });

      theUser.picture = data.result;
      localStorage.setItem('user', JSON.stringify(theUser));
      $("#avatarimg").removeClass("fa fa-camera fa-2x");
      $("#avatar").css("display", "");
      $("#avatar").attr("src", 'data:image/png;base64,' + theUser.picture);
    },
    add: function (e, data) {
      var uploadErrors = [],
        acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
      if (data.originalFiles[0]['type'] && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
        uploadErrors.push('Not an accepted file type');
      }
      if (data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > 600000) {
        uploadErrors.push('Filesize is too big (bigger than 600 kb!)');
      }
      if (uploadErrors.length > 0) {
        alert(uploadErrors.join("\n"));
      } else {
        data.submit();
      }
    },
  });

  //file upload
  
  if (theUser != null) {
    if ((theUser.picture === "undefined") || (theUser.picture === "") || (!theUser.picture)) {
      $("#avatar").css("display", "none");
      $("#avatarimg").addClass("fa fa-camera fa-2x");
    } else {
      $("#avatar").attr("src", 'data:image/png;base64,' + theUser.picture);
    }
    $("[name=name]").val(theUser.name);
    $("[name=age]").val(theUser.age);
    $("[name=email]").val(theUser.email);
    $("[name=phone]").val(theUser.phone);
    $("[name=position]").val(theUser.position);
    var fDate = moment(theUser.startDate).format("MM/DD/YYYY");
    $("[name=sDate]").val(fDate);
    $("[name=timeSpent]").val(moment().diff(theUser.startDate, 'months', false) + " months");
    $("[name=avDays]").val(theUser.avfreedays);
    window.theUser = theUser;
    localStorage.setItem('user', JSON.stringify(theUser));
    $.get(appConfig.url + appConfig.api + 'updateAvFreeDays?token=' + token + '&userEmail=' + theUser.email +
      '&avfreedays=' + $("[name=avDays]").val(), function (data) {
      out(data.code);
    });
    // });
  } else {
    alert("No user loged in!");
    window.location.href = "index.html";
  }

  if (localStorage.getItem('admin') != null) {
    $('#navbar1 ul:first-of-type > li:nth-child(2)').css('display', 'block');
  }

  $('#manager-table').on('click', 'td:nth-child(4)', function () {
    var data = $(this).text(),
      name = $(this).parent().find("td:nth-child(2)").text();
    $("#dialog-email .modal-title").text(name + "'s position info: ");
    $("#dialog-email textarea").text(data);
    $("#dialog-email").modal('show');
  });

  $('#users-list-table').on('click', 'td:nth-child(6)', function () {
    var data = $(this).text(),
      name = $(this).parent().find("td:nth-child(2)").text();
    $("#dialog-email .modal-title").text(name + "'s email address: ");
    $("#dialog-email textarea").text(data);
    $("#dialog-email").modal('show');
  });

  $('#users-list-table').on('click', 'td:nth-child(5)', function () {
    var data = $(this).text(),
      name = $(this).parent().find("td:nth-child(2)").text();
    $("#dialog-email .modal-title").text(name + "'s position info: ");
    $("#dialog-email textarea").text(data);
    $("#dialog-email").modal('show');
  });

  $('#users-list-table').on('click', 'td:nth-child(9)', function () {
    var data = $(this).text(),
      name = $(this).parent().find("td:nth-child(2)").text();
    $("#dialog-email .modal-title").text(name + "'s supervisor: ");
    $("#dialog-email textarea").text(data);
    $("#dialog-email").modal('show');
  });

  $('#manager-table').on('click', 'td:nth-child(5)', function () {
    var data = $(this).text(),
      name = $(this).parent().find("td:nth-child(2)").text();
    $("#dialog-email .modal-title").text(name + "'s email address: ");
    $("#dialog-email textarea").text(data);
    $("#dialog-email    ").modal('show');
  });

  if (theUser.admin === 2) {
    $("#tabs li:not(:last)").css('display', 'none');
  }

  if ((theUser.admin !== 3) && (theUser.admin !== 6) && (theUser.admin !== 2) && (theUser.admin !== 4)) {
    $("a[name='addUser']").css("display", "none");
    $("#row").css("display", "none");
  }

  if ((theUser.admin === 2) || (theUser.admin === 6)) {
    $("[name=userst]").parent().addClass('active');
    $("[name=userst]").attr('aria-expanded', true);
    $("#users-list").addClass('active in');
    $("[name=add]").parent().css('display', 'none');
    $("[name=addUser]").parent().css('display', 'block');
    $("#tabs li:first-child").css('display', 'none');
    $("#tabs li:nth-child(2)").css('display', 'none');
    $("#calendar").css('display', 'none');
    $("#you").css('display', 'none');
    $("#user-days-disp").css('display', 'none');
    $("#fd").css('display', 'none');
    $("#hi").css('left', '737px');
    $("#user-name-disp").attr('style', 'left: 750px !important');
    $("#user-name-disp").css('width', '51%');
    $("[name=avDays]").val(0);
    let f = navigator.userAgent.search("Firefox");
    if (f > -1) {
      $("#user-name-disp").attr("style", "left: 745px !important");
      $("#user-name-disp").css("bottom", " 40px");
    }
  } else if ((theUser.admin !== 2) || (theUser.admin !== 6)) {
    let f = navigator.userAgent.search("Firefox");
    if (f > -1) {
      $("#user-name-disp").attr("style", "left: 490px !important")
        .css("bottom", "40px")
        .attr("type", " ")
        .css("left", "312");
    }
    $("#forAdmin").css("display", 'none');
    $("#holiday").css('display', 'block');
    $("[name=addUser]").parent().css('display', 'block');
    $("#newyear").css("display", 'none');
  }

  if (theUser.admin === 3) {
    $("#newyear").css("display", 'block');
  }

  if (theUser.admin === 0) {
    $("[name=addUser]").css('display', 'none');
    $("#selectUsersDropdown").css('display', 'none');
    $("#selectAPDropdown").css('display', 'none')
    $("#managedUsers").css('display', 'none')
  }

  if (theUser.admin === 6) {
    $("[name=userst]").parent().removeClass('active');

    $("#tabs li:first-child").css('display', 'block');
    $("#calendar").css('display', 'block');
    // $("#tabs li:first-child").addClass('active');
    if (window.location.href.indexOf('managedUsers') === -1) {
      $("#users-list").removeClass('active in');
    }
  }

  var date = new Date();
  date.setDate(date.getDate());


  $('#myModal').on('hidden.bs.modal', function () {
    $('#eventForm').bootstrapValidator('resetForm', true);
    $(this).find("input,textarea,select").val('').end();
    $('.modal-body> div:first-child').css('display', 'none');
    $('.modal-body> div:nth-child(2)').css('display', 'none');
    $('.modal-body> div:nth-child(3)').css('display', 'none');
  });

  var nrOfDays, from, to,
    formValid = false;

  function addholidayForm() {
    let legalH, legalM;
    if (theUser.country === "ro") {
      legalH = datesDisabledRo;
      freedays();
    } else {
      legalM = datesDisabledMd;
      freedaysMD();
    }
    $('#eventForm')
      .formValidation({
        framework: 'bootstrap',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          vacationtype: {
            validators: {
              notEmpty: {
                message: 'The type is required'
              }
            }
          },
          comment: {
            validators: {
              notEmpty: {
                message: 'The comment is required'
              },
              regexp: {
                regexp: '^(([A-z ,.]|[0-9]){1,100})$',
                message: 'Comment field can\'t contain symbols'
              }
            },
          },
          dates: {
            validators: {
              notEmpty: {
                message: 'The date is required'
              }
            }
          }
        }
      }).on('change', function (e, data) {
      $("#eventForm").formValidation('revalidateField', 'vacationtype');
      $("#eventForm").formValidation('revalidateField', 'comment');
      $("#datepicker").datepicker("refresh");
      if ($(".date").val()) {
        $("#datepicker").datepicker("refresh");
        $("div #info").empty();
        $("div #danger").empty();
        var date = $(".date").val().split(";"),
          stdate = date[0],
          enddate = date[1],
          type = $('#vacationtype').val();
        from = moment(stdate).format('YYYY-MM-DD');
        if (!enddate) {
          to = moment(stdate).format('YYYY-MM-DD');
        } else {
          to = moment(enddate).format('YYYY-MM-DD');
        }
        var aux;

        if (moment(from).isAfter(moment(to)) || moment(to).isBefore(moment(from))) {
          aux = to;
          to = from;
          from = aux;
        }
        var isWeekend = weekend(moment(from), moment(to)),
          arrayOfSelectedDays = arrayFromStToEnd(from, to),
          dateArr = [],
          daysWithWk = 0,
          addDays = 0;
        if (theUser.country === "ro") {
          nrOfDays = checkArrays(arrayOfSelectedDays, legalH, isWeekend);
        } else {
          nrOfDays = checkArrays(arrayOfSelectedDays, legalM, isWeekend);
        }

        if (type) {
          if (type === "Concediu") {
            $("#danger").css('display', 'none');
          }
          if (type === 'Maternitate/Paternitate') {
            to = moment(from, "YYYY/MM/DD").add(730, 'days');
            nrOfDays = 730;
            $("#datepicker").val(stdate + ";" + moment(to).format('YYYY-MM-DD'));
          }
          if (type === 'Nou nascut') {
            to = moment(from, "YYYY/MM/DD").add(10, 'days');
            dateArr = arrayFromStToEnd(from, moment(to).format("YYYY/MM/DD"));
            isWeekend = weekend(moment(from), moment(moment(to).format("YYYY/MM/DD")));
            if (theUser === "ro") {
              daysWithWk = checkArrays(dateArr, legalH, isWeekend);
            } else {
              daysWithWk = checkArrays(dateArr, legalM, isWeekend);
            }

            if (daysWithWk < 10) {
              addDays = 10 + (10 - daysWithWk);
            }
            to = moment(from, "YYYY/MM/DD").add(addDays, 'days');

            nrOfDays = 10;
            $("#datepicker").val(stdate + ";" + moment(to).format('YYYY-MM-DD'));
          }
        }

        for (let j in holidaysNoCount) {
          if (type === holidaysNoCount[j].type) {
            if (nrOfDays !== holidaysNoCount[j].days) {
              $("div #danger").append("<p>The duration for this type of holiday is " +
                holidaysNoCount[j].days + " days.</p>").css('display', 'block');
              $("[name=comment]").attr('disabled', true);
              $("#save").attr('disabled', true);
              formValid = false;
              break;
            } else {
              $("#danger").css('display', 'none');
              $("[name=comment]").attr('disabled', false);
              $("#save").attr('disabled', false);
              formValid = true;
              break;
            }
          } else {
            $("[name=comment]").attr('disabled', false);
            $("#save").attr('disabled', false);
            formValid = true;
          }
        }
        if (type) {
          $("div #info").css('display', 'block');
          if (type == 'Concediu') {
            $("div #info").append("<p>You selected " + nrOfDays + " days. " +
              "These will be taken from the total number of available leave days.</p>")
          } else {
            $("div #info").append("<p>You selected " + nrOfDays + " days. " +
              "These will not be taken from the total number of available leave days.</p>")
          }
        }
      } else {
        $("div #info").css('display', 'none');
      }

    }).on('success.form.fv', function (e, data) {
      e.preventDefault();
      var holidayOptions = {
        managerName: manager,
        manag: manId,
        vacationtype: $("#vacationtype").val(),
        comment: $("#comment").val(),
        avDays: theUser.avfreedays,
        stdate: from,
        enddate: to,
        duration: nrOfDays
      };
// expected output: true

      if (nrOfDays == 0) {
        $('.modal-body> div:first-child').css('display', 'none');
        $('.modal-body> div:nth-child(2)').css('display', 'none');
        $('.modal-body> div:nth-child(3)').css('display', 'block');
        resetFormAddH();
      } else {
        check(from, to, holidayOptions, addHoliday);
        $("div #info").css('display', 'none');
        $("div #danger").css('display', 'none');
      }

      if ($("#eventForm div").hasClass('form-horizontal')) {
        $("#myModal").modal("hide");
        $('.modal-backdrop').remove();
      }
      resetFormAddH();
    }).on('submit', function (e, data) {
      $("#eventForm").formValidation('revalidateField', 'vacationtype');
      $("#eventForm").formValidation('revalidateField', 'comment');
    });

    $('#logout').click(function (e) {
      $.post(appConfig.url + appConfig.api + 'logout', {
        email: theUser.email
      }).done(function (data) {
        localStorage.removeItem('token');
        localStorage.removeItem('admin');
        localStorage.removeItem('user');
        window.location.href = "index.html";
      });
    });

    function addHoliday(options) {
      var stdate = moment(options.stdate).format("DD-MM-YYYY"),
        enddate = moment(options.enddate).format("DD-MM-YYYY"),
        days = options.duration,
        dataUser = jQuery.parseJSON(theUser.object),
        searchSupervisors = [], emailList = "", result;

      let currentDate = (moment(new Date())).format('YYYY-MM-DD'),
        currentMonth = moment(currentDate).month() + 1,
        holidayMonth = moment(options.stdate).month() + 1;

      obj = dataUser;
      for (let j = 0; j < obj.ids.length; j++) {
        searchSupervisors.push(obj.ids[j]['id']);
      }
      if (obj.dep.ids) {
        for (let j = 0; j < obj.dep.ids.length; j++) {
          searchSupervisors.push(obj.dep.ids[j]['id']);
        }
      }

      result = getSupervisorsName();
      for (let i in result) {
        for (let j in searchSupervisors) {
          if (result[i].id === searchSupervisors[j]) {
            emailList += result[i].email + " ,";
          }
        }
      }

      socket.emit('/updatedate', {
        token: token,
        vacationtype: options.vacationtype,
        comment: options.comment,
        stdate: moment(options.stdate).format("YYYY/MM/DD"),
        enddate: moment(options.enddate).format("YYYY/MM/DD"),
        userID: theUser.userID,
        days: options.duration,
        emailList: emailList,
        name: theUser.name,
        userObject: JSON.stringify(dataUser)
      });

      $("#myModal").modal("hide");
      $('.modal-backdrop').remove();
      socket.on('/resupdatedate', function (data) {
        if (data.userId == theUser.userID) {
          $("#save").attr('disabled', 'disabled');
          $("#myModal").modal("hide");
          $('.modal-backdrop').remove();
        }
      });

      $('.modal-body> div:first-child').css('display', 'block');
      $('.modal-body> div:nth-child(2)').css('display', 'none');
      $('.modal-body> div:nth-child(3)').css('display', 'none');


      $("div #info").css('display', 'none');
      $("div #danger").css('display', 'none');
    }

    function check(startString, endString, options, callback) {
      var start = moment(startString).format("YYYY/MM/DD"),
        end = moment(endString).format("YYYY/MM/DD");
      $.get(appConfig.url + appConfig.api + 'getFreeDaysApprovedOrPending?token=' + token + '&userID=' +
        theUser.userID, function (data) {
        out(data.code);
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            var st = moment(data[i].startDate).format("YYYY/MM/DD"),
              ends = moment(data[i].endDate).format("YYYY/MM/DD"),
              start2 = start,
              end2 = end,
              dateArray = new Array();

            if (moment(st).isBetween(start, end) || moment(ends).isBetween(start, end) ||
              moment(start).isBetween(st, ends) || moment(end).isBetween(st, ends) ||
              start == st || start == ends || end == st || end == ends || start > end) {
              $('.modal-body> div:first-child').css('display', 'none');
              $('.modal-body> div:nth-child(2)').css('display', 'none');
              $('.modal-body> div:nth-child(3)').css('display', 'block');
              $("div #info").css('display', 'none');
              $("div #danger").css('display', 'none');
              isOk = false;
              resetFormAddH();
              break;
            } else {
              isOk = true;
            }

            dateArray = arrayFromStToEnd(start2, end2);

            for (var j = 0; j < dateArray.length; j++) {
              if (dateArray[j] == st || dateArray[j] == ends) {
                $('.modal-body> div:first-child').css('display', 'none');
                $('.modal-body> div:nth-child(2)').css('display', 'none');
                $('.modal-body> div:nth-child(3)').css('display', 'block');
                resetFormAddH();
                break;
              } else {
                isOk = true;
              }
            }
          }
          if (isOk) {
            callback(options);

            $("div #info").css('display', 'none');
            $("div #danger").css('display', 'none');
          }
        } else {
          $("div #info").css('display', 'none');
          $("div #danger").css('display', 'none');
          callback(options);
        }
      });
    }


    function arrayFromStToEnd(st, end) {
      var datesArr = new Array();
      while (st <= end) {
        datesArr.push(st);
        var duration = moment.duration({
          'days': 1
        });
        st = moment(st, 'YYYY-MM-DD').add(duration).format('YYYY-MM-DD');
      }
      return datesArr;
    }

    function checkArrays(arr1, arr2, options) {
      // Remove duplicates from public holidays array.
      var unique_arr2 = [];
      $.each(arr2, function (i, el) {
        if ($.inArray(el, unique_arr2) === -1) {
          unique_arr2.push(el);
        }
      });
      for (let l = 0; l < arr1.length; l++) {
        for (let d = 0; d < unique_arr2.length; d++) {
          let x = new Date(unique_arr2[d]);
          if (x.getDay() != 0 && x.getDay != 6) {
            if (arr1[l] == unique_arr2[d]) {
              options--;
            }
          }
        }
      }
      return options;
    }

    function out(data) {
      if (data == 110) {
        if (!appConfig.sessionInvalid) {
          window.location.href = '' + linkLocationRef + '';
          appConfig.sessionInvalid = true;
          alert('Session expired');
          $.post(appConfig.url + appConfig.api + 'logout', {
            email: theUser.email
          });
        }
      }
    }
  }

  function displayForm() {
    $("#myModal").load("addholiday.html", function () {
      addholidayForm();
    });
  }

  function resetFormAddH() {
    $("#eventForm")[0].reset();
    $("#eventForm").formValidation('revalidateField', 'vacationtype');
    $("#eventForm").formValidation('revalidateField', 'comment');
    $(".input-daterange").next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
    $("#eventForm div").each(function () {
      $(this).removeClass('has-success').addClass('has-error');
    });
  }

  function updatelocalStorageUser(email, token) {
    $.ajax({
      type: 'GET',
        url: appConfig.url + appConfig.api + "checkUser",
        data: {
        email: email,
          token: token
      },
      dataType: 'json',
        'success': function (data) {
        var theUser = new appNameSpace.User();
        theUser['userID'] = data.userID;
        theUser['isActive'] = data.isActive;
        theUser['picture'] = data.picture;
        theUser['age'] = moment().diff(data.age, 'years', false);
        theUser['name'] = data.name;
        theUser['position'] = data.position;
        theUser['phone'] = data.phone;
        theUser['startDate'] = data.startDate;
        theUser['admin'] = data.admin;
        theUser['avfreedays'] = data.avfreedays;
        theUser['bonus'] = data.bonus;
        theUser['email'] = data.email;
        theUser['object'] = data.userObject;

        if (data.admin > 0) {
          localStorage.setItem('admin', data.admin);
        }
        if (data.isActive == 0) {
          alert("You don't work here");
          appConfig.sessionInvalid = true;
          $.post(appConfig.url + appConfig.api + 'logout', {
            email: theUser.email
          }).done(function (data) {
          });
          window.location.href = '' + linkLocationRef + '';
        }
        localStorage.setItem('token', data.token);
        localStorage.setItem('user', JSON.stringify(theUser));
        theUser =  JSON.parse(localStorage.getItem('user'));
        // console.log(theUser);
        $("#username").val(theUser.name);
        $("#phoneUser").val(theUser.phone);
        $("#user-name-disp").val(theUser.name);
        $("#user-days-disp").val(theUser.avfreedays);
        return false;
      }
    });
}

  $("#dropdownMenu2").click(function () {
    $("div #info").css('display', 'none');
    $("div #danger").css('display', 'none');
    displayForm();
  });
});
