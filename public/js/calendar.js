var d = [],
  dd = [],
  legalDatesCalendar = [],
  legalDatesCalendarMD = [],
  allEvents = [],
  theUser = JSON.parse(localStorage.getItem('user')),
  user = localStorage.getItem('user'),
  manager, change = true,
  token = localStorage.getItem('token'),
  socket = io(appConfig.io),
  action = 'create',
  loaded = 0, countryOption, loadCountry = "All", arrayOptions, eventInfo;

$(document).ready(function () {
  countryOption = localStorage.getItem('countryOption');
  setTimeout(function () {
    $('[name="selectUser"].selectpicker').selectpicker('selectAll', 'refresh');
    $('[name="selectAP"].selectpicker').selectpicker('selectAll', 'refresh');
    loaded = 1;
  }, 1000);
  $('[name="selectUser"].selectpicker').on('change', function (e) {
    if (loaded) {
      getFilteredFreedays()
    }
  });

  $('[name="selectCountry"].selectpicker').on('change', function () {
    if (loaded) {
      var options = $('[name="selectCountry"].selectpicker option:selected');
      if (options.length == 2) {
        arrayOptions = '';
        arrayOptions = options[0].innerHTML + " " + options[1].innerHTML;
      } else if (options.length == 1) {
        arrayOptions = '';
        arrayOptions = options[0].innerHTML;
      } else if (options.length == 0) {
        arrayOptions = '';
      }
      localStorage.setItem('countryOption', arrayOptions);
      getLegalHolidays();
    }
  });

  $('[name="selectAP"].selectpicker').on('change', function () {
    if (loaded) {
      getFilteredFreedays()
    }
  });
});

socket.on('/resRefreshCalendar', function (data) {
  getLegalHolidays();
  change = false;
  allEvents = $("#calendar").fullCalendar('clientEvents');

  for (let i in allEvents) {
    if (typeof allEvents[i].id !== 'string') {
      $("#calendar").fullCalendar('removeEvents', allEvents[i]._id);
    }
  }
  getFilteredFreedays();
});

socket.on('/resdeleteHoliday', function (data) {
  let view = $("#calendar").fullCalendar('getView'), date, refresh = false;
  if (view.intervalStart) {
    date = view.intervalStart.format('YYYY/MM/DD');
  } else {
    date = moment(new Date()).format('YYYY/MM/DD');
  }
  if (view.type == 'month' && moment(date).format('M') == moment(data.stdate).format('M')) {
    refresh = true;
  } else if (view.type == 'basicWeek') {
    var current = new Date(date),
      weekstart = current.getDate() - current.getDay() + 1,
      weekend = weekstart + 6,
      monday = moment(new Date(current.setDate(weekstart))).format('YYYY/MM/DD'),
      sunday = moment(new Date(current.setDate(weekend))).format('YYYY/MM/DD');

    if (moment(data.stdate).isSameOrAfter(moment(monday)) && moment(data.stdate).isSameOrBefore(moment(sunday))) {
      refresh = true;
    }
  } else {
    if (data.stdate == date) {
      refresh = true;
    }
  }
  if (refresh) {
    $('[name="selectUser"].selectpicker').selectpicker('refresh');
    change = false;
    getFilteredFreedays();
  }
});

socket.on('/resupdatedate', function (data) {
  let view = $("#calendar").fullCalendar('getView'), date, refresh = false;
  if (view.intervalStart) {
    date = view.intervalStart.format('YYYY/MM/DD');
  } else {
    date = moment(new Date()).format('YYYY/MM/DD');
  }
  if (view.type == 'month' && moment(date).format('M') == moment(data.stdate).format('M')) {
    refresh = true;
  } else if (view.type == 'basicWeek') {
    var current = new Date(date),
      weekstart = current.getDate() - current.getDay() + 1,
      weekend = weekstart + 6,
      monday = moment(new Date(current.setDate(weekstart))).format('YYYY/MM/DD'),
      sunday = moment(new Date(current.setDate(weekend))).format('YYYY/MM/DD');

    if (moment(data.stdate).isSameOrAfter(moment(monday)) && moment(data.stdate).isSameOrBefore(moment(sunday))) {
      refresh = true;
    }
  } else {
    if (data.stdate == date) {
      refresh = true;
    }
  }
  if (refresh) {
    $('[name="selectUser"].selectpicker').selectpicker('refresh');
    change = false;
    let events = [], endTime;
    endTime = new Date(data.enddate);
    endTime.setMinutes(1);

    if (data.userId === theUser.userID) {
      events.push({
        start: new Date(data.stdate),
        end: endTime,
        title: 'pending: ' + data.type,
        id: theUser.userID,
        className: 'fc-myevent-pending'
      });
      $('#calendar').fullCalendar('renderEvents', events, true);

    } else {
      getFilteredFreedays();
    }
  }
});

socket.on('/resSetApproved', function (data) {
  $('[name="selectUser"].selectpicker').selectpicker('refresh');
  getFilteredFreedays();
});

function fillDate() {
  let currentDate = (moment(new Date())).format('YYYY-MM-DD'), vacations = [], idss = [theUser.userID],
    type = 'month', url = new URL(window.location.href), hid = url.searchParams.get("hid"), year = moment().year(),
    month = url.searchParams.get('month'), filteredUrlCreate;

  $.ajax({
    type: 'GET',
    url: appConfig.url + appConfig.api + 'getCalendarUsersName?token=' + token + '&userID=' + theUser.userID,
    async: false,
  }).done(function (data) {
    for (let i = 0; i < data.length; i++) {
      idss.push(data[i].userID);
      $('[name="selectUser"].selectpicker').append($("<option></option>")
        .attr("value", data[i].userID)
        .text(data[i].name));
      $("[name='selectUser'].selectpicker").selectpicker('refresh');
    }
  }).then(function () {
    if (month > 0) {
      currentDate = moment().set({'year': year, 'month': month - 1, 'date': 10}).format("YYYY-MM-DD");
      filteredUrlCreate = appConfig.url + appConfig.api + 'getFilteredFreedays?token=' + token + "&userID=" + theUser.userID +
        "&ids=" + idss + "&approved=0,1&viewType=" + type + "&viewDate=" + currentDate + "&month=" + month;
    } else {
      currentDate = moment(new Date()).format('YYYY-MM-DD');
      filteredUrlCreate = appConfig.url + appConfig.api + 'getFilteredFreedays?token=' + token + "&userID=" + theUser.userID +
        "&ids=" + idss + "&approved=0,1&viewType=" + type + "&viewDate=" + currentDate;
    }
    $.ajax({
      type: 'GET',
      url: filteredUrlCreate,
      async: false,
    }).done(function (data) {
        eventInfo = [];
        let color = 'black', supervisorApprover = false,
          userObject;

        for (let i = 0; i < data.length; i++) {
          userObject = JSON.parse(data[i].userObject);
          if (theUser.admin === 6) {
            if (JSON.parse(data[i].userObject).dep.ids) {
              color = 'black';
            } else {
              color = 'green';
            }
          }
          let avFdApprove, avFdReject;

          if (data[i].type == "Concediu" && theUser.admin !== 1) {
            avFdApprove = data[i].avfreedays - data[i].days
          } else {
            avFdApprove = data[i].avfreedays;
          }

          if (data[i].approved === 1 && data[i].type === 'Concediu') {
            avFdReject = data[i].avfreedays + data[i].days;
          } else {
            avFdReject = data[i].avfreedays;
          }
          eventInfo[data[i].id] = {
            id: data[i].id,
            email: data[i].email,
            obj: $.parseJSON(data[i].userObject),
            type: data[i].type,
            avFdApprove: avFdApprove,
            avFdReject: avFdReject
          };
          let endTime;
          endTime = new Date(data[i].endDate);
          endTime.setMinutes(1);

          for (let i in userObject.ids) {
            if (theUser.admin === 1 && userObject.ids[i].approved) {
              supervisorApprover = true;
              break;
            }
            if ((theUser.admin === 3 || theUser.admin === 4 || theUser.admin === 6) && userObject.ids[i].approved) {
              supervisorApprover = true;
              break;
            }
            if (userObject.ids[i].approved && theUser.userID === userObject.ids[i].id) {
              supervisorApprover = true;
              break;
            } else {
              supervisorApprover = false;
            }
          }

          if (jQuery.isEmptyObject(userObject.dep)) {
            supervisorApprover = true;
          }

          if (data[i].approved === 0) {
            if (data[i].userID === theUser.userID) {
              vacations.push({
                start: new Date(data[i].startDate),
                end: endTime,
                title: 'pending: ' + data[i].type,
                id: theUser.userID,
                className: 'fc-myevent-pending'
              });
            } else {
              if (data[i].id == hid) {
                vacations.push({
                  start: new Date(data[i].startDate),
                  end: endTime,
                  title: 'pending: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                  id: data[i].id,
                  className: 'fc-newevent-pending',
                  eventTextColor: color
                });
              } else {
                if (supervisorApprover) {
                  vacations.push({
                    start: new Date(data[i].startDate),
                    end: endTime,
                    title: 'pending: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                    id: data[i].id,
                    className: 'fc-event-supervisor-pending',
                    eventTextColor: color
                  });
                } else {
                  vacations.push({
                    start: new Date(data[i].startDate),
                    end: endTime,
                    title: 'pending: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                    id: data[i].id,
                    className: 'fc-event-pending',
                    eventTextColor: color
                  });
                }
              }
            }
          }
          else if (data[i].approved === 1) {
            if (data[i].userID === theUser.userID) {
              vacations.push({
                start: new Date(data[i].startDate),
                end: endTime,
                title: 'approved: ' + data[i].type,
                id: theUser.userID,
                className: 'fc-myevent-approved',
              });
            } else {
              vacations.push({
                start: new Date(data[i].startDate),
                end: endTime,
                title: 'approved: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                id: data[i].id,
                className: 'fc-event-approved',
                eventTextColor: color
              });
            }
          }
        }
      }
    );

    let cArray = [];
    if (countryOption == "Romania Moldova") {
      $('[name="selectCountry"].selectpicker').selectpicker('selectAll');
      $('[name="selectCountry"].selectpicker').selectpicker('refresh');
      loadCountry = 'All';
      cArray = [0, 1];
    } else if (countryOption == "Romania") {
      $('[name="selectCountry"]').val(0);
      $('[name="selectCountry"].selectpicker').selectpicker('refresh');
      loadCountry = "Ro";
      cArray = [0];
    } else if (countryOption == "Moldova") {
      $('[name="selectCountry"]').val(1);
      $('[name="selectCountry"].selectpicker').selectpicker('refresh');
      loadCountry = "Md";
      cArray = [1];
    } else if (countryOption == "" || !countryOption) {
      // loadCountry = "All";
      if (theUser.country == "ro") {
        loadCountry = "Ro";
        cArray = [0];
        $('[name="selectCountry"]').val(0);
      } else {
        loadCountry = "Md";
        $('[name="selectCountry"]').val(1);
        cArray = [1];
      }
      $('[name="selectCountry"].selectpicker').selectpicker('refresh');
    }

    $.get(appConfig.url + appConfig.api + 'retrieveFilteredHolidays?token=' + token +
      "&viewType=month&viewDate=" + currentDate + '&countries=' + cArray, function (data) {
      // $('#calendar').fullCalendar('removeEvents');
      for (let i in data) {
        if (data[i].type === "public") {
          if (theUser.country === 'ro') {
            if (data[i].country === 'md') {
              if (data[i].type === "public") {
                legalDatesCalendarMD.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                vacations.push({
                  start: new Date(data[i].startDate),
                  title: '+ ' + data[i].name + ' MD',
                  id: data[i].id + 's',
                  className: 'fc-md-holiday'
                });
              }
            } else {
              legalDatesCalendar.push(moment(data[i].startDate).format("YYYY/MM/DD"));
              vacations.push({
                start: new Date(data[i].startDate),
                title: '+ ' + data[i].name,
                id: data[i].id + 's',
                className: 'fc-ro-holiday'
              });
            }
          } else {
            if (data[i].country === 'ro') {
              if (data[i].type === "public") {
                legalDatesCalendar.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                vacations.push({
                  start: new Date(data[i].startDate),
                  title: '+ ' + data[i].name + ' RO',
                  id: data[i].id + 's',
                  className: 'fc-ro-holiday'
                });
              }
            } else {
              legalDatesCalendarMD.push(moment(data[i].startDate).format("YYYY/MM/DD"));
              vacations.push({
                start: new Date(data[i].startDate),
                title: '+ ' + data[i].name,
                id: data[i].id + 's',
                className: 'fc-md-holiday'
              });
            }
          }
        }
      }
      createCalendar(vacations, month);
    });
  })
}


function createCalendar(d, month) {
  var defDate;
  if (month > 0) {
    let year = moment().year();
    defDate = moment().set({'year': year, 'month': month - 1, 'date': 10}).format("YYYY-MM-DD");
  } else {
    defDate = moment(new Date()).format("YYYY-MM-DD");
  }

  $('#external-events div.external-event').each(function () {
    let eventObject = {
      title: $.trim($(this).text()) // use the element's text as the event title
    };

    $(this).data('eventObject', eventObject);
    $(this).draggable({
      zIndex: 999,
      revert: true,
      revertDuration: 0
    });
  });

  $(function () {
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultView: 'month',
        defaultDate: defDate,
        firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
        eventTextColor: '#000',
        eventLimit: true, // allow "more" link when too many events
        // eventLimitClick: 'week',
        events: d,
        eventRender: function (event, element) {
          element.prop("title", event.title);
          element.css('color', event.eventTextColor)
        },
        dayClick: function (date) {
          $("body > .popover").popover('hide');
          if (theUser.admin !== 6 && (moment(date).day() !== 0 && moment(date).day() !== 6)) {
            $("#datepicker").datepicker("refresh");
            $("#eventForm")[0].reset();
            $("#eventForm div").removeClass('has-error').removeClass('has-success');
            $("#eventForm i").remove();
            $("#myModal").modal('show');
            let ok = false;
            if (theUser.country === "ro") {
              for (let i in legalDatesCalendar) {
                if (moment(date).format("YYYY-MM-DD") === moment(legalDatesCalendar[i]).format("YYYY-MM-DD")) {
                  ok = true;
                }
              }
            } else if (theUser.country === "md") {
              for (let i in legalDatesCalendarMD) {
                if (moment(date).format("YYYY-MM-DD") === moment(legalDatesCalendarMD[i]).format("YYYY-MM-DD")) {
                  ok = true;
                }
              }
            }

            let a = date.format("YYYY-MM-DD"), queryDate = "'" + a + "'",
              dateParts = queryDate.match(/(\d+)/g),
              realDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
            // months are 0-based!

            if (ok === false) {
              // $("#datepicker").datepicker("setDate", realDate);
              $("#datepicker").datepicker({
                dateFormat: 'YYYY-MM-DD'
              }).datepicker("setDate", realDate)
            } else if (ok === true) {
              $("#datepicker").datepicker("setDate");
            }
            $(".alert").css('display', 'none');
          }
        },
        eventClick: function (eventObj, $el) {
          $("body > .popover").popover('hide');
          let content, contentEnd;
          $this = $(this);
          if (!eventObj.end) {
            content = moment(eventObj.start).format("DD/MM/YYYY");
            if (eventObj.title.indexOf('name') !== -1) {
              if (eventObj.className[0].indexOf('event-pending') !== -1 && theUser.admin !== 2 && theUser.admin !== 0) {
                content += "<span class='fa fa-check'" +
                  " onclick='approve(" + eventObj.id + ", 1)'></span><span class='fa fa-times' " +
                  "onclick='approve(" + eventObj.id + ", 2)'></span>";
              } else if (theUser.admin == 3 || theUser.admin == 6) {
                content += "<span class='fa fa-times' onclick='approve(" + eventObj.id + ", 2)'></span>"
              }
            }
            $this.popover({
              trigger: 'manual',
              html: true,
              title: '<button type="button" id="close" class="close" ">&times;</button>',
              content: content,
              placement: 'top',
              container: 'body'
            }).popover('show');
          } else {
            contentEnd = moment(eventObj.start).format("DD/MM/YYYY") + ' - ' + moment(eventObj.end).format("DD/MM/YYYY");
            if (eventObj.title.indexOf('name') !== -1) {
              if (eventObj.className[0].indexOf('event-pending') !== -1 || eventObj.className[0].indexOf('fc-event-supervisor-pending') !== -1) {
                if (theUser.admin === 6 || (eventObj.className[0].indexOf('fc-event-supervisor-pending') === -1 && theUser.admin === 1)) {
                  contentEnd += "<span id = 'calendarButtons' class='fa fa-check' " +
                    "onclick='approve(" + eventObj.id + ", 1)'></span><span class='fa fa-times'" +
                    " onclick='approve(" + eventObj.id + ", 2)'></span>";
                }
                if (eventObj.className[0].indexOf('event-pending') === -1 &&
                  eventObj.className[0].indexOf('fc-event-supervisor-pending') !== -1 && theUser.admin > 1 && theUser.admin !== 6) {
                  contentEnd += "<span id = 'calendarButtons' class='fa fa-check' " +
                    "onclick='approve(" + eventObj.id + ", 1)'></span><span class='fa fa-times'" +
                    " onclick='approve(" + eventObj.id + ", 2)'></span>";
                }
              }
              if (eventObj.className[0].indexOf('fc-event-supervisor-pending') === -1 &&
                eventObj.className[0].indexOf('fc-event-pending') === -1 && eventObj.className[0].indexOf('event-pending') === -1 && (theUser.admin === 3 || theUser.admin === 6)) {
                contentEnd += "<span class='fa fa-times' onclick='approve(" + eventObj.id + ", 2)'></span>";
              }
            }
            $this.popover({
              trigger: 'manual',
              html: true,
              content: contentEnd,
              placement: 'top',
              container: 'body'
            }).popover('show');
          }
          return false
        },
        dayRender:

          function (date, cell) {
            if (moment(date._d).day() === 0 || moment(date._d).day() === 6) {
              cell.css("background-color", "#ffecea");
            }
          }

        ,
        viewRender: function () {
          getLegalHolidays();
          getFilteredFreedays();
        }
      });
    }
  )
  ;
}

setInterval(function () {
  $("body > .popover").popover('hide');
}, 5000);

function approve(id, status) {
  if (theUser.admin !== 1) {
    if (status === 1) {
      displayApproveModal(eventInfo[id].obj, 0, id, eventInfo[id].type, eventInfo[id].email,
        eventInfo[id].avFdApprove, status, true, status)
    } else {
      displayApproveModal(eventInfo[id].obj, 0, id, eventInfo[id].type, eventInfo[id].email,
        eventInfo[id].avFdReject, status, false, status)
    }
  } else if (theUser.admin === 1) {
    if (status === 1) {
      displayApproveModal(eventInfo[id].obj, 0, id, eventInfo[id].type, eventInfo[id].email,
        eventInfo[id].avFdApprove, status, true, 0)
    } else {
      displayApproveModal(eventInfo[id].obj, 0, id, eventInfo[id].type, eventInfo[id].email,
        eventInfo[id].avFdReject, status, false, status)
    }
  }
  $("body > .popover").popover('hide')
}

function getLegalHolidays() {
  let countries = [], options = $('[name="selectCountry"].selectpicker option:selected'),
    view = $("#calendar").fullCalendar('getView'),
    date = view.dateProfile.date.format('YYYY-MM-DD'), holidays = [];

  if (loaded) {
    for (let i = 0; i < options.length; i++) {
      countries.push(options[i].index);
    }
    allEvents = $("#calendar").fullCalendar('clientEvents');
    if (countries.length !== 0) {
      $.get(appConfig.url + appConfig.api + 'retrieveFilteredHolidays?token=' +
        token + "&viewType=" + view.type + "&viewDate=" + date + '&countries=' + countries, function (data) {
        for (let i in allEvents) {
          if (allEvents[i].title.indexOf('name') === -1 && typeof allEvents[i].id === 'string') {
            $("#calendar").fullCalendar('removeEvents', allEvents[i]._id);
          }
        }
        for (let i in data) {
          if (data[i].type === "public") {
            if (theUser.country === 'ro') {
              if (data[i].country === 'md') {
                if (data[i].type === "public") {
                  legalDatesCalendarMD.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                  holidays.push({
                    start: new Date(data[i].startDate),
                    title: '+ ' + data[i].name + ' MD',
                    id: data[i].id + 's',
                    className: 'fc-md-holiday'
                  });
                }
              } else {
                legalDatesCalendar.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                holidays.push({
                  start: new Date(data[i].startDate),
                  title: '+ ' + data[i].name,
                  id: data[i].id + 's',
                  className: 'fc-ro-holiday'
                });
              }
            } else {
              if (data[i].country === 'ro') {
                if (data[i].type === "public") {
                  legalDatesCalendar.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                  holidays.push({
                    start: new Date(data[i].startDate),
                    title: '+ ' + data[i].name + ' RO',
                    id: data[i].id + 's',
                    className: 'fc-ro-holiday'
                  });
                }
              } else {
                legalDatesCalendarMD.push(moment(data[i].startDate).format("YYYY/MM/DD"));
                holidays.push({
                  start: new Date(data[i].startDate),
                  title: '+ ' + data[i].name,
                  id: data[i].id + 's',
                  className: 'fc-md-holiday'
                });
              }
            }
          }
        }
        $('#calendar').fullCalendar('renderEvents', holidays, true);
      });
    } else {
      for (let i in allEvents) {
        if (allEvents[i].title.indexOf('name') === -1 && typeof allEvents[i].id === 'string') {
          $("#calendar").fullCalendar('removeEvents', allEvents[i]._id);
        }
      }
    }
  }
}

function getFilteredFreedays(requestUserId) {
  let approvedO = $('[name="selectAP"].selectpicker option:selected'),
    idsO = $('[name="selectUser"].selectpicker option:selected'),
    options = $('[name="selectCountry"].selectpicker option:selected'),
    view, date, ids = [theUser.userID], approved = [], countries = [], type;
  if (loaded) {
    allEvents = $("#calendar").fullCalendar('clientEvents');

    if ($("#calendar>*").length != 0) {
      view = $("#calendar").fullCalendar('getView');
      date = view.dateProfile.date.format('YYYY-MM-DD');
    } else {
      date = (moment(new Date())).format('YYYY-MM-DD');
    }

    for (let i = 0; i < approvedO.length; i++) {
      approved.push(approvedO[i].index);
    }
    if (!requestUserId) {
      for (let i = 0; i < idsO.length; i++) {
        ids.push(idsO[i].value);
      }
    } else {
      for (let i = 0; i < ids.length; i++) {
        if (ids[i] != requestUserId) {
          ids.push(requestUserId);
        }
      }
      for (let i = 0; i < idsO.length; i++) {
        ids.push(idsO[i].value);
      }
    }

    for (let i = 0; i < options.length; i++) {
      countries.push(options[i].index);
    }

    if (view) {
      type = view.type;
    } else {
      type = 'month';
    }

    if (ids.length > 0 && approved.length > 0) {
      $.ajax({
        type: 'GET',
        url: appConfig.url + appConfig.api + 'getCalendarUsersName?token=' + token + '&userID=' + theUser.userID,
        async: false,
      }).done(function (dataCalendar) {
        let add, idsU = $('[name="selectUser"].selectpicker option');
        if (change === false) {
          for (let i = 0; i < dataCalendar.length; i++) {
            if (idsU.length == 0) {
              add = true;
            }
            for (let j = 0; j < idsU.length; j++) {
              if (idsU[j].value != dataCalendar[i].userID)
                add = true;
              else {
                add = false;
                break;
              }
            }
            if (add) {
              $('[name="selectUser"].selectpicker')
                .append($("<option selected></option>")
                  .attr("value", dataCalendar[i].userID)
                  .text(dataCalendar[i].name));
              ids.push(dataCalendar[i].userID)
            }
            $("[name='selectUser'].selectpicker").selectpicker('refresh');

          }
          change = true;
        }
      }).then(function () {
        $.ajax({
          type: 'GET',
          url: appConfig.url + appConfig.api + 'getFilteredFreedays?token=' + token + "&userID=" + theUser.userID +
          "&ids=" + ids + "&approved=" + approved + "&viewType=" + type + "&viewDate=" + date,
          async: false,
        }).then(function (data) {
          d = [];
          eventInfo = [];
          let color = 'black';
          for (let i in allEvents) {
            if (typeof allEvents[i].id !== 'string') {
              $("#calendar").fullCalendar('removeEvents', allEvents[i]._id);
            }
          }
          for (let i = 0; i < data.length; i++) {
            if (theUser.admin === 6) {
              if (JSON.parse(data[i].userObject).dep.ids) {
                color = 'black';
              } else {
                color = 'green';
              }
            }

            let endTime, avFdReject, userObject,
              supervisorApprover = false, avFdApprove;

            if (data[i].type === "Concediu" && theUser.admin !== 1) {
              avFdApprove = data[i].avfreedays - data[i].days
            } else {
              avFdApprove = data[i].avfreedays;
            }

            endTime = new Date(data[i].endDate);
            endTime.setMinutes(1);
            if (data[i].approved === 1 && data[i].type === 'Concediu') {
              avFdReject = data[i].avfreedays + data[i].days;
            } else {
              avFdReject = data[i].avfreedays;
            }
            eventInfo[data[i].id] = {
              id: data[i].id,
              email: data[i].email,
              obj: $.parseJSON(data[i].userObject),
              type: data[i].type,
              avFdApprove: avFdApprove,
              avFdReject: avFdReject
            };

            userObject = JSON.parse(data[i].userObject);
            for (let i in userObject.ids) {
              if (theUser.admin == 1 && userObject.ids[i].approved) {
                supervisorApprover = true;
                break;
              }
              if ((theUser.admin === 3 || theUser.admin === 4 || theUser.admin === 6) && userObject.ids[i].approved) {
                supervisorApprover = true;
                break;
              }
              if (userObject.ids[i].approved && theUser.userID === userObject.ids[i].id) {
                supervisorApprover = true;
                break;
              } else {
                supervisorApprover = false;
              }
            }

            if (jQuery.isEmptyObject(userObject.dep)) {
              supervisorApprover = true;
            }

            if (data[i].approved === 0) {
              if (data[i].userID === theUser.userID) {
                d.push({
                  start: new Date(data[i].startDate),
                  end: endTime,
                  title: 'pending: ' + data[i].type,
                  id: theUser.userID,
                  className: 'fc-myevent-pending'
                });
              } else {
                if (supervisorApprover) {
                  d.push({
                    start: new Date(data[i].startDate),
                    end: endTime,
                    title: 'pending: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                    id: data[i].id,
                    className: 'fc-event-supervisor-pending',
                    eventTextColor: color
                  });
                } else {
                  d.push({
                    start: new Date(data[i].startDate),
                    end: endTime,
                    title: 'pending: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                    id: data[i].id,
                    className: 'fc-event-pending',
                    eventTextColor: color
                  });
                }
              }
            } else if (data[i].approved === 1) {
              if (data[i].userID === theUser.userID) {
                d.push({
                  start: new Date(data[i].startDate),
                  end: endTime,
                  title: 'approved: ' + data[i].type,
                  id: theUser.userID,
                  className: 'fc-myevent-approved'
                });
              } else {
                d.push({
                  start: new Date(data[i].startDate),
                  end: endTime,
                  title: 'approved: ' + data[i].type + '\n' + 'name: ' + data[i].name,
                  id: data[i].id,
                  className: 'fc-event-approved',
                  eventTextColor: color
                });
              }
            }
          }
          $('#calendar').fullCalendar('renderEvents', d, true);
        });
      });
    } else {
      for (let i in allEvents) {
        if (allEvents[i].title.indexOf('name') !== -1 && typeof allEvents[i].id !== 'string' && allEvents[i].id !== theUser.userID) {
          $("#calendar").fullCalendar('removeEvents', allEvents[i]._id);
        }
      }
    }
  }
}