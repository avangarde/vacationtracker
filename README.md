# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Use this as a holiday planner in your company.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* create database holidayplanner
* import holidayplanner.sql into the database created before
* cd backend
* npm install
* node app.js
* launch your app in the browser 
